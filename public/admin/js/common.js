
function readURL(input, name) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.readAsDataURL(input.files[0]);
        reader.onload = function (e) {
            $('#preview-'+name).attr('src', e.target.result);
//              $("#remove").val(0);
        };
    }
}
function confirmDelete (msg) {
    if (window.confirm(msg)) {
        return true;
    }
    return false;
}

$("#formSubmit").on("submit", function(){
    $("#btnPrevent").prop('disabled',true);
    $('#loadingIcon').show();
});

function generateSlug() {
    let string = $('#nameRecord').val();
    let slug = this.getSlug(string);
    $('#slugRecord').val(slug);
}

function manualSlug() {
    let string = $('#slugRecord').val();
    let slug = this.getSlug(string);
    $('#slugRecord').val(slug);
}

function getSlug(str) {
    let slug = "";
    let titleLower = str.toLowerCase();
    slug = titleLower.replace(/e|é|è|ẽ|ẻ|ẹ|ê|ế|ề|ễ|ể|ệ/gi, 'e');
    slug = slug.replace(/a|á|à|ã|ả|ạ|ă|ắ|ằ|ẵ|ẳ|ặ|â|ấ|ầ|ẫ|ẩ|ậ/gi, 'a');
    slug = slug.replace(/o|ó|ò|õ|ỏ|ọ|ô|ố|ồ|ỗ|ổ|ộ|ơ|ớ|ờ|ỡ|ở|ợ/gi, 'o');
    slug = slug.replace(/u|ú|ù|ũ|ủ|ụ|ư|ứ|ừ|ữ|ử|ự/gi, 'u');
    slug = slug.replace(/đ/gi, 'd');
    slug = slug.replace(/ì|í|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ỳ|ý|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/\s+/g, '-')  ;
    slug = slug.replace(/[^\w\-]+/g, '');
    slug = slug.replace(/\-\-+/g, '-');
    slug = slug.replace(/^-+/, '') ;
    slug = slug.replace(/-+$/, '');
    return slug;
}
let valuesArray = '';
$("#check_all").change(function(){
    var checked = $(this).is(':checked');
    if(checked){
        $(".checkbox").each(function(){
            $(this).prop("checked",true);
        });
        valuesArray = $('.checkbox:checked').map( function() {
            return this.value;
        }).get().join(",");

    }else{
        $(".checkbox").each(function(){
            $(this).prop("checked",false);
        });
        valuesArray = '';
    }
    $('#data_checkboxes').val(valuesArray);
});

// Changing state of CheckAll checkbox
$(".checkbox").click(function(){

    if($(".checkbox").length == $(".checkbox:checked").length) {
        $("#check_all").prop("checked", true);
    } else {
        $("#check_all").prop("checked", false);
    }
    valuesArray = $('.checkbox:checked').map( function() {
        return this.value;
    }).get().join(",");
    $('#data_checkboxes').val(valuesArray);
});

function changeStatus(stt, id, table) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/api/update-status',
        type: 'POST',
        cache: false,
        data: {
            status: stt,
            id: id,
            table: table,
        },
        success: function (data) {
            $('.background-success').show();
            setTimeout(function() {
                location.reload();
                }, 1000);
        },
        error: function (data) {
            $('.background-danger').show();
            setTimeout(function() {
                location.reload();
            }, 1000);
        }
    });
}

function updateOrder(id, table) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    let ord = $('#updateOrder-'+id).val();
    $.ajax({
        url: '/api/update-order',
        type: 'POST',
        cache: false,
        data: {
            id: id,
            order: ord,
            table: table,
        },
        success: function (data) {
            console.log(data);
        },
        error: function (err) {
            console.log(err);

        }
    });
}


