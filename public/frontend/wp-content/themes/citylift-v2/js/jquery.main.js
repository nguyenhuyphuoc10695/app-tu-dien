jQuery(document).ready(function($) {
    
    /******************************************************************************************
    Open Menu
    ******************************************************************************************/
    $('.toggle').click(function() {
		$(this).parent().toggleClass("active");
        $("body").toggleClass("menu");
  	});
    
    /******************************************************************************************
    Scroll to section on page load
    Used in "Other Properties" carousel on template-solutions-template.php
    ******************************************************************************************/
    if (window.location.hash) {        
        setTimeout(function() {
            $('html, body').scrollTop(0).show();
            var menuHeight = $('header').height() - 1;
            console.log(menuHeight);
            $('html, body').animate({
                scrollTop: $(window.location.hash).offset().top - menuHeight}, 1200)
            }, 
        2);
    }
    
    /******************************************************************************************
    Contact Form
    Add custom fields to contact form
    ******************************************************************************************/
    var selectArray = [ 'ARE YOU A DEVELOPER OR ARCHITECT?', 'PROJECT  TYPE', 'STATE'];
    for (var i = 0; i < selectArray.length; i++) {
        var p = selectArray[i]
        $('.wpcf7').find('select').eq(i).find('option:eq(0)').html(p);
    }
    $('.wpcf7-submit').on('click', function () {
        ga('send', 'event', 'button', 'click', 'form-submit-click');
    });
    
    /******************************************************************************************
    Gallery
    - page-case-studies.php (video)
    - template-casestudy.php (video)
    - template-solutions-template.php (images + video)
    ******************************************************************************************/
    if (typeof galleryBtn !== 'undefined') {
        function openGallery(wg,md,wo){
            var slidecontainer = $('.gallery-overlay-slides'),
                navcontainer = $('.gallery-overlay-nav ul'),
                KEYCODE_ESC = 27,
                KEYCODE_PREV = 37,
                KEYCODE_NEXT = 39,
                slideIndex = wo;
            
            console.log("-------------");
            console.log(wg);
            console.log(md);
            
            // Open gallery overlay
            $("body").addClass("gallery-overlay-open");
            
            // Emptyout containers
            slidecontainer.empty();
            navcontainer.empty();
            
            if(md == "photo"){
                var totalSlides = wg.length;
                
                // Create slides & slide nav
                for(var i = 0; i <= totalSlides - 1; i++) {
                    slidecontainer.append('<div class="overlay-slide" style="background-image: url(\'' + wg[i] + '\');"></div>');
                    navcontainer.append('<li><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve"><path class="ring" d="M10.1,19c-5,0-9-4-9-9c0-5,4-9,9-9c5,0,9,4,9,9C19.1,15,15.1,19,10.1,19z M10.1,2.4c-4.2,0-7.7,3.4-7.7,7.6 c0,4.2,3.5,7.6,7.7,7.6c4.2,0,7.7-3.4,7.7-7.6C17.8,5.8,14.4,2.4,10.1,2.4z"/><circle class="dot" cx="10.1" cy="10" r="7.7"/></svg></li>');  
                }
                
                // Set initial slide
                $(".gallery-overlay-slides .overlay-slide:nth-child(" + slideIndex + ")" ).addClass("current-slide");
                
                // Set initial nav
                $(".gallery-overlay-nav ul li:nth-child(" + slideIndex + ")" ).addClass("gallery-overlay-nav-active");
                
                // Gallery slide nav
                $(".gallery-overlay-nav li").click(function(){
                    slideIndex = $(this).index() + 1;
                    updateSlide("direct");
                });
            } else {
                
                var totalSlides = videoArr.length;
                $(".gallery-overlay-slides").html('<div class="gallery-video-container"><iframe src="https://player.vimeo.com/video/'+wg+'?autoplay=1" width="960" height="540" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>');
                
                if (totalSlides > 1){
                    for(var i = 0; i <= totalSlides - 1; i++) {
                        navcontainer.append('<li><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve"><path class="ring" d="M10.1,19c-5,0-9-4-9-9c0-5,4-9,9-9c5,0,9,4,9,9C19.1,15,15.1,19,10.1,19z M10.1,2.4c-4.2,0-7.7,3.4-7.7,7.6 c0,4.2,3.5,7.6,7.7,7.6c4.2,0,7.7-3.4,7.7-7.6C17.8,5.8,14.4,2.4,10.1,2.4z"/><circle class="dot" cx="10.1" cy="10" r="7.7"/></svg></li>');  
                    }
                }
                
                // Set initial nav
                $(".gallery-overlay-nav ul li:nth-child(" + (slideIndex + 1) + ")" ).addClass("gallery-overlay-nav-active");
                
                // Gallery slide nav
                $(".gallery-overlay-nav li").click(function(){
                    slideIndex = $(this).index();
                    updateSlide("direct");
                });
            }
            
            if (totalSlides <=1){
                $(".gallery-overlay-prev").fadeOut(0);
                $(".gallery-overlay-next").fadeOut(0);
            }

            // Gallery keyboard functionality
            $(document).keyup(function(e) {
                if (e.keyCode == KEYCODE_ESC) {
                    $(".gallery-overlay-close").click();
                }
                if (e.keyCode == KEYCODE_PREV) {
                    e.preventDefault();
                    $(".gallery-overlay-prev").click();
                }
                if (e.keyCode == KEYCODE_NEXT) {
                    e.preventDefault();
                    $(".gallery-overlay-next").click();
                }
            });

            // Close gallery
            $(".gallery-overlay-close").click(function(){

                $(".gallery-overlay-slides").html('');

                // Unbind
                $(".gallery-overlay-nav li, .gallery-overlay-prev, .gallery-overlay-next").unbind("click");
                $(this).unbind("click");
                $(document).unbind("keyup");

                $("body").removeClass("gallery-overlay-open"); // Close gallery overlay
                $(".gallery-overlay-nav ul li").removeClass("gallery-overlay-nav-active"); // Reset nav
                $(".gallery-overlay-slides ul li").removeClass("gallery-overlay-slide-active"); // Reset slide
            });

            // Prev button
            $(".gallery-overlay-prev").click(function(){
                updateSlide("prev");
            });

            // Next button
            $(".gallery-overlay-next").click(function(){
                updateSlide("next");
            });

            // Image click
            $(".gallery-overlay-slides .overlay-slide").click(function(){
                updateSlide("next");
            });

            function updateSlide(dir){
                
                if(md == "photo"){
                    
                    // Update Slides
                    if(dir == "next"){
                        slideIndex++;
                        if(slideIndex > totalSlides){
                            slideIndex = 1;
                        }
                    } else if (dir == "prev"){
                        slideIndex--;
                        if(slideIndex < 1){
                            slideIndex = totalSlides;
                        }
                    }
                    
                    // Update slide
                    $(".gallery-overlay-slides .overlay-slide").removeClass("current-slide");
                    $(".gallery-overlay-slides .overlay-slide:nth-child(" + slideIndex + ")" ).addClass("current-slide");
                    
                    // Update Nav
                    $(".gallery-overlay-nav ul li").removeClass("gallery-overlay-nav-active");
                    $(".gallery-overlay-nav ul li:nth-child(" + slideIndex + ")" ).addClass("gallery-overlay-nav-active");
                    
                } else {
                    
                    // Update Slides
                    if(dir == "next"){
                        slideIndex++;
                        if(slideIndex > (totalSlides - 1)){
                            slideIndex = 0;
                        }
                    } else if (dir == "prev"){
                        slideIndex--;
                        if(slideIndex < 0){
                            slideIndex = totalSlides - 1;
                        }
                    }
                    // Update Video
                    $(".gallery-overlay-slides").html('<div class="gallery-video-container"><iframe src="https://player.vimeo.com/video/'+videoArr[slideIndex]+'?autoplay=1" width="960" height="540" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>');
                    
                    // Update Nav
                    $(".gallery-overlay-nav ul li").removeClass("gallery-overlay-nav-active");
                    $(".gallery-overlay-nav ul li:nth-child(" + (slideIndex+1) + ")" ).addClass("gallery-overlay-nav-active");
                }
                
                
            }

                
        }
        
        // galleryBtn variable is defined in the below pages
        // var galleryBtn = jQuery(".casestudy-view-more");
        // page-case-studies.php
        // template-casestudy.php
        // template-solutions-template.php
        galleryBtn.click(function(e){
            e.preventDefault();
            gtype = $(this).attr('data-gallery-type');
            if(gtype == "photo"){
                gid = eval('slideArr' + $(this).attr('data-gallery-id'));
                wo = 1;
            }else{
                gid = $(this).attr('data-gallery-id');
                wo = $(this).parent().parent().parent().index();
                console.log("Slide = " + (wo + 1));
            }
            
            openGallery(gid,gtype,wo);
        });
    }

    
    /******************************************************************************************
    Homepage
    - page-home.php
    ******************************************************************************************/
    
    if($('body').hasClass('home')){ 

        // Animate SVG on panel 3
        var animateSVG = function(){
            executed = true;
            // do something
            var shapes = $("rect, polyline, circle, line"),
            homeTL = new TimelineMax();
            homeTL.staggerFromTo(shapes, 1.25, {drawSVG:"-10% 0%"}, {drawSVG:"100%"}, 0.23);
        };
        
        // FullPage functionality
        $('#home').fullpage({
            anchors: [],
            navigation: true,
            navigationPosition: 'right',
            navigationTooltips: [],
            verticalCentered: false,
            responsiveWidth: 1024,
            afterRender: function(){
                $('#subvideo').get(0).play();
            },
            afterLoad: function(anchorLink, index) {
                // Animate Panel Three SVG Animation
                if (index == 3) {
                    if($("#svg-home-box").hasClass("home-start")){
                        // Animate SVG
                        animateSVG();
                        // .home-start class sets stroke of SVG to background color of container
                        $("#svg-home-box").removeClass("home-start"); 
                    }
                }
            }
        });
        
        // Rollover footer functionality on homepage
        jQuery("#footer-home").mouseenter(function() {
            jQuery('body').addClass('home-footed');
        });
        jQuery("footer").mouseleave(function() {
            jQuery('body').removeClass('home-footed');
        });
    }
    
    
    /******************************************************************************************
    Solutions
    - page-solutions.php
    ******************************************************************************************/
    
    if($('body').hasClass('solutions')){ 
        
        var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
        
        // Roll over "View Specs" functionality
        $('.solutions-view-specs').hover(function() {
            if($(this).parent().hasClass("specs-shown")){ // If animated, don't animate again
                // Show rollover
                $(this).parent().addClass("show-specs");
            } else {
                // Set Vars
                var solID = $(this).attr('id'),
                    endRetrieval = $(this).attr('data-end-retrieval'),
                    endLevels = $(this).attr('data-end-levels'),
                    endInstalled = $(this).attr('data-end-installed');
                
                // Show rollover
                $(this).parent().addClass("show-specs");
                
                // Animate numbers
                $('#' + solID + '-retrieval').animateNumber({ number: endRetrieval }, 800);
                $('#' + solID + '-levels').animateNumber({ number: endLevels }, 600);
                if(solID == "info-solutions"){
                    $('#' + solID + '-installed').animateNumber({ number: endInstalled }, 800);
                }else{
                    $('#' + solID + '-installed').animateNumber({
                        number: endInstalled,
                        numberStep: comma_separator_number_step
                    }, 1000);
                }
            }
        }, function() {
            // Add .specs-shown class to disable animation on subsequent rollovers
            $(this).parent().addClass("specs-shown").removeClass("show-specs"); 
        });
    }
    
    /******************************************************************************************
    Solutions - Interior
    - template-solutions-template.php
    ******************************************************************************************/
    
    if($('body').hasClass('page-template-template-solutions-template-php')){
        
        // Full-width Video carousel
        $('#vid-carousel').owlCarousel({
            items: 1, 
            itemsDesktop: false,
            itemsDesktopSmall: false,
            itemsTablet: false,
            itemsMobile: false,
            nav: false,
            dots: true,
            autoHeight: true,
            mouseDrag: false,
            autoPlay: 4000,
            /*stopOnHover: true*/
        });  
        
        // Other properties carousel
        $("#properites-carousel").owlCarousel({
            items : 3,
            nav : true,
            dots : false, 
            //margin: 20,
            navText: [
            "<span class='previous-left'></span>",
            "<span class='next-right'></span>"
            ],
            //itemsDesktop : [1199,3],
            //itemsDesktopSmall : [979,3]
        });
    }
    
    /******************************************************************************************
    About
    - page-about.php
    ******************************************************************************************/
    
    if($('body').hasClass('about')){
        $('#info-countries').waypoint(function() {
            var endNum = $('#info-countries').attr('data-end-number');
            $('#info-countries').animateNumber({ number: endNum }, 600);
            this.destroy();
        },{offset:'80%'});
        
        var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
        $('#info-systems').waypoint(function() {
            var endNum = $('#info-systems').attr('data-end-number');
            $('#info-systems').animateNumber({
                number: endNum,
                numberStep: comma_separator_number_step
            }, 1500);
            this.destroy();
        },{offset:'80%'});
        
        $('#info-experience').waypoint(function() {
            var endNum = $('#info-experience').attr('data-end-number');
            $('#info-experience').animateNumber({ number: endNum }, 1000);
            this.destroy();
        },{offset:'80%'});
    }
    
});