jQuery(document).ready(function($){
    
    // Single News Post
    if($('body').hasClass('single-post')) {
        $('#svg-value').waypoint(function() {
            value();
            $('#svg-value').addClass("blue");
            this.destroy();
        },{offset:'60%'});
        
        $('#svg-collab-approach').waypoint(function() {
            $('#svg-collab-approach').addClass("blue");
            collabapproach();
            this.destroy();
        },{ offset:'60%'});

        $('#svg-collab').waypoint(function() {
            $('#svg-collab').addClass("blue");
            collab();
            this.destroy();
        },{ offset:'60%'});

        $('#svg-safety').waypoint(function() {
            $('#svg-safety').addClass("blue");
            safety();
            this.destroy();
        },{ offset:'60%'});
        
        $('#svg-puzzle').waypoint(function() {
            $('#svg-puzzle').addClass("blue");
            puzzle();
            this.destroy();
        },{offset:'80%'});

        $('#svg-tower').waypoint(function() {
            $('#svg-tower').addClass("blue");
            tower();
            this.destroy();
        },{offset:'80%'});

        $('#svg-aisle').waypoint(function() {
            $('#svg-aisle').addClass("blue");
            aielz();
            this.destroy();
        },{ offset:'80%'});

        $('#svg-shuffle').waypoint(function() {
            $('#svg-shuffle').addClass("blue");
            shuffle();
            this.destroy();
        },{ offset:'80%'});

        $('#svg-stacker').waypoint(function() {
            $('#svg-stacker').addClass("blue");
            stacker();
            this.destroy();
        },{ offset:'80%'});
    }
    
    // Single News Post
    if($('body').hasClass('about')) {        
        $('#svg-puzzle').waypoint(function() {
            $('#svg-puzzle').addClass("blue");
            puzzle();
            this.destroy();
        },{offset:'80%'});

        $('#svg-tower').waypoint(function() {
            $('#svg-tower').addClass("blue");
            tower();
            this.destroy();
        },{offset:'80%'});

        $('#svg-aisle').waypoint(function() {
            $('#svg-aisle').addClass("blue");
            aielz();
            this.destroy();
        },{ offset:'80%'});

        $('#svg-shuffle').waypoint(function() {
            $('#svg-shuffle').addClass("blue");
            shuffle();
            this.destroy();
        },{ offset:'80%'});

        $('#svg-stacker').waypoint(function() {
            $('#svg-stacker').addClass("blue");
            stacker();
            this.destroy();
        },{ offset:'80%'});
    }

    // Approach
    if($('body').hasClass('approach')) {
        $('#svg-value').waypoint(function() {
            value();
            $('#svg-value').addClass("blue");
            this.destroy();
        },{offset:'60%'});
        
        $('#svg-collab-approach').waypoint(function() {
            $('#svg-collab-approach').addClass("blue");
            collabapproach();
            this.destroy();
        },{ offset:'60%'});

        $('#svg-collab').waypoint(function() {
            $('#svg-collab').addClass("blue");
            collab();
            this.destroy();
        },{ offset:'60%'});

        $('#svg-safety').waypoint(function() {
            $('#svg-safety').addClass("blue");
            safety();
            this.destroy();
        },{ offset:'60%'});
        
        $('#svg-puzzle').waypoint(function() {
            $('#svg-puzzle').addClass("blue");
            puzzle();
            this.destroy();
        },{offset:'80%'});

        $('#svg-tower').waypoint(function() {
            $('#svg-tower').addClass("blue");
            tower();
            this.destroy();
        },{offset:'80%'});

        $('#svg-aisle').waypoint(function() {
            $('#svg-aisle').addClass("blue");
            aielz();
            this.destroy();
        },{ offset:'80%'});

        $('#svg-shuffle').waypoint(function() {
            $('#svg-shuffle').addClass("blue");
            shuffle();
            this.destroy();
        },{ offset:'80%'});

        $('#svg-stacker').waypoint(function() {
            $('#svg-stacker').addClass("blue");
            stacker();
            this.destroy();
        },{ offset:'80%'});
    }
    
    //News
    if($('body').hasClass('news')) {
        $('#svg-value').waypoint(function() {
            value();
            $('#svg-value').addClass("blue");
            this.destroy();
        },{offset:'60%'});
        
        $('#svg-collab-approach').waypoint(function() {
            $('#svg-collab-approach').addClass("blue");
            collabapproach();
            this.destroy();
        },{ offset:'60%'});

        $('#svg-collab').waypoint(function() {
            $('#svg-collab').addClass("blue");
            collab();
            this.destroy();
        },{ offset:'60%'});

        $('#svg-safety').waypoint(function() {
            $('#svg-safety').addClass("blue");
            safety();
            this.destroy();
        },{ offset:'60%'});
        
        $('#svg-puzzle').waypoint(function() {
            $('#svg-puzzle').addClass("blue");
            puzzle();
            this.destroy();
        },{offset:'80%'});

        $('#svg-tower').waypoint(function() {
            $('#svg-tower').addClass("blue");
            tower();
            this.destroy();
        },{offset:'80%'});

        $('#svg-aisle').waypoint(function() {
            $('#svg-aisle').addClass("blue");
            aielz();
            this.destroy();
        },{ offset:'80%'});

        $('#svg-shuffle').waypoint(function() {
            $('#svg-shuffle').addClass("blue");
            shuffle();
            this.destroy();
        },{ offset:'80%'});

        $('#svg-stacker').waypoint(function() {
            $('#svg-stacker').addClass("blue");
            stacker();
            this.destroy();
        },{ offset:'80%'});
    }


    // Solutions
    if($('body').hasClass('solutions')) {
        $('#svg-puzzle').waypoint(function() {
            $('#svg-puzzle').addClass("blue");
            puzzle();
            this.destroy();
        },{offset:'60%'});

        $('#svg-tower').waypoint(function() {
            $('#svg-tower').addClass("blue");
            tower();
            this.destroy();
        },{offset:'60%'});

        $('#svg-aisle').waypoint(function() {
            $('#svg-aisle').addClass("blue");
            aielz();
            this.destroy();
        },{ offset:'60%'});

        $('#svg-shuffle').waypoint(function() {
            $('#svg-shuffle').addClass("blue");
            shuffle();
            this.destroy();
        },{ offset:'60%'});

        $('#svg-stacker').waypoint(function() {
            $('#svg-stacker').addClass("blue");
            stacker();
            this.destroy();
        },{ offset:'60%'});
        
        $('#svg-me').waypoint(function() {
            $('#svg-me').addClass("blue");
            mechanical();
            this.destroy();
        },{offset:'80%'});

        $('#svg-pm').waypoint(function() {
            $('#svg-pm').addClass("blue");
            management();
            this.destroy();
        },{ offset:'80%' });

        $('#svg-maintenance').waypoint(function() {
            $('#svg-maintenance').addClass("blue");
            maintenance();
            this.destroy();
        },{offset:'80%'}); 
    }

    // Service
    if($('body').hasClass('service')) {
        $('#svg-me').waypoint(function() {
            $('#svg-me').addClass("blue");
            mechanical();
            this.destroy();
        },{offset:'60%'});

        $('#svg-pm').waypoint(function() {
            $('#svg-pm').addClass("blue");
            management();
            this.destroy();
        },{ offset:'60%' });

        $('#svg-maintenance').waypoint(function() {
            $('#svg-maintenance').addClass("blue");
            maintenance();
            this.destroy();
        },{offset:'60%'});     
        
        $('#svg-puzzle').waypoint(function() {
            $('#svg-puzzle').addClass("blue");
            puzzle();
            this.destroy();
        },{offset:'80%'});

        $('#svg-tower').waypoint(function() {
            $('#svg-tower').addClass("blue");
            tower();
            this.destroy();
        },{offset:'80%'});

        $('#svg-aisle').waypoint(function() {
            $('#svg-aisle').addClass("blue");
            aielz();
            this.destroy();
        },{ offset:'80%'});

        $('#svg-shuffle').waypoint(function() {
            $('#svg-shuffle').addClass("blue");
            shuffle();
            this.destroy();
        },{ offset:'80%'});

        $('#svg-stacker').waypoint(function() {
            $('#svg-stacker').addClass("blue");
            stacker();
            this.destroy();
        },{ offset:'80%'});
    }

   var value = (function() {
        var executed = false;
        return function () {
            if (!executed) {
                executed = true;
                // do something
                var $vt = $("#object_1v"),
                $vm = $("#object_2v"),
                valueTL = new TimelineLite(),
                valueStart = .25;

                valueTL.fromTo($vm, 1.5, {opacity:0, scaleX:0.5,  scaleY:0.5, transformOrigin:"center center"}, {opacity:1, scale:1, rotation:90}, valueStart);
                valueTL.play();
            }
        };
    })();
    
    var collab = (function() {
        var executed = false;
        return function () {
            if (!executed) {
                executed = true;
                // do something
                var $ct = $("#object_1c"),
                $cm = $("#object_2c"),
                collabTL = new TimelineLite(),
                collabStart = .25;
                
                collabTL.fromTo($cm, 1.5, {opacity:0}, {opacity:1, x:35, y:37}, collabStart);
                collabTL.play();
            }
        };
    })();

    var collabapproach = (function() {
        var executed = false;
        return function () {
            if (!executed) {
                executed = true;
                // do something
                var $at = $("#object_4"),
                $am = $("#object_3"),
                $ab = $("#aisle-b"),
                aisleTL = new TimelineLite(),
                aisleStart = .25;

                aisleTL.from($am, 1.5, {opacity: 0, y:0, x: 0, scale:0.5}, aisleStart);
                aisleTL.from($at, 1.5, {opacity: 0, scale:0.5, x:30, y:30}, aisleStart);
                aisleTL.play();
            }
        };
    })();

    var safety = (function() {
        var executed = false;
        return function () {
            if (!executed) {
                executed = true;
                // do something
                var safety = $("#svg-safety polygon"),
                safetyTL = new TimelineLite();

                safetyTL.staggerFrom(safety, 1.75, {autoAlpha:0}, 0.25, "stagger");
                safetyTL.play();
            }
        };
    })();


    var tower = (function() {
        var executed = false;
        return function () {
            if (!executed) {
                executed = true;
                // do something
               var $tower = $("#svg-tower polygon"),
                towerTL = new TimelineLite(),
                towerStart = .25;
          
                towerTL.staggerFrom($tower, 0.2, {autoAlpha:0}, 1, "stagger");
                towerTL.play();
            }
        };
    })();


    var puzzle = (function() {
        var executed = false;
        return function () {
            if (!executed) {
                executed = true;
                var puzzles = $("#svg-puzzle path"),
                puzzleTL = new TimelineLite();

                puzzleTL.staggerFrom(puzzles, 1.75, {autoAlpha:0}, 0.25, "stagger");
                puzzleTL.play();
            }
        };
    })();


    var aielz = (function() {
        var executed = false;
        return function () {
            if (!executed) {
                executed = true;
               var $at = $("#aisle-t"),
                $am = $("#aisle-m"),
                $ab = $("#aisle-b"),
                aisleTL = new TimelineLite(),
                aisleStart = .25;

                aisleTL.from($ab, 1.5, {opacity: 0}, aisleStart);
                aisleTL.from($am, 1.5, {opacity: 0, x: -28}, aisleStart);
                aisleTL.from($at, 1.5, {opacity: 0}, aisleStart);
                aisleTL.play();
            }
        };
    })();

    var shuffle = (function() {
        var executed = false;
        return function () {
            if (!executed) {
                executed = true;
                var $sh1  = $("#shuffle-1-slide"),
                    $shb1 = $("#shuffle-1-base"),
                    $sh2  = $("#shuffle-2-slide"),
                    $shb2 = $("#shuffle-2-base"),
                    $sh3  = $("#shuffle-3-slide"),
                    $shb3 = $("#shuffle-3-base"),
                    shuffleTL = new TimelineLite(),
                    shuffleStart1 = .25,
                    shuffleStart2 = .65,
                    shuffleStart3 = 1.05
          
                shuffleTL.from($sh1, 1.25, {opacity: 0.1, x:40}, shuffleStart3);
                shuffleTL.from($shb1, .75, {opacity: 0.1}, shuffleStart3);
                shuffleTL.from($sh2, 1.5, {opacity: 0.1, x:-40}, shuffleStart2);
                shuffleTL.from($shb2, .75, {opacity: 0.1}, shuffleStart2);
                shuffleTL.from($sh3, 1.75, {opacity: 0.1, x:40}, shuffleStart1);
                shuffleTL.from($shb3, .75, {opacity: 0.1}, shuffleStart1)
                shuffleTL.play();
            }
        };
    })();


    var stacker = (function() {
        var executed = false;
        return function () {
            if (!executed) {
                executed = true;
                var stacker = $("#svg-stacker path"),
                stackerTL = new TimelineLite(),
                stackerStart = .25;
          
                stackerTL.staggerFrom(stacker, 1.75, {autoAlpha:0}, 0.25, "stagger");
                stackerTL.play();
            }
        };
    })();


    var management = (function() {
        var executed = false;
        return function () {
            if (!executed) {
                executed = true;
                var $pm = $(".box-pm1"),
                $pm2 = $(".box-pm2"),
                $pm3 = $(".box-pm3"),
                $pmrec = $(".box-pm-in"),
                pmTL = new TimelineLite(),
                pmStart = .25;
          
                //pmTL.set($pmrec, {opacity:0});
                pmTL.from($pm, 1.5, {opacity: 0}, pmStart);
                pmTL.from($pm2, 1.5, {opacity: 0, delay:0.5}, pmStart);
                pmTL.from($pm3, 1.5, {opacity: 0, delay:1}, pmStart);
                pmTL.from($pmrec, 1.5, {opacity: 0, delay:1.25}, pmStart);
                pmTL.play();
            }
        };
    })();


    var mechanical = (function() {
        var executed = false;
        return function () {
            if (!executed) {
                executed = true;
                var $me = $("#me1"),
                $me2 = $("#me2"),
                $me3 = $("#me3"),
                meTL = new TimelineLite(),
                meStart = .25;
          
                meTL.from($me3, 1.5, {opacity: 0, rotation:90, transformOrigin:"50% 50%"}, meStart);
                meTL.from($me2, 1.5, {opacity: 0, rotation:90, transformOrigin:"50% 50%", delay:0.75}, meStart);
                meTL.from($me, 1.5, {opacity: 0, rotation:90, transformOrigin:"50% 50%", delay:1}, meStart);
                
                meTL.play();
            }
        };
    })();



    var maintenance = (function() {
        var executed = false;
        return function () {
            if (!executed) {
                executed = true;
                var $hr = $("#hour"),
                $min = $("#minute"),
                $clock = $("#clock-face"),
                maintenanceTL = new TimelineLite(),
                maintenanceStart = .25;

                maintenanceTL.fromTo($hr, 3, {opacity: 0,rotation: -90, transformOrigin:"center bottom"},{opacity: 1,rotation: -40}, maintenanceStart);
                maintenanceTL.fromTo($min, 3, {opacity: 0,rotation: -315, transformOrigin:"center bottom"},{opacity: 1,rotation: 45}, maintenanceStart);
                maintenanceTL.from($clock, 2, {opacity: 0}, maintenanceStart);
                maintenanceTL.play();
            }
        };
    })();

})
































