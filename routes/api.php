<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['as' => 'api.'], function () {
    Route::post('list-careers', 'API\MainController@getListCareers');
    Route::post('update-status', 'API\MainController@updateStatus');
    Route::post('update-order', 'API\MainController@updateOrder');
    Route::post('contact-owner', 'API\MainController@saveOwner');
    Route::post('contact-investor', 'API\MainController@saveInvestor');
    Route::group(['prefix' => 'menu'], function () {
        Route::get('index', 'API\OrderController@index');
        Route::get('show/{id}', 'API\OrderController@show');
        Route::post('update/{id}', 'API\OrderController@update');
        Route::delete('delete/{id}', 'API\OrderController@destroy');
    });
});