<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('/', 'FrontEnd\HomeController@index')->name('home');
//Route::get('page/{slug}', 'FrontEnd\PageController@getPage');
//Route::get('chi-tiet-tin/{slug}', 'FrontEnd\PostController@getDetailNews')->name('detailPost');
//Route::get('event/{slug}', 'FrontEnd\PageController@getDetailEvent')->name('detailEvent');
//Route::get('tin-tuc/{slug}', 'FrontEnd\PostController@getCategoryNews');
//Route::get('giai-phap/{slug}', 'FrontEnd\PageController@getCategorySolution');
//Route::get('course/{slug}', 'FrontEnd\CourseController@getDetailCourse');
//Route::get('search', 'FrontEnd\CourseController@getSearchCourse')->name('search');
//Route::get('ajax/comment', 'FrontEnd\PageController@getCommentByPost')->name('comment');
//Route::post('ajax/save-comment', 'FrontEnd\PageController@saveCommentByPost')->name('save_comment');
//Route::get('ajax/list-course', 'FrontEnd\CourseController@getCourseAjax')->name('list_course');
//Route::get('ajax/category-post', 'FrontEnd\HomeController@getCategoryPosts')->name('category_post');
//Route::get('career/{slug}', 'FrontEnd\PageController@getCareerDetail')->name('career_detail');
//Route::post('contact/', 'FrontEnd\PageController@saveContact')->name('save_contact');
//Route::post('newsletter/', 'FrontEnd\PageController@saveNewsLetter')->name('save_newsletter');
//Route::get('change-language/{slug}', 'FrontEnd\HomeController@changeLanguage')->name('change_language');

Auth::routes();


Route::group(['middleware' => ['auth', 'is_active'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('dashboard', 'Admin\AdminController@index')->name('dashboard');
    Route::get('logout', 'Admin\AdminController@logout')->name('logout');
    // Manage User Account
    Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
        Route::get('index', 'Admin\UserController@index')->name('index');
        Route::get('create', 'Admin\UserController@create')->name('create');
        Route::post('store', 'Admin\UserController@store')->name('store');
        Route::get('edit/{id}', 'Admin\UserController@edit')->name('edit');
        Route::post('update/{id}', 'Admin\UserController@update')->name('update');
        Route::get('change-password', 'Admin\UserController@getChangePassword')->name('change_password');
        Route::post('update-password', 'Admin\UserController@updatePassword')->name('update_password');
        Route::get('delete/{id}', 'Admin\UserController@destroy')->name('destroy');
    });

    // Manage category
    Route::group(['prefix' => 'category', 'as' => 'category.'], function () {
        Route::get('index', 'Admin\CategoryController@index')->name('index');
        Route::get('create', 'Admin\CategoryController@create')->name('create');
        Route::post('store', 'Admin\CategoryController@store')->name('store');
        Route::get('edit/{id}', 'Admin\CategoryController@edit')->name('edit');
        Route::post('update/{id}', 'Admin\CategoryController@update')->name('update');
        Route::get('delete/{id}', 'Admin\CategoryController@destroy')->name('destroy');
    });

    // Manage post
    Route::group(['prefix' => 'post', 'as' => 'post.'], function () {
        Route::get('index', 'Admin\PostController@index')->name('index');
        Route::get('create', 'Admin\PostController@create')->name('create');
        Route::post('store', 'Admin\PostController@store')->name('store');
        Route::get('edit/{id}', 'Admin\PostController@edit')->name('edit');
        Route::post('update/{id}', 'Admin\PostController@update')->name('update');
        Route::get('delete/{id}', 'Admin\PostController@destroy')->name('destroy');
        Route::post('delete-multiple', 'Admin\PostController@delete')->name('delete');
        Route::post('upload-image', 'Admin\PostController@uploadImage')->name('upload_image');
    });
});
