@extends('admin.layouts.admin')
@section('title') Quản lý bài viết @endsection
@section('content')
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Thêm mới bài viết</div>
                    <div class="panel-body">
                        <form id="formSubmit" action="{!! route('admin.post.store') !!}" method="POST" enctype="multipart/form-data" novalidate>
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <div class="checkbox-fade fade-in-primary">
                                        <label>
                                            <input type="checkbox" value="1" id="status-id" name="status">
                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                            <span>Hiển Thị</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group hidden">
                                    <div class="checkbox-fade fade-in-primary">
                                        <label>
                                            <input type="checkbox" value="1" id="hotnews-id" name="hotnews">
                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                            <span>Tin nổi bật</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group hidden">
                                    <div class="checkbox-fade fade-in-primary">
                                        <label>
                                            <input type="checkbox" value="1" id="care-id" name="care">
                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                            <span>Tin được quan tâm</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label">Tên bài viết</label>
                                            <input type="text" class="form-control @if($errors->first('name')) form-control-danger @endif"
                                                   value="{!! old('name') !!}" name="name" placeholder="Tên " id="nameRecord" onkeyup="generateSlug()"/>
                                            <div class="form-group has-error">
                                                <span class="help-block" id="error-name">{!! $errors->first('name') !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label">Liên kết</label>
                                            <input type="text" class="form-control"
                                                   value="{!! old('link') !!}" name="link" placeholder="Liên kết " required/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label">Thẻ tiêu đề SEO</label>
                                            <input type="text" class="form-control"
                                                   value="{!! old('meta_title') !!}" name="meta_title" placeholder="Thẻ tiêu đề SEO " required/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label">Thẻ mô tả SEO</label>
                                            <input type="text" class="form-control"
                                                   value="{!! old('meta_description') !!}" name="meta_description" placeholder="Thẻ mô tả SEO " required/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label">Thẻ từ khóa SEO</label>
                                            <input type="text" class="form-control"
                                                   value="{!! old('meta_keywords') !!}" name="meta_keywords" placeholder="Thẻ từ khóa SEO " required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label">Đường dẫn</label>
                                            <input type="text" class="form-control @if($errors->first('slug')) form-control-danger @endif"
                                                   value="{!! old('slug') !!}" name="slug" id="slugRecord" placeholder="abc-def-ghi" onchange="manualSlug()"/>
                                            <div class="form-group has-error">
                                                <span class="help-block" id="error-slug">{!! $errors->first('slug') !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label">Danh mục</label>
                                            <select class="form-control" style="height: auto !important" name="category_id">
                                                <option value="0">----- NONE -----</option>
                                                {!! \App\Helpers\Common::getCategoryOptions(0) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row hidden">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label">Thẻ tags</label>
                                            <input type="text" class="form-control"
                                                   value="{!! old('tags') !!}" name="tags" placeholder="VD: tag1, tag2, tag3" required/>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 form-group">
                                            <label class="control-label">Ảnh</label>
                                            <div class="">
                                                <label class="label p-0" data-toggle="tooltip" title="Click vào đây để tiến hành upload ảnh">
                                                    <img id="avatar" src="{!! asset('images/no-image.png') !!}" class="" width="150" height="100"/>
                                                    <input type="file" class="sr-only" id="input" name="img" accept="image/*">
                                                    <input type="hidden" value="" id="image_name" name="img_name"/>
                                                </label>
                                                <div class="alert" role="alert"></div>
                                                <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="modalLabel">Crop the image</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>

                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-6 col-xs-6 col-sm-6">
                                                                        <p><strong>Width: </strong> <span id="width">100 </span> px</p>
                                                                    </div>
                                                                    <div class="col-md-6 col-xs-6 col-sm-6">
                                                                        <p><strong>Height: </strong> <span id="height">50</span> px</p>
                                                                    </div>
                                                                </div>
                                                                <div class="img-container">
                                                                    <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                                                <button type="button" class="btn btn-primary" id="crop">Cắt</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label class="control-label">Mô tả trang</label>
                                    <textarea class="form-control" id="menuDescription" name="description" placeholder="">{!! old('description') !!}</textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label class="control-label">Nội dung</label>
                                    <textarea class="form-control" id="menuContent" name="content" placeholder="">{!! old('content') !!}</textarea>
                                </div>
                            </div>
                            <div class="row" style="margin-top:20px">
                                <div class="col-xs-12 form-group">
                                    <button type="submit" class="btn btn-success btn-luu" id="btnPrevent">Lưu</button>
                                    <div class="waitting-execute" id="loadingIcon" style="display: none;">
                                        <img src="{!! asset('images/Loading_icon.gif') !!}" style="width:100px;display:block;margin:0 auto;"/>
                                        <p style="text-align:center;color:red;">Vui lòng đợi website xử lý trong giây lát</p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        CKEDITOR.replace('menuDescription', options);
        CKEDITOR.replace('menuContent', options);
        //upload logo
        $("#img").change(function() {
            readURL(this, 'img');
        });

    </script>

    <script>
        window.addEventListener('DOMContentLoaded', function () {
            var avatar = document.getElementById('avatar');
            var image = document.getElementById('image');
            var input = document.getElementById('input');
            var $progress = $('.progress');
            var $progressBar = $('.progress-bar');
            var $alert = $('.alert');
            var $modal = $('#modal');
            var cropper;
            $('[data-toggle="tooltip"]').tooltip();
            input.addEventListener('change', function (e) {
                var files = e.target.files;
                var done = function (url) {
                    input.value = '';
                    image.src = url;
                    $alert.hide();
                    $modal.modal('show');
                };
                var reader;
                var file;
                var url;
                if (files && files.length > 0) {
                    file = files[0];

                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });
            $modal.on('shown.bs.modal', function () {
                cropper = new Cropper(image, {
                    // aspectRatio: 1,
                    viewMode: 3,
                    crop: function (e) {
                        var data = e.detail;
                        $('#width').text(Math.round(data.width));
                        $('#height').text(Math.round(data.height));
                    },
                });
            }).on('hidden.bs.modal', function () {
                cropper.destroy();
                cropper = null;
            });
            document.getElementById('crop').addEventListener('click', function () {
                var initialAvatarURL;
                var canvas;

                $modal.modal('hide');

                if (cropper) {
                    canvas = cropper.getCroppedCanvas({
                        //width: 160,
                        //height: 160,
                    });
                    initialAvatarURL = avatar.src;
                    avatar.src = canvas.toDataURL();
                    $progress.show();
                    $alert.removeClass('alert-success alert-warning');
                    canvas.toBlob(function (blob) {
                        var formData = new FormData();
                        formData.append('img', blob, 'post_image.jpg');
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax('{{route('admin.post.upload_image')}}', {
                            method: 'POST',
                            data: formData,
                            processData: false,
                            contentType: false,
                            xhr: function () {
                                var xhr = new XMLHttpRequest();
                                xhr.upload.onprogress = function (e) {
                                    var percent = '0';
                                    var percentage = '0%';

                                    if (e.lengthComputable) {
                                        percent = Math.round((e.loaded / e.total) * 100);
                                        percentage = percent + '%';
                                        $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                                    }
                                };
                                return xhr;
                            },
                            success: function (response) {
                                // console.log(response);
                                $alert.show().addClass('alert-success').text('Upload ảnh thành công');
                                $('#image_name').val(response);
                            },
                            error: function () {
                                avatar.src = initialAvatarURL;
                                $alert.show().addClass('alert-warning').text('Lỗi khi upload ảnh');
                            },

                            complete: function () {
                                $progress.hide();
                            },
                        });
                    });
                }
            });
        });
    </script>

@endsection