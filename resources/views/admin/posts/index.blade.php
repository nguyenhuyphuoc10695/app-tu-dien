@extends('admin.layouts.admin')
@section('title') Quản lý bài viết @endsection
@section('content')
    <div class="row">
        <div class="col-md-12 form-group">
            <a href="{!! route('admin.post.create') !!}" class="btn btn-success">Thêm mới bài viết</a>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            Danh sách bài viết

        </div>
        <div class="panel-body">

            @if (Session::has('flash_message'))
                <div class="alert alert-{{ Session::get('flash_level') }} icons-alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="icofont icofont-close-line-circled"></i>
                    </button>
                    <p><strong>{!! Session::get('flash_message') !!}</strong></p>
                </div>
            @endif
            <div class="row">
                <div class="col-md-7 form-group">
                    <form method="post" action="{!! route('admin.post.delete') !!}">
                        {!! csrf_field() !!}
                        <input type="hidden" value="" name="data_checkboxes" id="data_checkboxes">
                        <button type="submit" class="btn btn-sm btn-danger" style="margin-bottom: 15px;">Xóa lựa chọn</button>
                    </form>
                </div>

                <div class="col-md-5 form-group">
                    <form method="get" action="{!! route('admin.post.index') !!}">
                        <div class="col-md-9 form-group">
                            <input type="text" class="form-control" name="keyword" id="inputSearch" placeholder="Nhập từ khóa tìm kiếm....."/>
                        </div>
                        <div class="col-md-3 form-group" style="padding-left: 0px;">
                            <button class="btn btn-primary" type="submit"><i class="ti-search"></i> Tìm kiếm</button>
                        </div>
                    </form>
                </div>
            </div>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <td class="text-center text-vertical-align text-bold">
                        <input type="checkbox" id='check_all'/>
                    </td>
                    <td class="text-center text-vertical-align text-bold" width="100">
                        Cập nhật thứ tự
                    </td>
                    <td class="text-center text-vertical-align text-bold">Tên bài viết</td>
                    <td class="text-center text-vertical-align text-bold">Danh mục</td>
                    <td class="text-center text-vertical-align text-bold" width="150">Ảnh</td>
                    <td class="text-center text-vertical-align text-bold">Trạng thái</td>
                    <td class="text-center text-vertical-align text-bold">Ngày cập nhật</td>
                    <td class="text-center text-vertical-align text-bold">Thao tác</td>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $key => $item)
                <tr>
                    <td class="text-center text-vertical-align">
                        <input type="checkbox" value="{!! $item['id'] !!}" class="checkbox"/>
                    </td>
                    <td class="text-center text-vertical-align" width="100">
                        <input type="number" class="form-control" value="{!! $item['ord'] !!}"
                               id="updateOrder-{!! $item['id'] !!}" onchange="updateOrder({!! $item['id'] !!},'posts')"/>
                    </td>
                    <td class="text-center text-vertical-align" style="white-space:normal;text-align: left !important;">
                        {!! $item['name'] !!}
                    </td>
                    <td class="text-center text-vertical-align" style="white-space:normal;text-align: left !important;">
                        {!! \App\Helpers\Common::getCategoryByID($item['category_id'])['name'] !!}
                    </td>
                    <td width="150" class="text-center text-vertical-align">
                        @if(!is_null($item['img']))
                            <img class="img-responsive image-thumbnail" src="{!! asset('upload/images/_thumbs/thumb_') !!}{!! $item['img'] !!}"
                                 width="150"/>
                        @else
                            <img id="preview-img" src="{!! asset('images/no-image.png') !!}" class="" width="150" height="100"/>
                        @endif
                    </td>
                    <td class="text-center text-vertical-align">
                        @if($item['status'] == 1)
                            <span class="span-color" style="background: #2ab27b;color:white;padding: 5px;border-radius: 5px;cursor: pointer;"
                                  onclick="changeStatus({!! $item['status'] !!}, {!! $item['id'] !!}, 'posts')">Hiển thị</span>
                            @else
                            <span class="span-color" style="background: grey;color:white;padding: 5px;border-radius: 5px;cursor: pointer;"
                                  onclick="changeStatus({!! $item['status'] !!}, {!! $item['id'] !!}, 'posts')">Khóa</span>
                            @endif
                    </td>
                    <td class="text-center text-vertical-align">{!! $item['updated_at'] !!}</td>
                    <td class="text-center text-vertical-align">
                        <a href="{!! route('admin.post.edit', $item['id']) !!}"
                           class="btn btn-xs btn-primary">
                            Sửa
                        </a>
                        <a onclick="return confirmDelete('Bạn có chắc muốn xóa bản ghi này không?')"
                           href="{!! route('admin.post.destroy', $item['id']) !!}"
                           class="btn btn-xs btn-danger">
                            Xóa
                        </a>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
                {{ $posts->appends(\Request::all())->links() }}
        </div>
    </div>
    <div class="alert background-success announce">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="icofont icofont-close-line-circled text-white"></i>
        </button>
        <strong>Cập nhật trạng thái thành công</strong>
    </div>
    <div class="alert background-danger announce">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="icofont icofont-close-line-circled text-white"></i>
        </button>
        <strong>Cập nhật trạng thái thất bại</strong>
    </div>
    <style>
        .announce {
            display: none;
            position: fixed;
            top: 75px;
            right: 30px;
        }
        .alert button {
            margin-top: 0px !important;
            margin-left: 30px !important;
        }
    </style>
@endsection