@extends('admin.layouts.admin')
@section('title') Quản lý danh mục @endsection
@section('content')
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Thêm mới danh mục</div>
                    <div class="panel-body">
                        <form id="formSubmit" action="{!! route('admin.category.store') !!}" method="POST" enctype="multipart/form-data" novalidate>
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label">Tên danh mục</label>
                                            <input type="text" class="form-control @if($errors->first('name')) form-control-danger @endif"
                                                   value="{!! old('name') !!}" name="name" placeholder="Tên " id="nameRecord"/>
                                            <div class="form-group has-error">
                                                <span class="help-block" id="error-name">{!! $errors->first('name') !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label">Tên danh mục (Tiếng Đức)</label>
                                            <input type="text" class="form-control @if($errors->first('ge_name')) form-control-danger @endif"
                                                   value="{!! old('ge_name') !!}" name="ge_name" placeholder="Tên " id="geNameRecord" />
                                            <div class="form-group has-error">
                                                <span class="help-block" id="error-ge-name">{!! $errors->first('ge_name') !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <?php
                                                $parentCategories = \App\Http\Services\CategoryService::getCategoryByParent(0);
                                            ?>
                                            <label class="control-label">Danh mục</label>
                                            <select id="category" name="category_id" class="form-control" style="height: auto !important">
                                                <option value="0">-- None --</option>
                                                @foreach($parentCategories as $category)
                                                    <option value="{!! $category->id !!}">{!! $category->name !!}</option>
                                                    <?php
                                                    $childrenCategories = \App\Http\Services\CategoryService::getCategoryByParent($category->id);
                                                    ?>
                                                    @foreach($childrenCategories as $item)
                                                        <option value="{!! $item->id !!}">--{!! $item->name !!}</option>
                                                    @endforeach
                                                @endforeach
{{--                                                    @foreach($parentCategories as $category)--}}
{{--                                                        @if($category->has_children == 1)--}}
{{--                                                            <optgroup label="{!! $category->name !!}" class="group-{!! $category->id !!}">--}}
{{--                                                                <?php--}}
{{--                                                                $childrenCategories = \App\Http\Services\CategoryService::getCategoryByParent($category->id);--}}
{{--                                                                ?>--}}
{{--                                                                @foreach($childrenCategories as $item)--}}
{{--                                                                    <option value="{!! $item->id !!}">{!! $item->name !!}</option>--}}
{{--                                                                @endforeach--}}
{{--                                                            </optgroup>--}}
{{--                                                        @endif--}}
{{--                                                        <optgroup label="{!! $category->name !!}" class="group-{!! $category->id !!}">--}}
{{--                                                            <option value="{!! $category->id !!}">{!! $category->name !!}</option>--}}
{{--                                                        </optgroup>--}}
{{--                                                    @endforeach--}}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="1" id="status-id" name="status">
                                                    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                    <span>Hiển Thị</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <div class="checkbox-fade fade-in-primary">
                                                <label>
                                                    <input type="checkbox" value="1" id="has-children-id" name="has_children">
                                                    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                    <span>Chứa danh mục con</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 form-group">
                                            <label class="control-label">Ảnh</label>
                                            <input type="file" name="img" id="img">
                                            <div class="form-group has-error">
                                                <span class="help-block" id="error-img">{!! $errors->first('img') !!}</span>
                                            </div>
                                            <img id="preview-img" src="{!! asset('images/no-image.png') !!}" class="" width="150" height="100"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label class="control-label">Mô tả danh mục</label>
                                    <textarea class="form-control" id="menuContent" name="content" placeholder="">{!! old('content') !!}</textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label class="control-label">Mô tả danh mục (Tiếng Đức)</label>
                                    <textarea class="form-control" id="menuGeContent" name="ge_content" placeholder="">{!! old('ge_content') !!}</textarea>
                                </div>
                            </div>
                            <div class="row" style="margin-top:20px">
                                <div class="col-xs-12 form-group">
                                    <button type="submit" class="btn btn-success btn-luu" id="btnPrevent">Lưu</button>
                                    <div class="waitting-execute" id="loadingIcon" style="display: none;">
                                        <img src="{!! asset('images/Loading_icon.gif') !!}" style="width:100px;display:block;margin:0 auto;"/>
                                        <p style="text-align:center;color:red;">Vui lòng đợi website xử lý trong giây lát</p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        CKEDITOR.replace('menuGeContent', options);
        CKEDITOR.replace('menuContent', options);
        //upload logo
        $("#img").change(function() {
            readURL(this, 'img');
        });

    </script>

@endsection
