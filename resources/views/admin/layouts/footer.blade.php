
<!-- j-pro js -->
<script type="text/javascript" src="{{ asset('admin/js/jquery.ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/jquery.maskedinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/jquery-cloneya.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/autoNumeric.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/jquery.stepper.min.js') }}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{ asset('admin/js/jquery.slimscroll.js') }}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{ asset('admin/js/modernizr.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/css-scrollbars.js') }}"></script>
<!-- classie js -->
<script type="text/javascript" src="{{ asset('admin/js/classie.js') }}"></script>
<!-- i18next.min.js -->
<script type="text/javascript" src="{{ asset('admin/js/i18next.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/i18nextXHRBackend.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/i18nextBrowserLanguageDetector.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/jquery-i18next.min.js') }}"></script>
<!-- Custom js -->
<script type="text/javascript" src="{{ asset('admin/js/currency-form.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/script.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/common.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.5/cropper.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
