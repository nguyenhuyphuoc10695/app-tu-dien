<div class="main-menu">

    <div class="main-menu-content">
        <ul class="main-navigation">

            <li class="nav-title" data-i18n="nav.category.navigation">
                <i class="ti-line-dashed"></i>
                <span>Navigation</span>
            </li>
            <li class="nav-item @if(Route::currentRouteName() == 'admin.dashboard')has-class @endif">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="ti-home"></i>
                    <span data-i18n="">Dashboard</span>
                </a>
            </li>
            <li class="nav-item @if(Route::currentRouteName() == 'admin.user.index' || Route::currentRouteName() == 'admin.user.create'
            || Route::currentRouteName() == 'admin.user.edit')has-class @endif">
                <a href="{!! route('admin.user.index') !!}">
                    <i class="ti-user"></i>
                    <span data-i18n="nav.page_layout.main">Quản lý tài khoản</span>
                </a>
            </li>

            <li class="nav-item @if(Route::currentRouteName() == 'admin.category.index' || Route::currentRouteName() == 'admin.category.create'
            || Route::currentRouteName() == 'admin.category.edit')has-class @endif">
                <a href="{!! route('admin.category.index') !!}">
                    <i class="ti-menu-alt"></i>
                    <span data-i18n="nav.page_layout.main">Danh mục bài viết</span>
                </a>
            </li>

            <li class="nav-item @if(Route::currentRouteName() == 'admin.post.index' || Route::currentRouteName() == 'admin.post.create'
            || Route::currentRouteName() == 'admin.post.edit')has-class @endif">
                <a href="{!! route('admin.post.index') !!}">
                    <i class="ti-write"></i>
                    <span data-i18n="nav.page_layout.main">Quản lý bài viết</span>
                </a>
            </li>
        </ul>
    </div>
</div>
