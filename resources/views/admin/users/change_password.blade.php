@extends('admin.layouts.admin')
@section('title') Thông tin tài khoản @endsection
@section('content')
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Chỉnh sửa tài khoản</div>
                    <div class="panel-body">
                        @if (Session::has('flash_message'))
                            <div class="alert alert-{{ Session::get('flash_level') }} icons-alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="icofont icofont-close-line-circled"></i>
                                </button>
                                <p><strong>{!! Session::get('flash_message') !!}</strong></p>
                            </div>
                        @endif
                        <form id="formSubmit" action="{!! route('admin.user.update_password') !!}" method="POST" novalidate>
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label class="control-label">Tên</label>
                                    <input type="text" class="form-control @if($errors->first('name')) form-control-danger @endif"
                                           value="{!! $user['name'] ? $user['name'] : old('name') !!}" name="name" placeholder="Tên " id="nameRecord" onkeyup="generateSlug()"/>
                                    <div class="form-group has-error">
                                        <span class="help-block" id="error-name">{!! $errors->first('name') !!}</span>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label class="control-label">Mật khẩu cũ</label>
                                    <input type="password" class="form-control @if($errors->first('password')) form-control-danger @endif"
                                           value="" name="password" placeholder="********" required/>
                                    <div class="form-group has-error">
                                        <span class="help-block" id="error-password">{!! $errors->first('password') !!}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label class="control-label">Email</label>
                                    <input type="email" class="form-control @if($errors->first('email')) form-control-danger @endif" disabled
                                           value="{!! $user['email'] ? $user['email'] : old('email') !!}" name="email" placeholder="Ex: abc@gmail.com " required/>
                                    <div class="form-group has-error">
                                        <span class="help-block" id="error-email">{!! $errors->first('email') !!}</span>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label class="control-label">Nhập mật khẩu mới</label>
                                    <input type="password" class="form-control @if($errors->first('newPassword')) form-control-danger @endif"
                                           value="" name="newPassword" placeholder="********" required/>
                                    <div class="form-group has-error">
                                        <span class="help-block" id="error-newPassword">{!! $errors->first('newPassword') !!}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top:20px">
                                <div class="col-xs-12 form-group">
                                    <button type="submit" class="btn btn-success btn-luu" id="btnPrevent">Lưu</button>
                                    <div class="waitting-execute" id="loadingIcon" style="display: none;">
                                        <img src="{!! asset('images/Loading_icon.gif') !!}" style="width:100px;display:block;margin:0 auto;"/>
                                        <p style="text-align:center;color:red;">Vui lòng đợi website xử lý trong giây lát</p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection