@extends('admin.layouts.admin')
@section('title') Quản lý tài khoản @endsection
@section('content')
    <div class="row">
        <div class="col-md-12 form-group">
            @if(\Illuminate\Support\Facades\Auth::user()->level <= 2)
            <a href="{!! route('admin.user.create') !!}" class="btn btn-success">Thêm mới tài khoản</a>
                @endif
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            Danh sách tài khoản

        </div>
        <div class="panel-body">

            @if (Session::has('flash_message'))
                <div class="alert alert-{{ Session::get('flash_level') }} icons-alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="icofont icofont-close-line-circled"></i>
                    </button>
                    <p><strong>{!! Session::get('flash_message') !!}</strong></p>
                </div>
            @endif
            <div class="row">
                <div class="col-md-7"></div>
                <div class="col-md-5 form-group">
                    <form method="get" action="{!! route('admin.user.index') !!}">
                        <div class="col-md-9 form-group">
                            <input type="text" class="form-control" name="keyword" id="inputSearch" placeholder="Nhập từ khóa tìm kiếm....."/>
                        </div>
                        <div class="col-md-3 form-group" style="padding-left: 0px;">
                            <button class="btn btn-primary" type="submit"><i class="ti-search"></i> Tìm kiếm</button>
                        </div>
                    </form>
                </div>
            </div>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <td class="text-center text-vertical-align text-bold">STT</td>
                    <td class="text-center text-vertical-align text-bold">Tên tài khoản</td>
                    <td class="text-center text-vertical-align text-bold">Email</td>
                    <td class="text-center text-vertical-align text-bold">Vai trò</td>
                    <td class="text-center text-vertical-align text-bold">Trạng thái</td>
                    <td class="text-center text-vertical-align text-bold">Ngày tạo</td>
                    <td class="text-center text-vertical-align text-bold">Thao tác</td>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $key => $item)
                <tr>
                    <td class="text-center text-vertical-align" style="white-space:normal;text-align: center !important;">
                        {!! $key + 1 !!}
                    </td>
                    <td class="text-center text-vertical-align" style="white-space:normal;text-align: left !important;">
                        {!! $item['name'] !!}
                    </td>
                    <td class="text-center text-vertical-align">
                        {!! $item['email'] !!}
                    </td>
                    <td class="text-center text-vertical-align">
                        @php $k = $item['level'] - 1; @endphp
                        {!! config('role')[$k]['name'] !!}
                    </td>
                    <td class="text-center text-vertical-align">
                        @if($item['status'] == 1)
                            <span class="span-color" style="background: #2ab27b;color:white;padding: 5px;border-radius: 5px;cursor: pointer;"
                                  onclick="changeStatus({!! $item['status'] !!}, {!! $item['id'] !!}, 'users')">Kích hoạt</span>
                        @else
                            <span class="span-color" style="background: grey;color:white;padding: 5px;border-radius: 5px;cursor: pointer;"
                                  onclick="changeStatus({!! $item['status'] !!}, {!! $item['id'] !!}, 'users')">Khóa</span>
                        @endif
                    </td>
                    <td class="text-center text-vertical-align">{!! $item['created_at'] !!}</td>
                    <td class="text-center text-vertical-align">
                        <a href="{!! route('admin.user.edit', $item['id']) !!}"
                           class="btn btn-xs btn-primary">
                            Sửa
                        </a>
                        <a onclick="return confirmDelete('Bạn có chắc muốn xóa bản ghi này không?')"
                           href="{!! route('admin.user.destroy', $item['id']) !!}"
                           class="btn btn-xs btn-danger">
                            Xóa
                        </a>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $users->appends(\Request::all())->links() }}
        </div>
    </div>
    <div class="alert background-success announce">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="icofont icofont-close-line-circled text-white"></i>
        </button>
        <strong>Cập nhật trạng thái thành công</strong>
    </div>
    <div class="alert background-danger announce">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="icofont icofont-close-line-circled text-white"></i>
        </button>
        <strong>Cập nhật trạng thái thất bại</strong>
    </div>
    <style>
        .announce {
            display: none;
            position: fixed;
            top: 75px;
            right: 30px;
        }
        .alert button {
            margin-top: 0px !important;
            margin-left: 30px !important;
        }
    </style>
@endsection
