@extends('admin.layouts.admin')
@section('title') Quản lý tài khoản @endsection
@section('content')
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Chỉnh sửa tài khoản</div>
                    <div class="panel-body">
                        @if (Session::has('flash_message'))
                            <div class="alert alert-{{ Session::get('flash_level') }} icons-alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="icofont icofont-close-line-circled"></i>
                                </button>
                                <p><strong>{!! Session::get('flash_message') !!}</strong></p>
                            </div>
                        @endif
                        <form id="formSubmit" action="{!! route('admin.user.update', $user['id']) !!}" method="POST" novalidate>
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <div class="row">
                                        <div class="col-md-2 form-group">
                                            <div class="checkbox-fade fade-in-primary" style="margin-top: 20px;">
                                                <label>
                                                    <input type="checkbox" value="1" id="status-id" name="status" @if($user['status'] == 1) checked @endif>
                                                    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                    <span>Kích hoạt</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-10 form-group">
                                            <label class="control-label" style="color: #fff; display: block; width: 100%">Accepted Courses</label>
                                            <select id="courseAccepted" name="courses[]" multiple class="multiple-select-checkboxes">
                                                <optgroup label="Ngữ pháp cơ bản" class="group-1">
                                                    <option value="1-1" @if(in_array("1-1", json_decode($user['course']))) selected @endif>Option 1.1</option>
                                                    <option value="1-2" @if(in_array("1-2", json_decode($user['course']))) selected @endif>Option 1.2</option>
                                                    <option value="1-3" @if(in_array("1-3", json_decode($user['course']))) selected @endif>Option 1.3</option>
                                                    <option value="1-4" @if(in_array("1-4", json_decode($user['course']))) selected @endif>Option 1.4</option>
                                                    <option value="1-5" @if(in_array("1-5", json_decode($user['course']))) selected @endif>Option 1.5</option>
                                                </optgroup>
                                                <optgroup label="Tình huống hội thoại" class="group-2">
                                                    <option value="2-1" @if(in_array("2-1", json_decode($user['course']))) selected @endif>Option 2.1</option>
                                                    <option value="2-2" @if(in_array("2-2", json_decode($user['course']))) selected @endif>Option 2.2</option>
                                                    <option value="2-3" @if(in_array("2-3", json_decode($user['course']))) selected @endif>Option 2.3</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label class="control-label">Vai trò</label>
                                    <select class="form-control" style="height: auto !important" name="level">
                                        {!! \App\Helpers\Common::getRoleOptions(\Illuminate\Support\Facades\Auth::user()->level, $user['level']) !!}
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label class="control-label">Tên</label>
                                    <input type="text" class="form-control @if($errors->first('name')) form-control-danger @endif"
                                           value="{!! $user['name'] ? $user['name'] : old('name') !!}" name="name" placeholder="Tên " id="nameRecord" onkeyup="generateSlug()"/>
                                    <div class="form-group has-error">
                                        <span class="help-block" id="error-name">{!! $errors->first('name') !!}</span>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label class="control-label">Mật khẩu</label>
                                    <input type="password" class="form-control @if($errors->first('password')) form-control-danger @endif" disabled
                                           value="{!! $user['password'] ? $user['password'] : old('password') !!}" name="password" placeholder="********" required/>
                                    <div class="form-group has-error">
                                        <span class="help-block" id="error-password">{!! $errors->first('password') !!}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label class="control-label">Email</label>
                                    <input type="email" class="form-control @if($errors->first('email')) form-control-danger @endif" disabled
                                           value="{!! $user['email'] ? $user['email'] : old('email') !!}" name="email" placeholder="Ex: abc@gmail.com " required/>
                                    <div class="form-group has-error">
                                        <span class="help-block" id="error-email">{!! $errors->first('email') !!}</span>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label class="control-label">Nhập lại mật khẩu</label>
                                    <input type="password" class="form-control @if($errors->first('rePassword')) form-control-danger @endif" disabled
                                           value="{!! $user['password'] ? $user['password'] : old('rePassword') !!}" name="rePassword" placeholder="********" required/>
                                    <div class="form-group has-error">
                                        <span class="help-block" id="error-rePassword">{!! $errors->first('rePassword') !!}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top:20px">
                                <div class="col-xs-12 form-group">
                                    <button type="submit" class="btn btn-success btn-luu" id="btnPrevent">Lưu</button>
                                    <div class="waitting-execute" id="loadingIcon" style="display: none;">
                                        <img src="{!! asset('images/Loading_icon.gif') !!}" style="width:100px;display:block;margin:0 auto;"/>
                                        <p style="text-align:center;color:red;">Vui lòng đợi website xử lý trong giây lát</p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#courseAccepted').multiselect({
                nonSelectedText: 'Chọn khoá học được phép tham gia',
                enableClickableOptGroups: true,
                enableCollapsibleOptGroups: true,
                enableFiltering: true,
                includeSelectAllOption: true,
                buttonWidth: '100%',
                numberDisplayed: 5,
                nSelectedText: ' - Khoá học đã được chọn!',
                allSelectedText: 'Tất cả khoá học đã được chọn',
                maxHeight: 500,
            });
        });
    </script>
@endsection
