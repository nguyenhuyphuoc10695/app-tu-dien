<div class="td-category-pulldown-filter td-wrapper-pulldown-filter">
    <div class="td-pulldown-filter-display-option">
        <div class="td-subcat-more">Mới nhất <i class="td-icon-menu-down"></i></div>
        <ul class="td-pulldown-filter-list">
            <li class="td-pulldown-filter-item">
                <a class="td-pulldown-category-filter-link"
                   id="td_uid_2_5d1eac0b194ff"
                   data-td_block_id="td_uid_1_5d1eac0b1949c"
                   href="{!! url()->current() !!}?filter=1">Mới nhất</a>
            </li>
            <li class="td-pulldown-filter-item">
                <a class="td-pulldown-category-filter-link"
                   id="td_uid_3_5d1eac0b1954f"
                   data-td_block_id="td_uid_1_5d1eac0b1949c"
                   href="{!! url()->current() !!}?filter=2">Tin nổi bật</a>
            </li>
            <li class="td-pulldown-filter-item">
                <a class="td-pulldown-category-filter-link"
                   id="td_uid_4_5d1eac0b1959c"
                   data-td_block_id="td_uid_1_5d1eac0b1949c"
                   href="{!! url()->current() !!}?filter=3">Ngẫu nhiên</a>
            </li>
        </ul>
    </div>
</div>