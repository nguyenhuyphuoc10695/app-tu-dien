@extends('frontend.layouts.master_home')
@section('title', $category->meta_title)
@section('description', $category->meta_decription)
@section('keywords', $category->meta_keywords)
@section('image', $category->img)

@section('content')
    <div id="main" class="clearfix">
        <div class="inner-wrap clearfix">
            <div id="primary">
                <div id="content" class="clearfix">
                    <header class="page-header"><h1 class="page-title" style="border-bottom-color: #dd5a5a">
                            <span style="background-color: #dd5a5a">{!! $category->name !!}</span>
                        </h1>
                    </header>
                    <div class="article-container">
                        @foreach($postCategories as $key => $item)
                            @if($key == 0)
                                <article id="post-{!! $item['id'] !!}"
                                         class="post-{!! $item['id'] !!} post type-post status-publish format-standard has-post-thumbnail hentry category-entertainment category-female category-health category-sports">
                                    <div class="featured-image">
                                        <a href="{!! url('tin-tuc',$item['slug']) !!}.html"
                                           title="{!! $item['name'] !!}">
                                            <img width="800" height="445"
                                                 data-src="{!! url('upload/images',$item['img']) !!}"
                                                 src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                                                 class="attachment-colormag-featured-image size-colormag-featured-image wp-post-image"
                                                 alt="{!! $item['name'] !!}"
                                                 title="{!! $item['name'] !!}"/>
                                        </a>
                                    </div>
                                    <div class="article-content clearfix">
                                        <div class="above-entry-meta">
                                            <span class="cat-links">
                                                 <a href="{!! url('/',\App\Helpers\Common::getPostCategory($item['category_id'])['slug']) !!}.html" style="background-color:{!! config('color')[array_rand(config('color'))] !!}"
                                                    rel="category tag">{!! \App\Helpers\Common::getPostCategory($item['category_id'])['name'] !!}
                                                 </a>
                                            </span>
                                        </div>
                                        <header class="entry-header">
                                            <h2 class="entry-title">
                                                <a href="{!! url('tin-tuc',$item['slug']) !!}.html"
                                                    title="{!! $item['name'] !!}">{!! $item['name'] !!}
                                                </a>
                                            </h2>
                                        </header>
                                        <div class="below-entry-meta">
                                            <span class="posted-on">
                                                <a href="#" rel="bookmark">
                                                    <i class="fa fa-calendar-o"></i>
                                                    <time class="entry-date published" datetime="{!! $item['created_at'] !!}">
                                                                {!! \App\Helpers\Common::convertTime($item['created_at']) !!}
                                                            </time>
                                                </a>
                                            </span>
                                        </div>
                                        <div class="entry-content clearfix">
                                            {!! $item['description'] !!}
                                            <a class="more-link" title="{!! $item['name'] !!}"
                                               href="{!! url('tin-tuc',$item['slug']) !!}.html"><span>Xem chi tiết</span>
                                            </a>
                                        </div>
                                    </div>
                                </article>
                            @else
                                <article id="post-{!! $item['id'] !!}"
                                         class="post-{!! $item['id'] !!} post type-post status-publish format-standard has-post-thumbnail hentry category-general category-latest category-sports">
                                    <div class="featured-image">
                                        <a href="{!! url('tin-tuc',$item['slug']) !!}.html"
                                           title="{!! $item['name'] !!}">
                                            <img width="800" height="445"
                                                 data-src="{!! url('upload/images',$item['img']) !!}"
                                                 src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                                                 class="attachment-colormag-featured-image size-colormag-featured-image wp-post-image"
                                                 alt="{!! $item['name'] !!}"
                                                 title="{!! $item['name'] !!}"/>
                                        </a>
                                    </div>
                                    <div class="article-content clearfix">
                                        <div class="above-entry-meta">
                                            <span class="cat-links">
                                                <a href="{!! url('/',\App\Helpers\Common::getPostCategory($item['category_id'])['slug']) !!}.html" style="background-color:{!! config('color')[array_rand(config('color'))] !!}"
                                                   rel="category tag">{!! \App\Helpers\Common::getPostCategory($item['category_id'])['name'] !!}
                                                 </a>
                                            </span>
                                        </div>
                                        <header class="entry-header">
                                            <h2 class="entry-title">
                                                <a href="{!! url('tin-tuc',$item['slug']) !!}.html"
                                                   title="{!! $item['name'] !!}">{!! $item['name'] !!}
                                                </a>
                                            </h2>
                                        </header>
                                        <div class="below-entry-meta">
                                            <span class="posted-on">
                                                <a href="#" rel="bookmark">
                                                    <i class="fa fa-calendar-o"></i>
                                                     <time class="entry-date published" datetime="{!! $item['created_at'] !!}">
                                                                {!! \App\Helpers\Common::convertTime($item['created_at']) !!}
                                                            </time>
                                                </a>
                                            </span>
                                        </div>
                                        <div class="entry-content clearfix">
                                            {!! $item['description'] !!}
                                            <a class="more-link" title="{!! $item['name'] !!}"
                                               href="{!! url('tin-tuc',$item['slug']) !!}.html"><span>Xem chi tiết</span>
                                            </a>
                                        </div>
                                    </div>
                                </article>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <div id="secondary">
                @include('frontend.layouts.sidebar')
            </div>
        </div>
    </div>
@endsection