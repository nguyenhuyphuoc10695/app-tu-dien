<div class="td-category-siblings">
    <ul class="td-category">
        @foreach(\App\Helpers\Common::getCategories() as $item)
        <li class="entry-category">
            <a style="background-color:{!! config('color')[array_rand(config('color'))] !!};color:white;border-color:{!! config('color')[array_rand(config('color'))] !!};"
               class="td-current-sub-category" href="{!! url('category',$item['slug']) !!}.html">{!! $item['name'] !!}</a>
        </li>
        @endforeach
    </ul>
    <div class="td-subcat-dropdown td-pulldown-filter-display-option">
        <div class="td-subcat-more"><i class="td-icon-menu-down"></i></div>
        <ul class="td-pulldown-filter-list"></ul>
    </div>
    <div class="clearfix"></div>
</div>