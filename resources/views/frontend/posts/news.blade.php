@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_decription)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)

@section('content')
    <div class="page-content">
        <div data-jc="newsPage">
            @include('frontend.layouts.nav_post')
            <div class="container">
                <div class="post-list">
                    @if(count($posts) > 0)
                    <div class="row row flex-wrapper justify-content-center">
                        <div class="col-sm-11 no-padding-small"><h1 class="sr-only">{!! $pageContent->meta_title !!}</h1>
                            <ul class="list-unstyled">
                                @foreach($posts as $key => $item)
                                    @if($item['category_id'] == 1)
                                        <li class="post-item">
                                            <a class="" href="{!! url('tin-tuc',$item['slug']) !!}.html">
                                                <div class="post-item-tile">
                                                    <div class="post-item-content">
                                                        <div class="post-item-media"
                                                             style="background-image: url('{!! url('upload/images', $item['img']) !!}');"></div>
                                                        <div class="post-item-info ">
                                                            <div class="post-item-info-header">
                                                                <span class="post-info-category">{!! \App\Helpers\Common::getPostCategory($item['category_id'])['name'] !!}</span>&nbsp;|&nbsp;
                                                                <span class="post-info-datecreated">{!! date('F d, Y', strtotime($item['created_at'])) !!}</span>
                                                            </div>
                                                            <h2 class="post-item-info-title">{!! $item['name'] !!}</h2>
                                                            <p class="post-item-info-description">{!! $item['description'] !!}</p>
                                                            <p><span class="btn btn-primary btn-post"> {!! config('language')[\App\Helpers\Common::getLanguage()]['READ_MORE'] !!}</span></p></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    @elseif($item['category_id'] == 2)
                                        <li class="post-item">
                                            <a class="" href="{!! $item['link'] !!}"
                                               target="_blank" rel="noopener">
                                                <div class="post-item-tile">
                                                    <div class="post-item-content">
                                                        <div class="post-item-info full">
                                                            <div class="post-item-info-header">
                                                                <span class="post-info-category">{!! \App\Helpers\Common::getPostCategory($item['category_id'])['name'] !!}</span>&nbsp;|&nbsp;
                                                                <span class="post-info-datecreated">{!! date('F d, Y', strtotime($item['created_at'])) !!}</span>
                                                            </div>
                                                            <h2 class="post-item-info-title">{!! $item['name'] !!}</h2>
                                                            <p class="post-item-info-description">{!! $item['content'] !!}</p>
                                                            <p><span class="btn btn-primary btn-post"> {!! config('language')[\App\Helpers\Common::getLanguage()]['READ_MORE'] !!}</span></p></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    @else
                                        <li class="post-item">
                                            <a class="" href="#" data-toggle="modal" data-backdrop="static" data-target="#video_{!! $item['id'] !!}">
                                                <div class="post-item-tile">
                                                    <div class="post-item-content">
                                                        <div class="post-item-info full">
                                                            <div class="post-item-info-header">
                                                                <span class="post-info-category">{!! \App\Helpers\Common::getPostCategory($item['category_id'])['name'] !!}</span>&nbsp;|&nbsp;
                                                                <span class="post-info-datecreated">{!! date('F d, Y', strtotime($item['created_at'])) !!}</span>
                                                            </div>
                                                            <h2 class="post-item-info-title">{!! $item['name'] !!}</h2>
                                                            <p class="post-item-info-description">{!! $item['description'] !!}</p>
                                                            <p><span class="btn btn-primary btn-post">  {!! config('language')[\App\Helpers\Common::getLanguage()]['PLAY'] !!} </span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                            <div id="video_{!! $item['id'] !!}" data-jc="videopostmodal"
                                                 class="modal fade modal-video" tabindex="-1" role="dialog">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <img class="img-responsive"
                                                                 src="{!! asset('frontend/default/img/icn-close-modal.svg') !!}"
                                                                 alt="">
                                                        </button>
                                                        {!! $item['content'] !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @else
                        <div class="row" style="margin-top:30px;margin-bottom:100px" id="empty">
                            <div class="col-md-6 col-md-offset-3 m ui-center">
                                <div><span class="fa fa-search fa-3x"></span></div>
                                <br>
                                <div>{!! config('language')[\App\Helpers\Common::getLanguage()]['SEARCH_RESULT'] !!}</div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="pagination-news">
                    <nav>
                        @php $paginator = $posts @endphp
                        {!! $paginator->render('frontend.layouts.pagination') !!}
                        {{--@include('frontend.layouts.pagination')--}}
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <style>
        .post-item-info {
            font-weight: 100 !important;
            line-height: 1.7 !important;
        }
        iframe {
            width: 100% !important;
            height: 480px !important;
        }
    </style>
@endsection