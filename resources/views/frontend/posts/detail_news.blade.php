@extends('frontend.layouts.master')
@section('title', $post['meta_title'])
@section('description', $post['meta_description'])
@section('keywords', $post['meta_keywords'])
@section('image', $post['img'])
@section('pageName', 'news')
@section('pageID', $post['id'])

@section('content')

    <div id="title-banner" style="background: url({!! url('upload/images',\App\Helpers\Common::getCategoryByID($post['category_id'])['img']) !!}) center center no-repeat;background-size: cover;">
        <div class="contained middle">
            <h1>GreenP {!! \App\Helpers\Common::getCategoryByID($post['category_id'])['name'] !!}</h1>
        </div>
    </div>
    <div id="news-container" class="contained">
        <section id="news-l">

            <h3>{!! \App\Helpers\Common::getCategoryByID($post['category_id'])['name'] !!} </h3>
            <article id="article-3911">
                <h6><span>{!! date("F d, Y", strtotime($post['created_at'])); !!}</h6>
                <h2>{!! $post['name'] !!}</h2>
                <p>&nbsp;</p>
                {!! $post['content'] !!}
                <a class="blue-btn" role="button" href="{!! url('chi-tiet-tin', $postNext['slug']) !!}.html">Bài kế tiếp</a>
            </article>
        </section>

        @include('frontend.layouts.sidebar')
    </div>

    <!-- Service Footer -->
    @include('frontend.layouts.solution')

@endsection