@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_decription)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)

@section('content')
    <div class="page-content">
        <div class="who-we-are">
            <div>
                <section class="page-hero">
                    <div class="container">
                        <div class="row">
                            <picture>
                                <img class="img-responsive" src="{!! url('upload/images',$pageContent->img) !!}"
                                     alt="{!! $pageContent->meta_title !!}"
                                     title="{!! $pageContent->title !!}" />
                            </picture>
                        </div>
                    </div>
                </section>
                <section id="intro">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1"><br><br>
                                {!! $pageContent->content !!}
                            </div>
                        </div>
                    </div>
                </section>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <hr>
                        </div>
                    </div>
                </div>
                <section id="leadership">
                    <div class="container">
                        <div class="row">
                            @foreach(\App\Helpers\Common::getMenuChildren($pageContent->id, \App\Helpers\Common::getLang()) as $key => $item)
                                @if($key == 0)
                                    <div class="col-sm-10 col-sm-offset-1">
                                            <h2 class="title underline-full text-center"><span>{!! $item['name'] !!}</span></h2><br>
                                            <h3 class="text-center text-light">{!! $item['title'] !!}</h3>
                                            <br><br>
                                    </div>

                                    <div class="col-sm-12 no-padding">
                                        <div>
                                            <div class="leadership-block-list col-sm-12">
                                                <div class="row flex-wrapper justify-content-center flex-flow-row-wrap">
                                                    <div class="col-sm-8 ">
                                                        <div class="row flex-wrapper justify-content-center flex-flow-row-wrap">
                                                            @foreach(\App\Helpers\Common::getMenuChildren($item['id'], \App\Helpers\Common::getLang()) as $k => $val)
                                                                @if($k < 4)
                                                                    <div class="col-sm-6 col-md-6">
                                                                        <div class="leadership-item">
                                                                            <h4 class="leadership-name text-light">{!! $val['name'] !!}</h4>
                                                                            <p class="leadership-title">{!! $val['title'] !!}</p>
                                                                            {!! $val['content'] !!}
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="leadership-box-list col-sm-12">
                                                @foreach(\App\Helpers\Common::getMenuChildren($item['id'], \App\Helpers\Common::getLang()) as $k => $val)
                                                    @if($k > 3)
                                                        <div class="col-sm-4 col-md-3">
                                                            <div class="leadership-item-box" data-toggle="modal"
                                                                 data-target="#id{!! $val['id'] !!}">
                                                                <div class="leadership-item-content">
                                                                    <h4 class="leadership-name">{!! $val['name'] !!}</h4>
                                                                    <p class="leadership-title">{!! $val['title'] !!}</p>
                                                                </div>
                                                            </div>
                                                            <div id="id{!! $val['id'] !!}" class="modal modal-bio fade" tabindex="-1"
                                                                 role="dialog">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close"><i class="ccricon-icn-times"></i>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <h4 class="leadership-name">{!! $val['name'] !!}</h4>
                                                                            <p class="leadership-title">{!! $val['title'] !!}</p>
                                                                            {!! $val['content'] !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </section>
                @include('frontend.pages.common')
                <style>
                    @media (min-width: 768px) {
                        .who-we-are #leadership .leadership-item-box {
                            min-height: 200px;
                        }
                    }
                </style>
            </div>
        </div>
    </div>
@endsection