@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_description)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)
@section('pageName', 'about')
@section('pageID', $pageContent->id)

@section('content')

    <div id="title-banner" style="background: url({!! url('upload/images',$pageContent->image) !!}) center center no-repeat;background-size: cover;">
        <div class="contained middle">
            <h1>{!! $pageContent->title !!}</h1>
        </div>
    </div>
    <section>
        <!-- Intro -->
        <div id="intro" class="contained">
            {!! $pageContent->content !!}
        </div>

        <div id="about-overview" class="contained">
            @foreach($abouts as $key => $item)
                @if($key < 2)
                    <div class="col50">
                        <h3>{!! $item->name !!}</h3>
                        <h6>{!! $item->title !!}</h6>
                        {!! $item->content !!}
                    </div>
                @endif
            @endforeach
        </div>

        <div id="about-stats" class="contained">
            @foreach($abouts as $key => $item)
                @if($key == 2)
                    <div class="about-countries">
                        <h3><span id="info-countries" data-end-number="{!! $item->title !!}">0</span></h3>
                        <p>{!! $item->name !!}</p>
                    </div>
                @elseif($key == 3)
                    <div class="about-units">
                        <h3><span id="info-systems" data-end-number="{!! $item->title !!}">0</span></h3>
                        <p>{!! $item->name !!}</p>
                    </div>
                @elseif($key == 4)
                    <div class="about-experience">
                        <h3><span id="info-experience" data-end-number="{!! $item->title !!}">0</span><span class="yrs"><sub>YRS</sub></span></h3>
                        <p>{!! $item->name !!}</p>
                    </div>
                @endif
            @endforeach
        </div>
    </section>

    <section>
        <div id="executive-team" class="contained">
            <h3>Đội ngũ điều hành</h3>
            <!-- Service Item -->
            @foreach($about_list as $item)
                <div class="contained col-padding">
                <div class="col-l">
                    <img src="{!! url('upload/images', $item->img) !!}">
                </div>
                <div class="col-r">
                    <h4>{!! $item->name !!}</h4>
                    <h5>{!! $item->title !!}</h5>
                    {!! $item->content !!}
                </div>
            </div>
            @endforeach
            <!-- Service Item -->
        </div>
    </section>

    <!-- Service Footer -->
    @include('frontend.layouts.solution')

@endsection