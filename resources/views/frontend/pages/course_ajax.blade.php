@if(count($courses) > 0)
    @foreach($courses->chunk(3) as $course)
        <div class="row grid">
            @foreach($course as $key => $item)
                <div class="col-lg-4 col-md-6 grid-item filter1">
                    <div class="course-item">
                        <div class="course-img">
                            <img src="{!! url('upload/images',$item->img) !!}" alt="{!! $item->name !!}" />
                            <span class="course-value">{!! number_format($item->price) !!}</span>
                            <div class="course-toolbar">
                                <h4 class="course-category">
                                    <a href="{!! url('category',\App\Helpers\Common::getCategoryByID($item->category_id)['slug']) !!}.html">
                                        {!! \App\Helpers\Common::getCategoryByID($item->category_id)['name'] !!}
                                    </a>
                                </h4>
                                <div class="course-date">
                                    <i class="fa fa-calendar"></i> {!! date('d-m-Y', strtotime($item->created_at)) !!}
                                </div>
                                <div class="course-duration">
                                    <i class="fa fa-clock-o"></i> {!! $item->time !!} year
                                </div>
                            </div>
                        </div>
                        <div class="course-body">
                            <div class="course-desc">
                                <h4 class="course-title">
                                    <a href="{!! url('course',$item->slug) !!}.html">{!! $item->name !!}</a>
                                </h4>
                                {!! $item->description !!}
                            </div>
                        </div>
                        <div class="course-footer">
                            <div class="course-seats">
                                <i class="fa fa-users"></i> {!! $item->slot !!} SEATS
                            </div>
                            <div class="course-button">
                                <a href="{!! $item->link !!}">APPLY NOW</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach

    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item">
                <a class="page-link fa fa-angle-left" href="javascript:void(0)" @if (!$courses->onFirstPage()) onclick="getDataPrev({!! $category !!})" @endif tabindex="-1"></a>
            </li>
            <li class="page-item"><a class="page-link fa fa-angle-right" href="javascript:void(0)" @if ($courses->hasMorePages()) onclick="getDataNext({!! $category !!})" @endif></a></li>
        </ul>
    </nav>
@endif