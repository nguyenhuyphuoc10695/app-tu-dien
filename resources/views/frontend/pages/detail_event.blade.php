@extends('frontend.layouts.master')
@section('title', $event['meta_title'])
@section('description', $event['meta_description'])
@section('keywords', $event['meta_keywords'])
@section('image', $event['img'])

@section('content')

    <!-- Event Details Start -->
    <div class="rs-event-details pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="event-details-content">
                        <h3 class="event-title"><a href="#">{!! $event['name'] !!}</a></h3>
                        <div class="event-meta">
                            <div class="event-date">
                                <i class="fa fa-calendar"></i>
                                <span>{!! date('F d, Y', strtotime($event['created_at'])) !!}</span>
                            </div>
                            <div class="event-time">
                                <i class="fa fa-clock-o"></i>
                                <span>{!! $event['duration'] !!}</span>
                            </div>
                            <div class="event-location">
                                <i class="fa fa-map-marker"></i>
                                <span>{!! $event['address'] !!}</span>
                            </div>
                        </div>

                        <div class="event-desc">
                            {!! $event['content'] !!}
                        </div>
                        <div class="share-area">
                            <div class="row rs-vertical-middle">
                                <div class="col-md-4">
                                    <div class="book-btn">
                                        <a href="{!! $event['link'] !!}">ỨNG TUYỂN NGAY</a>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="share-inner">
                                        <span>CHIA SẺ:</span>
                                        <a href="http://www.facebook.com/sharer.php?u={!! url()->current() !!}"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/share?url={!! url()->current() !!}&amp;text=Big%Language%20&amp;hashtags=biglanguage"><i class="fa fa-twitter"></i></a>
                                        <a href="https://plus.google.com/share?url={!! url()->current() !!}"><i class="fa fa-google"></i></a>
                                        {{--<a href="#"><i class="fa fa-pinterest-p"></i></a>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('frontend.layouts.sidebar')
            </div>
        </div>
    </div>
    <!-- Event Details End -->

    <!-- Branches Start -->
    @include('frontend.layouts.branch')
    <!-- Branches End -->

    <!-- Partner Start -->
    @include('frontend.layouts.partner')
    <!-- Partner End -->

    <style>
        .entry-content img {
            margin: 0 auto !important;
            width: 100% !important;
            display: block;
            filter: grayscale(0%);
            transition: all 0.8s ease-in-out 0s;
        }

        .entry-content img:hover {
            filter: grayscale(100%);
            transform: scale(1.1);
        }

        @media screen and (min-width: 600px) {

        }

        @media screen and (min-width: 767px) {

        }
    </style>
@endsection