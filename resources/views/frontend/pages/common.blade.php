<section id="other-pages">
    <div class="container">
        <div class="row ">
            @foreach(\App\Helpers\Common::getMenuByID([18,20], \App\Helpers\Common::getLang()) as $item)
            <div class="page-showcase-wrapper col-sm-6 flex-wrapper align-items-center justify-content-center"
                 style="background-image: url('{!! url('upload/images', $item['image']) !!}');">
                <div class="page-showcase text-background-color-white">
                    <h4 class="title text-color-white underline-full text-center">
                        <span>{!! $item['name'] !!}</span>
                    </h4>
                    {!! $item['description'] !!}
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>