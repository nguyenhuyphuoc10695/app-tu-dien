@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_description)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)
@section('pageName', 'news')
@section('pageID', $pageContent->id)

@section('content')

    <div id="title-banner" style="background: url({!! url('upload/images',$pageContent->image) !!}) center center no-repeat;background-size: cover;">
        <div class="contained middle">
            <h1>{!! $pageContent->title !!}</h1>
        </div>
    </div>
    <div id="news-container" class="contained">
        <section id="news-l">
            <h3>Tin mới</h3>
            @foreach($news as $key => $item)
                <article id="article-{!! $item->id !!}">
                <h6><span>{!! \App\Helpers\Common::getCategoryByID($item->category_id)['name'] !!} </span>
                    {!! date("F d, Y", strtotime($item->created_at)); !!}
                </h6>
                <h2>
                    <a href="{!! url('chi-tiet-tin', $item->slug) !!}.html"
                       title="{!! $item->name !!}">
                        {!! $item->name !!}
                    </a>
                </h2>
                {!! $item->description !!}
                <div class="row">
                    <div class="container pad-10-t">
                        <a class="blue-btn" role="button"
                           href="{!! url('chi-tiet-tin', $item->slug) !!}.html">Xem thêm</a>
                    </div>
                </div>
            </article>
            @endforeach
        </section>

        @include('frontend.layouts.sidebar')
    </div>

    <!-- Service Footer -->
    @include('frontend.layouts.solution')

@endsection