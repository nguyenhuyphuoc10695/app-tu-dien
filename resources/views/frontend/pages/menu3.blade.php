@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_decription)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)

@section('content')
    <div class="page-content">
        <div class="communities">
            <div>
                <section class="page-hero">
                    <div class="container">
                        <div class="row">
                            <picture>
                                <img class="img-responsive" src="{!! url('upload/images',$pageContent->image) !!}"
                                     alt="{!! $pageContent->meta_title !!}"
                                     title="{!! $pageContent->title !!}" />
                            </picture>
                        </div>
                    </div>
                </section>
            </div>

            <div class="container ">
                <div class="row flex-wrapper justify-content-center flex-flow-row-wrap">
                    <div class="col-sm-7 h1-light"><br>
                        {!! $pageContent->content !!}
                    </div>
                    <div class="col-xs-11">
                        <hr>
                    </div>
                    @foreach(\App\Helpers\Common::getMenuChildren($pageContent->id, \App\Helpers\Common::getLang()) as $key => $item)
                        @if( $key == 0)
                            <div class="row flex-wrapper justify-content-space-between align-items-center">
                                <div class="col-sm-5 col-md-5">
                                    <div>
                                        <div class="img-wrapper-full">
                                            <picture>
                                                <img class="img-responsive" src="{!! url('upload/images',$item['img']) !!}"
                                                     alt="{!! $item['meta_title'] !!}"
                                                     title="{!! $item['meta_title'] !!}" />
                                            </picture>
                                        </div>
                                        <br></div>
                                </div>
                                <div class="col-sm-7 col-md-5">
                                    <div>
                                        <h3 class="subtitle text-light">{!! $item['name'] !!}</h3>
                                        {!! $item['content'] !!}
                                    </div>
                                </div>
                             </div>
                            <div class="col-xs-11">
                                <hr>
                            </div>

                        @elseif( $key == 1)
                            {!! 2333 !!}
                            <div class="col-sm-12 flex-wrapper justify-content-center flex-flow-row-wrap">
                                <div class="col-sm-7 col-md-6 col-lg-5">
                                    <h2 class="title underline-full text-center"><span>{!! $item['title'] !!}</span></h2><br>
                                    <h3 class="subtitle text-center text-light">{!! $item['name'] !!}</h3><br><br>
                                </div>
                                <div class="col-sm-12 flex-wrapper flex-flow-row-wrap justify-content-space-between">
                                    @foreach(\App\Helpers\Common::getMenuChildren($item['id'], \App\Helpers\Common::getLang()) as $k => $val)
                                        <div class="col-sm-6 col-md-4">
                                            <div class="cycle-item">
                                                <div class="img-wrapper-full">
                                                    <picture>
                                                        <img class="img-responsive" src="{!! url('upload/images',$val['img']) !!}"
                                                             alt="{!! $val['meta_title'] !!}"
                                                             title="{!! $val['meta_title'] !!}"/>
                                                    </picture>
                                                </div>
                                                {!! $val['content'] !!}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @elseif( $key == 2)
                            <div class="good-neighbors flex-wrapper">
                                <div class="col-lg-6 no-padding">
                                    <div class="img-wrapper-full">
                                        <picture>
                                            <img class="img-responsive" src="{!! url('upload/images',$item['img']) !!}"
                                                 alt="{!! $item['meta_title'] !!}"
                                                 title="{!! $item['meta_title'] !!}"/>
                                        </picture>
                                    </div>
                                </div>
                                <div class="col-lg-6 bg-color-main-gradient flex-wrapper align-items-center justify-content-center">
                                    <div class="content-wrapper col-sm-8 col-md-11 col-lg-10">
                                        <div><br></div>
                                        <div><br></div>
                                        <h2 class="title underline-full text-color-white"><span>{!! $item['title'] !!}</span></h2><br>
                                        <h3 class="subtitle text-color-white">{!! $item['name'] !!}</h3>
                                        <div class="text-background-color-white">
                                            {!! $item['content'] !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif( $key > 2 && $key < 6)
                            <div class="col-sm-8"><br><br><br>
                                <h2 class="title underline-full text-center"><span>{!! $item['title'] !!}</span>
                                </h2>
                                <h3 class="subtitle text-center">{!! $item['name'] !!}</h3><br>
                                <div class="content-image">
                                    {!! $item['content'] !!}
                                </div>
                            </div>
                            @if($key == 5)
                                <div class="col-xs-7 flex-wrapper justify-content-center">
                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                        <hr class="line-separator-short">
                                    </div>
                                </div>
                            @endif
                        @elseif( $key == 6)
                            <div class="col-sm-11 flex-wrapper flex-flow-row-wrap justify-content-center">
                                <div class="row flex-wrapper justify-content-center">
                                    <div class="col-sm-8"><h3 class="subtitle_2 text-center">{!! $item['name'] !!}</h3><br><br>
                                    </div>
                                </div>
                                @foreach(\App\Helpers\Common::getMenuChildren($item['id'], \App\Helpers\Common::getLang()) as $k => $val)
                                    <div class="col-sm-4">
                                        <div class="partnerships-item">
                                                <div class="img-wrapper-full">
                                                    <picture>
                                                        <img class="img-responsive" src="{!! url('upload/images',$val['img']) !!}"
                                                             alt="{!! $val['meta_title'] !!}"
                                                             title="{!! $val['meta_title'] !!}"/>
                                                    </picture>
                                                </div>
                                                {!! $val['content'] !!}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-xs-11">
                                <hr>
                            </div>
                        @elseif( $key > 6 && $key < 9)
                            <div class="col-sm-8"><br><br><br>
                                <h2 class="title underline-full text-center"><span>{!! $item['title'] !!}</span>
                                </h2>
                                <h3 class="subtitle text-center">{!! $item['name'] !!}</h3><br>
                                <div class="content-image">
                                    {!! $item['content'] !!}
                                </div>
                            </div>
                        @elseif( $key == 9)
                            <div class="col-sm-11 col-md-9">
                                <div class="banner-bee" style="background-image: url('{!! url('upload/images', $item['image']) !!}');">
                                    <p class="text-color-white">{!! $item['name'] !!}</p>
                                    <a href="{!! $item['title'] !!}"
                                            class="btn btn-primary"
                                            title="{!! $item['name'] !!}">{!! config('language')[\App\Helpers\Common::getLanguage()]['READ_MORE'] !!}</a>
                                </div>
                            </div>
                        @elseif( $key > 9 && $key < 12)
                            <div class="col-sm-11"><br><br><br>
                                <h2 class="title underline-full text-center"><span>{!! $item['title'] !!}</span>
                                </h2>
                                <h3 class="subtitle text-center">{!! $item['name'] !!}</h3><br>
                                <div class="content-image">
                                    {!! $item['content'] !!}
                                </div>
                            </div>
                        @elseif( $key == 12)
                            <div class="col-sm-11 col-md-9">
                                <h3 class="subtitle text-center" style="margin-top: 50px;">{!! $item['name'] !!}</h3>
                            </div>
                        @endif
                    @endforeach


                </div>
            </div>
            <section id="other-pages">
                <div class="container">
                    <div class="row ">
                        @foreach(\App\Helpers\Common::getRegionChildren(0, \App\Helpers\Common::getLang()) as $key => $item)
                            @if($key == 0)
                                <div class="page-showcase-wrapper col-sm-6 flex-wrapper align-items-center justify-content-center"
                                     style="background-image: url('{!! url('upload/images', $item['img']) !!}');">
                                    <div class="page-showcase col-sm-12 col-md-11 text-background-color-white">
                                        <h4 class="title text-color-white underline-full text-center"><span>{!! $item['name'] !!}</span>
                                        </h4>
                                        {!! $item['description'] !!}
                                        <div class="row" data-jc="selectDownload">
                                            <div class="col-sm-7 col-md-8"><br class="visible-xs">
                                                <select class="selectpicker jsSelectAsset" data-size="integer"
                                                        aria-label="{!! $item['name'] !!}">
                                                    <option selected="" disabled="">Choose Your Region</option>
                                                    @foreach(\App\Helpers\Common::getRegionChildren($item['id'], \App\Helpers\Common::getLang()) as $k => $val)
                                                        <option value="{!! url('upload/files', $val['file']) !!}">{!! $val['name'] !!}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-5 col-md-4 no-padding text-center"><br class="visible-xs"><a href="#"
                                                                                                                            class="btn btn-primary"
                                                                                                                            rel="noopener"
                                                                                                                            data-jc="pdfdownload">Download</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="page-showcase-wrapper col-sm-6 flex-wrapper align-items-center justify-content-center"
                                     style="background-image: url('{!! url('upload/images', $item['img']) !!}');">
                                    <div class="page-showcase col-sm-11 text-background-color-white"><h4
                                                class="title text-color-white underline-full text-center"><span>{!! $item['name'] !!}</span></h4>
                                        {!! $item['description'] !!}
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
    </div>
    <style>
        .h1-light h1 {
            font-weight: 100;
        }
        .content-image img {
            width: 100%;
        }
    </style>
@endsection