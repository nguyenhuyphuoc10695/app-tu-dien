@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_decription)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)

@section('content')
    <div class="page-content">
        <div>
            <section class="page-hero">
                <div class="container">
                    <div class="row">
                        <div class="img-wrapper-full">
                            <picture>
                                <img class="img-responsive" src="{!! url('upload/images',$pageContent->img) !!}"
                                     alt="{!! $pageContent->meta_title !!}"
                                     title="{!! $pageContent->title !!}" />
                            </picture>
                        </div>
                    </div>
                </div>
            </section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1"><br>
                        <h1 class="title underline-full text-center"><span>{!! $pageContent->title !!}</span></h1>
                        <br><br>
                    </div>
                </div>
                <div id="faqpage" data-jc="faqnavpage">
                    <section class="faq-questions">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <ul class="list-unstyled">
                                    @foreach(\App\Helpers\Common::getMenuChildren($pageContent->id, \App\Helpers\Common::getLang()) as $key => $item)
                                        <li><a href="#">{!! $item['name'] !!}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </section>
                    <hr>
                    <section class="faq-answers">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                @foreach(\App\Helpers\Common::getMenuChildren($pageContent->id, \App\Helpers\Common::getLang()) as $key => $item)
                                    <article id="">
                                    <h2>{!! $item['name'] !!}</h2>
                                        {!! $item['content'] !!}
                                    </article>
                                @endforeach
                            </div>
                        </div>
                    </section>
                </div>
                <br><br>
                @include('frontend.pages.common')
            </div>
        </div>


    </div>
@endsection