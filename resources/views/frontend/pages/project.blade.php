@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_description)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)
@section('pageName', 'case-studies')
@section('pageID', $pageContent->id)

@section('content')

    <div id="title-banner" style="background: url({!! url('upload/images',$pageContent->image) !!}) center center no-repeat;background-size: cover;">
        <div class="contained middle">
            <h1>{!! $pageContent->title !!}</h1>
        </div>
    </div>
    <section>
        @foreach($projects as $key => $item)
            <article class="case-study contained" id="project-{!! $item->id !!}">
                <div class="case-study-img-gal">
                    <a class="blue-btn casestudy-view-more" role="button" id="btn{!! $item->id !!}" data-gallery-id="{!! $item->id !!}"
                       data-gallery-type="photo">Ảnh chi tiết</a>
                    <a>
                        <div class="blue-hover"></div>
                    </a>
                    @if(json_decode($item->img))
                        <img src="{!! url('upload/images',json_decode($item->img)[0]) !!}">
                        @endif
                </div>
                <div class="case-study-content">
                    <h2>{!! $item->name !!}</h2>
                    {!! $item->content !!}
                    <div class="case-study-col">
                        <ul class="first">
                            <li><span class="font-bold">Configuration:</span> {!! $item->configuration !!}</li>
                            <li><span class="font-bold">Levels:</span> {!! $item->level !!}</li>
                            <li><span class="font-bold">Number of spaces:</span> {!! $item->spaces !!}</li>
                        </ul>
                    </div>
                    <div class="case-study-col">
                        <ul>
                            <li><span class="font-bold">Developer:</span> {!! $item->developer !!}</li>
                            <li><span class="font-bold">Building Type:</span> {!! \App\Helpers\Common::getSolutionByID($item->type)['name'] !!}</li>
                            <li><span class="font-bold">Completion Date:</span> {!! $item->year !!}</li>
                        </ul>
                    </div>
                </div>
                <script>
                    var slideArr{!! $item->id !!} = {!! json_encode($images[$key]) !!};
                </script>
            </article>
        @endforeach
    </section>

    <script>
        var galleryBtn = jQuery(".casestudy-view-more");
    </script>

    <div class="gallery-overlay">
        <div class="gallery-overlay-close">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve">

            <polygon
                    points="45.8,16.3 43.7,14.2 30,27.9 16.3,14.2 14.2,16.3 27.9,30 14.2,43.7 16.3,45.8 30,32.1 43.7,45.8 45.8,43.7 32.1,30 "/>
        </svg>
        </div>
        <div class="gallery-overlay-topper"></div>
        <div class="gallery-overlay-prev">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 57 47" style="enable-background:new 0 0 57 47;" xml:space="preserve">
            <rect x="1.2" y="1.1" class="button" width="55" height="45"/>
                <polygon class="arrow"
                         points="26.5,15.4 27.6,16.4 21.3,22.9 37.2,22.9 37.2,15.9 38.7,15.9 38.7,31.4 37.2,31.4 37.2,24.4 21.3,24.4 27.6,30.9 26.5,31.9 18.4,23.6"/>
        </svg>
        </div>
        <div class="gallery-overlay-next">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 57 47" style="enable-background:new 0 0 57 47;" xml:space="preserve">
            <rect x="1.2" y="1.1" class="button" width="55" height="45"/>
                <polygon class="arrow"
                         points="30.9,15.4 29.9,16.4 36.2,22.9 20.2,22.9 20.2,15.9 18.7,15.9 18.7,31.4 20.2,31.4 20.2,24.4 36.2,24.4 29.9,30.9 30.9,31.9 39,23.6"/>
        </svg>
        </div>
        <div class="gallery-overlay-padding"></div>
        <div class="gallery-overlay-slides"></div>
        <div class="gallery-overlay-video"></div>
        <div class="gallery-overlay-nav">
            <ul></ul>
        </div>
    </div>
    <!-- Contact an Expert (Global) -->
    <div id="contact-expert" class="contained tble">
        <div class="td-50">
            <p>Liên hệ với chúng tôi nếu bạn thấy dịch vụ của chúng tôi hữu ích </p>
        </div>
        <div class="td-50">
            <a class="blue-btn" href="{!! url('page/lien-he') !!}.html">Liên hệ</a>
        </div>
    </div>
@endsection