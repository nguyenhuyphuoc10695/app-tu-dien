@extends('frontend.layouts.master')
@section('title', $solution['meta_title'])
@section('description', $solution['meta_description'])
@section('keywords', $solution['meta_keywords'])
@section('image', $solution['img'])
@section('pageName', 'page-template page-template-template-solutions-template page-template-template-solutions-template-php
 page page-parent page-child parent-pageid-26 puzzle-mechanical-parking')
@section('pageID', $solution['id'])

@section('content')

    <section>
        <div id="intro" class="contained">
            <div class="solution-icon">
                <img src="{!! asset('frontend//wp-content/uploads/2015/08/stacker-light.svg') !!}">
            </div>
            <div class="solution-intro">
                {!! $solution['content'] !!}
            </div>
            <div class="solution-stats">
                <ul>
                    <li><span class="font-bold">Average Retrieval Time:</span> {!! $solution['time'] !!} Seconds</li>
                    <li><span class="font-bold">Levels:</span> {!! $solution['level'] !!}</li>
                    <li><span class="font-bold">Spaces Installed:</span> {!! number_format($solution['space']) !!}</li>
                </ul>

                <a href="{!! url('upload/files', $solution['file']) !!}" download class="btn-spec-sheet">Download
                    the Spec Sheet</a>


                <ul class="dwg_plan">

                    <li class="solutions-cta"><br/><a href="{!! url('page/lien-he') !!}.html" class="blue-btn">Gửi liên hệ</a></li>
                </ul>

            </div>
        </div>
    </section>

    <div id="vid-carousel" class="owl-carousel owl-theme">
        @foreach(json_decode($solution['img']) as $img)
            <div class="item">
                <img src="{!! url('upload/images', $img) !!}">
            </div>
        @endforeach
    </div>

    @foreach($projects as $key => $item)
        @if($key == 0)
            <section>
                <div class="case-study contained">
                    <div class="case-study-img-gal">
                        <a class="blue-btn casestudy-view-more" role="button" id="btn{!! $item->id !!}" data-gallery-id="{!! $item->id !!}"
                           data-gallery-type="photo">Ảnh chi tiết</a>
                        <a>
                            <div class="blue-hover"></div>
                        </a>
                        @if(json_decode($item->img))
                            <img src="{!! url('upload/images',json_decode($item->img)[0]) !!}">
                        @endif
                    </div>
                    <div class="case-study-content">
                        <h2>{!! $item->name !!}</h2>
                        {!! $item->content !!}
                        <div class="case-study-col">
                            <ul class="first">
                                <li><span class="font-bold">Configuration:</span> {!! $item->configuration !!}</li>
                                <li><span class="font-bold">Levels:</span> {!! $item->level !!}</li>
                                <li><span class="font-bold">Number of spaces:</span> {!! $item->spaces !!}</li>
                            </ul>
                        </div>
                        <div class="case-study-col">
                            <ul>
                                <li><span class="font-bold">Developer:</span> {!! $item->developer !!}</li>
                                <li><span class="font-bold">Building Type:</span> {!! \App\Helpers\Common::getSolutionByID($item->type)['name'] !!}</li>
                                <li><span class="font-bold">Completion Date:</span> {!! $item->year !!}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Featured Property Gallery -->
            <script>
                var slideArr{!! $item->id !!} = {!! json_encode($images[$key]) !!};
                var galleryBtn = jQuery(".casestudy-view-more");

            </script>
        @endif
    @endforeach

    <section>
        <div id="other-properties" class="contained">
            <div id="properties-title">Dự án khác</div>
            <div id="properites-carousel">
                @foreach($projects as $key => $item)
                    @if($key > 0)
                        <div class="child-item">
                            <a href="{!! url('page/du-an') !!}.html#project-{!! $item->id !!}">
                                <div class="blue-hover-case-properties">
                                    <h4>{!! $item->name !!}</h4>
                                    <p>{!! $item->address !!}</p>
                                </div>
                                @if(json_decode($item->img))
                                    <img src="{!! url('upload/images',json_decode($item->img)[0]) !!}">
                                @endif
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>

    <div class="gallery-overlay">
        <div class="gallery-overlay-close">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve">

            <polygon
                    points="45.8,16.3 43.7,14.2 30,27.9 16.3,14.2 14.2,16.3 27.9,30 14.2,43.7 16.3,45.8 30,32.1 43.7,45.8 45.8,43.7 32.1,30 "/>
        </svg>
        </div>
        <div class="gallery-overlay-topper"></div>
        <div class="gallery-overlay-prev">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 57 47" style="enable-background:new 0 0 57 47;" xml:space="preserve">
            <rect x="1.2" y="1.1" class="button" width="55" height="45"/>
                <polygon class="arrow"
                         points="26.5,15.4 27.6,16.4 21.3,22.9 37.2,22.9 37.2,15.9 38.7,15.9 38.7,31.4 37.2,31.4 37.2,24.4 21.3,24.4 27.6,30.9 26.5,31.9 18.4,23.6"/>
        </svg>
        </div>
        <div class="gallery-overlay-next">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 57 47" style="enable-background:new 0 0 57 47;" xml:space="preserve">
            <rect x="1.2" y="1.1" class="button" width="55" height="45"/>
                <polygon class="arrow"
                         points="30.9,15.4 29.9,16.4 36.2,22.9 20.2,22.9 20.2,15.9 18.7,15.9 18.7,31.4 20.2,31.4 20.2,24.4 36.2,24.4 29.9,30.9 30.9,31.9 39,23.6"/>
        </svg>
        </div>
        <div class="gallery-overlay-padding"></div>
        <div class="gallery-overlay-slides"></div>
        <div class="gallery-overlay-video"></div>
        <div class="gallery-overlay-nav">
            <ul></ul>
        </div>
    </div>
    <!-- Service Footer -->
    @include('frontend.layouts.solution')

@endsection