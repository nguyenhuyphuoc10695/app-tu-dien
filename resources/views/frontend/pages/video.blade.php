@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_decription)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)

@section('content')
    <div class="rs-gallery sec-spacer">
        <div class="container">
            <div class="sec-title-2 mb-50 text-center">
                <h2>{!! $pageContent->content !!}</h2>
            </div>
            @foreach($videos->chunk(2) as $video)
                <div class="row">
                    @foreach($video as $key => $item)
                        <div class="col-md-6">
                            <div class="gallery-item">
                                <img src="{!! url('upload/images',$item->img) !!}" alt="{!! $item->name !!}"/>
                                <div class="gallery-desc">
                                    <h3><a href="#">{!! $item->name !!}</a></h3>
                                    {!! $item->content !!}
                                    <a class="video-btn" data-toggle="modal" data-src="{!! $item->link !!}" data-target="#myModal">
                                        <i class="fa fa-youtube-play fa-4x" style="color: red"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
            <nav aria-label="Page navigation example">
                @php $paginator = $videos @endphp
                {!! $paginator->render('frontend.layouts.pagination') !!}
            </nav>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always"
                                allow="autoplay"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection