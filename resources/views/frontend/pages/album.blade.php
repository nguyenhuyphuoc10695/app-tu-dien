@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_decription)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)

@section('content')
    <div class="rs-gallery sec-spacer">
        <div class="container">
            <div class="sec-title-2 mb-50 text-center">
                <h2>{!! $pageContent->content !!}</h2>
            </div>
            @foreach($albums->chunk(2) as $album)
                <div class="row">
                    @foreach($album as $key => $item)
                        <div class="col-md-6">
                            <div class="gallery-item">
                                <img src="{!! url('upload/images',$item->img) !!}" alt="{!! $item->name !!}" />
                                <div class="gallery-desc">
                                    <h3><a href="#">{!! $item->name !!}</a></h3>
                                    {!! $item->content !!}
                                    <a class="image-popup" href="{!! url('upload/images',$item->img) !!}" title="{!! $item->name !!}">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
            <nav aria-label="Page navigation example">
                @php $paginator = $albums @endphp
                {!! $paginator->render('frontend.layouts.pagination') !!}
            </nav>
        </div>
    </div>
@endsection