@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_description)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)
@section('pageName', 'testimonial')
@section('pageID', $pageContent->id)

@section('content')

    <div id="title-banner" style="background: url({!! url('upload/images',$pageContent->image) !!}) center center no-repeat;background-size: cover;">
        <div class="contained middle">
            <h1>{!! $pageContent->title !!}</h1>
        </div>
    </div>
    <section id="contact-section" class="contained">
        <div class="col-60">
            <div id="contact-swapper">
                <h2>CẢM NHẬN CỦA KHÁCH HÀNG</h2>
                <p>
                @foreach($testimonials as $item)
                    <table>
                        <tr>
                            <td>
                                <font color="#00a8e0" size="4"><b>{!! $item->name !!}</b></font></br>
                                @if(!$item->img)
                                    <iframe width="560" height="315" src="{!! $item->link !!}" frameborder="0"
                                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                @else
                                    <img src="{!! url('upload/images', $item->img) !!}" style="width:560px;height:315px;" alt="{!! $item->name !!}">
                                @endif
                            </td>
                            <td valign="top" style="padding-left: 20px;">
                                {!! $item->content !!}
                            </td>
                        </tr>
                    </table>
                @endforeach
                </p>
            </div>
        </div>
    </section>

    <!-- Service Footer -->
    @include('frontend.layouts.request')
    <style>
        table {
            margin-top: 50px;
        }
    </style>

@endsection