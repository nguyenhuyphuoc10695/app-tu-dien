@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_decription)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)

@section('content')
    <div class="page-content">
        <div class="who-we-are">
            <div>
                <section class="page-hero">
                    <div class="container">
                        <div class="row">
                            <picture>
                                <img class="img-responsive" src="{!! url('upload/images',$pageContent->image) !!}"
                                     alt="{!! $pageContent->meta_title !!}"
                                     title="{!! $pageContent->title !!}" />
                            </picture>
                        </div>
                    </div>
                </section>
                <section id="intro">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1"><br><br>
                                {!! $pageContent->content !!}
                            </div>
                        </div>
                    </div>
                </section>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <hr>
                        </div>
                    </div>
                </div>
                @foreach(\App\Helpers\Common::getMenuChildren($pageContent->id, \App\Helpers\Common::getLang()) as $key => $item)
                    @if($key%2 == 0)
                        <section>
                            <div class="container">
                                <div class="row flex-wrapper">
                                    <div class="col-md-6 col-md-push-6 bg-color-main-gradient flex-wrapper align-items-center justify-content-center">
                                        <div class="content-wrapper col-sm-8 col-md-11 col-lg-7 no-padding-small text-background-color-white"><br><br>
                                            <h2 class="title underline-full text-color-white"><span>{!! $item['name'] !!}</span></h2>
                                            <h3 class="text-color-white">{!! $item['title'] !!}</h3>
                                            {!! $item['content'] !!}
                                            <br><br>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-md-pull-6 no-padding">
                                        <div class="img-wrapper-full">
                                            <picture>
                                                <img class="img-responsive" src="{!! url('upload/images',$item['img']) !!}"
                                                     alt="{!! $item['meta_title'] !!}"
                                                     title="{!! $item['title'] !!}" />
                                            </picture>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    @else
                        <section>
                            <div class="container">
                                <div class="row flex-wrapper">
                                    <div class="col-md-6 flex-wrapper align-items-center justify-content-center @if($key != 1) bg-color-main-gradient @endif">
                                        <div class="content-wrapper col-sm-8 col-md-11 col-lg-7 no-padding-small @if($key != 1) text-background-color-white @endif"><br><br>
                                            <h2 class="title underline-full @if($key != 1) text-color-white @endif"><span>{!! $item['name'] !!}</span></h2>
                                            <h3 class="@if($key != 1) text-color-white @endif">{!! $item['title'] !!}</h3>
                                            {!! $item['content'] !!}
                                            <br><br>
                                        </div>
                                    </div>
                                    <div class="col-md-6 no-padding">
                                        <div class="img-wrapper-full">
                                            <picture>
                                                <img class="img-responsive" src="{!! url('upload/images',$item['img']) !!}"
                                                     alt="{!! $item['meta_title'] !!}"
                                                     title="{!! $item['title'] !!}" />
                                            </picture>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection