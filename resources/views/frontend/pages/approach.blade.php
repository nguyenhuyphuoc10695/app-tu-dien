@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_description)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)
@section('pageName', 'approach')
@section('pageID', $pageContent->id)

@section('content')

    <div id="title-banner" style="background: url({!! url('upload/images',$pageContent->image) !!}) center center no-repeat;background-size: cover;">
        <div class="contained middle">
            <h1>{!! $pageContent->title !!}</h1>
        </div>
    </div>
    <section>
        <!-- Intro -->
        <div id="intro" class="contained">
            {!! $pageContent->content !!}
        </div>

        <!-- Service Item -->
        <div class="contained col-padding">
            <div class="col-l">
                <div id="svg-value" class="svg-wrapper">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                         y="0px"
                         viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;" xml:space="preserve">
                        <style type="text/css">
                            .st0 {
                                fill: #11A4DE;
                            }
                        </style>
                        <g id="object_3v">
                            <path class="st0s" d="M-345,257v100h100V257H-345z M-251,351h-88v-88h88V351z"/>
                        </g>
                        <g id="object_2v">
                            <polygon class="st0s" points="-265,304 -292,304 -292,277 -298,277 -298,304 -325,304 -325,310 -298,310 -298,337 -292,337
		-292,310 -265,310 	"/>
                        </g>
</svg>
                </div>
            </div>
            <div class="col-r">
                @foreach($approaches as $key => $item)
                    @if($key == 0)
                        <h3>{!! $item->name !!}</h3>
                        <h4>{!! $item->tile !!}</h4>
                        {!! $item->content !!}
                    @endif
                @endforeach
            </div>
        </div>
        <!-- Service Item -->
        <div class="contained col-padding">
            <div class="col-l">
                <div id="svg-collab-approach" class="svg-wrapper">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                         y="0px"
                         viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;" xml:space="preserve">
                        <style type="text/css">
                            .st0 {
                                fill: #16A3DD;
                            }

                            .st1 {
                                display: none;
                            }

                            .st2 {
                                display: inline;
                            }
                        </style>
                        <g id="object_4">
                            <g>
                                <path class="st0" d="M-283.3,339.7h-26.5v-55.5h64.8v55.5h-16.6l-0.2,17.3L-283.3,339.7z M-251,290.3h-52.8v43.5h22.6l13.5,10.9
			                    l0.1-10.9h16.5V290.3z"/>
                            </g>
                        </g>
                        <g id="object_3">
                            <g>
                                <path class="st0" d="M-328.2,329.7l-0.2-17.3H-345V257h64.8v55.5h-26.5L-328.2,329.7z M-339,306.5h16.5l0.1,10.9l13.5-10.9h22.6
			                    V263H-339V306.5z"/>
                            </g>
                        </g>
                        <g id="object_2" class="st1">
                            <g class="st2">
                                <path class="st0" d="M-264.2,348.4h-13.2v-27.7h32.4v27.7h-8.3l-0.1,8.6L-264.2,348.4z M-248,323.6h-26.4v21.7h11.3l6.8,5.4
			                    l0.1-5.4h8.3V323.6z"/>
                            </g>
                        </g>
                        <g id="object_1" class="st1">
                            <g class="st2">
                                <path class="st0" d="M-336.6,293.4l-0.1-8.6h-8.3V257h32.4v27.7h-13.2L-336.6,293.4z M-342,281.7h8.3l0.1,5.4l6.8-5.4h11.3V260
			H-342V281.7z"/>
                            </g>
                        </g>
</svg>
                </div>
            </div>
            <div class="col-r">
                @foreach($approaches as $key => $item)
                    @if($key == 1)
                        <h3>{!! $item->name !!}</h3>
                        <h4>{!! $item->tile !!}</h4>
                        {!! $item->content !!}
                    @endif
                @endforeach
            </div>
        </div>
        <!-- Service Item -->
        <div class="contained col-padding">
            <div class="col-l">
                <div id="svg-collab" class="svg-wrapper">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                         y="0px"
                         viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;" xml:space="preserve">
                        <style type="text/css">
                            .st0 {
                                fill: #11A4DE;
                                stroke-width: 10
                            }
                        </style>
                        <g id="object_2c">
                            <path id="bx-small" class="st0" d="M-328,273v84h83v-84H-328z M-251,351h-71v-72h71V351z"/>
                        </g>
                        <g id="object_1c">
                            <path class="st0" d="M-345,257v100h100V257H-345z M-251,351h-88v-88h88V351z"/>
                        </g>
</svg>
                </div>
            </div>
            <div class="col-r">
                @foreach($approaches as $key => $item)
                    @if($key == 2)
                        <h3>{!! $item->name !!}</h3>
                        <h4>{!! $item->tile !!}</h4>
                        {!! $item->content !!}
                    @endif
                @endforeach
            </div>
        </div>
        <!-- Service Item -->
        <div class="contained col-padding">
            <div class="col-l">
                <div id="svg-safety" class="svg-wrapper">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                         y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
            <polygon id="safety-l" points="4.2,20.2 0,24.5 25.5,49.9 0,75.4 4.2,79.6 33.9,49.9"/>
                        <polygon id="safety-t" points="79.7,4.2 75.5,0 50,25.5 24.6,0 20.3,4.2 50,33.9"/>
                        <polygon id="safety-r" points="100,24.6 95.8,20.4 66.1,50.1 95.8,79.8 100,75.5 74.5,50.1"/>
                        <polygon id="safety-b" points="79.9,95.8 50.2,66.1 20.5,95.8 24.7,100 50.2,74.5 75.6,100"/>
        </svg>
                </div>
            </div>
            <div class="col-r">
                @foreach($approaches as $key => $item)
                    @if($key == 3)
                        <h3>{!! $item->name !!}</h3>
                        <h4>{!! $item->tile !!}</h4>
                        {!! $item->content !!}
                    @endif
                @endforeach
            </div>
        </div>

        <!-- Manufacturing Excellence -->
        <div id="manufacturing-excellence" class="contained">
            @foreach($approaches as $key => $item)
                @if($key == 4)
                    <h3>{!! $item->name !!}</h3>
                    {!! $item->content !!}
                    <div class="col-img">
                        <img src="{!! url('upload/images', $item->img) !!}"/>
                    </div>
                    <div class="col-img">
                        <img src="{!! url('upload/images', $item->image) !!}"/>
                    </div>
                @endif
            @endforeach
        </div>
    </section>

    <!-- Service Footer -->
    @include('frontend.layouts.solution')

@endsection