@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_decription)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)

@section('content')

    <div class="page-content">
        <div class="careers">
            <div class="careers-home">
                <section class="page-hero">
                    <div class="container">
                        <div class="row">
                            <div>
                                {!! $pageContent->link !!}
                            </div>
                        </div>
                    </div>
                </section>
                <section class="careers-section">
                    <div class="container">
                        @foreach(\App\Helpers\Common::getMenuChildren($pageContent->id, \App\Helpers\Common::getLang()) as $key => $item)
                            @if($key == 0)
                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1"><br class="hidden-xs"><br class="hidden-xs">
                                        <h2 class="subtitle text-blue text-center">{!! $item['name'] !!}</h2>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                {!! $item['content'] !!}
                                            </div>
                                        </div>
                                        <br><br>
                                        <p class="text-center"><a class="btn btn-secundary btn-find-position" href="#">{!! config('language')[\App\Helpers\Common::getLanguage()]['JOIN_OUR_TEAM'] !!}</a></p><br><br><br><br>
                                    </div>
                                </div>
                            @elseif($key == 1)
                                <div class="opportunity row">
                                    <div class="col-lg-6 no-padding">
                                        <div class="img-wrapper-full">
                                            <picture>
                                                <img class="img-responsive" src="{!! url('upload/images',$item['img']) !!}"
                                                     alt="{!! $item['meta_title'] !!}"
                                                     title="{!! $item['meta_title'] !!}"/>
                                            </picture>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 bg-color-main-gradient flex-wrapper align-items-center justify-content-center">
                                        <div class="content-wrapper col-sm-11 col-lg-10 text-background-color-white"><br><br>
                                            <h2 class="title underline-full text-color-white"><span>{!! $item['title'] !!}</span></h2><br>
                                            <h3 class="subtitle text-color-white">{!! $item['name'] !!}</h3>
                                            {!! $item['content'] !!}<br><br>
                                        </div>
                                    </div>
                                </div>
                            @elseif($key == 2)
                                <div class="how-we-hire row">
                                    <div class="col-lg-6 no-padding">
                                        <div class="img-wrapper-full">
                                            <picture>
                                                <img class="img-responsive" src="{!! url('upload/images',$item['img']) !!}"
                                                     alt="{!! $item['meta_title'] !!}"
                                                     title="{!! $item['meta_title'] !!}"/>
                                            </picture>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 bg-color-main-gradient flex-wrapper align-items-center justify-content-center">
                                        <div class="content-wrapper col-sm-11 col-lg-10 text-background-color-white"><br><br>
                                            <h2 class="title underline-full text-color-white"><span>{!! $item['title'] !!}</span></h2>
                                            {!! $item['content'] !!}
                                            <br><br>
                                        </div>
                                    </div>
                                </div>
                            @elseif($key == 3)
                                <div class="row flex-wrapper justify-content-center flex-flow-row-wrap">
                                    <div class="col-sm-9"><br><br><br><br>
                                        <h2 class="title underline-full text-center"><span>{!! $item['title'] !!}</span></h2><br>
                                        <h3 class="subtitle text-center">{!! $item['name'] !!}</h3><br>
                                        {!! $item['content'] !!}
                                        <br><br>
                                    </div>
                                    <div class="col-xs-7 flex-wrapper justify-content-center">
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <hr class="line-separator-short">
                                        </div>
                                    </div>
                                </div>
                            @elseif($key == 4)
                                <div class="row flex-wrapper justify-content-center flex-flow-row-wrap">
                                    <div class="col-sm-8"><br>
                                        <h3 class="subtitle text-center">{!! $item['name'] !!}</h3><br>
                                        {!! $item['content'] !!}
                                        <br><br>
                                    </div>
                                    <div class="col-xs-11">
                                        <hr>
                                    </div>
                                </div>
                            @elseif($key == 5)
                                <div class="row flex-wrapper justify-content-center">
                                    <div class="col-sm-10 col-md-7"><br>
                                        <h3 class="subtitle text-center">{!! $item['name'] !!}</h3>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </section>
                <section data-jc="jobBoard" class="jobBoard container" data-jobboardname="cypresscreekrenewables">
                    <div class="row">
                        <div class="col-sm-12"><br>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="jobBoard-filter">
                                            <h3 class="jobBoard-filter-title">
                                                <a href="javascript:void(0)" class="btn-mobile-filter exec"
                                                        data-exec="#filter.all" onclick="getAllPosition()">{!! config('language')[\App\Helpers\Common::getLanguage()]['ALL_POSITION'] !!}</a>
                                            </h3>
                                            <h3 class="jobBoard-filter-title">
                                                <a href="#locationList" data-toggle="collapse" class="collapse collapsed"
                                                   aria-expanded="false">{!! config('language')[\App\Helpers\Common::getLanguage()]['LOCATION'] !!}</a>
                                            </h3>
                                            <ul id="locationList" class="list-filter location-list collapse"
                                                aria-expanded="false" style="height: 0px;">
                                                @foreach(\App\Helpers\Common::getLocations() as $location)
                                                    <li class="exec btn-filter" data-exec="#filter.locations"
                                                        data-filter="{!! $location->name !!}" onclick="getLocation({!! $location->id !!})">
                                                        {!! $location->name !!}
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <h3 class="jobBoard-filter-title">
                                                <a href="#departmentList" data-toggle="collapse"
                                                   class="collapse collapsed">{!! config('language')[\App\Helpers\Common::getLanguage()]['DEPARTMENT'] !!}</a>
                                            </h3>
                                            <ul id="departmentList" class="list-filter department-list collapse">
                                                @foreach(\App\Helpers\Common::getDepartments() as $department)
                                                    <li class="exec btn-filter" data-exec="#filter.departments"
                                                        data-filter="{!! $department->name !!}" onclick="getDepartment({!! $department->id !!})">
                                                        {!! $department->name !!}
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <div class="btn-mobile-filter-wrapper">
                                                <button class="btn btn-secundary btn-mobile-filter">Filter</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-7 no-padding">
                                        <div class="jobBoard-list">
                                            <div class="filter-controls">
                                                <button class="btn btn-secundary btn-job-addFilter">Add Filters</button>
                                                <div class="filters-selected"></div>
                                            </div>
                                            <ol class="joblist">
                                                @foreach($careers as $item)
                                                    <li class="joblist-item" data-jobid="1414629">
                                                        <a href="{!! url('/career', $item['slug']) !!}.html" target="_blank">
                                                            <div class="panelbox-expandable">
                                                                <div class="panelbox-expandable-tile">
                                                                    <div class="panelbox-expandable-wrapper">
                                                                        <div class="panelbox-expandable-content">
                                                                            <h4 class="jobtitle">{!! $item['name'] !!}</h4>
                                                                            <p class="joblocation">{!! $item['title'] !!}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                @endforeach
                                                @if($careers->hasMorePages())
                                                    <li class="text-center">
                                                        <button class="btn btn-secundary btn-show-all exec" onclick="getAllFilterData()">Show All</button>
                                                    </li>
                                                @endif
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="careers-job">
                <section class="careers-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <h1 class="section-title"><span class="title-underline">{!! $pageContent->name !!}</span></h1>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script>
        let locations = [];
        let departments = [];
        let perPage = 5;

        function getLocation(id) {
            if (locations.includes(id)) {
                locations.splice(locations.indexOf(id), 1);
            } else {
                locations.push(id);
            }
            getFilterData(locations,departments,perPage);
        }

        function getDepartment(id) {
            if (departments.includes(id)) {
                departments.splice(departments.indexOf(id), 1);
            } else {
                departments.push(id);
            }
            getFilterData(locations,departments,perPage);
        }

        function getAllPosition() {
            locations = [];
            departments = [];
            getFilterData(locations,departments,perPage);
        }

        function getAllFilterData() {
            getFilterData(locations,departments,10000);
        }


        function getFilterData(locations,departments,perPage) {
            let url = "{{ url('/') }}";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: url+'/api/list-careers',
                type: 'POST',
                cache: false,
                data: {
                    locations: locations.join(),
                    departments: departments.join(),
                    perPage: perPage,
                },
                success: function (data) {
                    let html = '';
                    // console.log(data.data);
                    let careers = data.data;
                    careers.forEach(function (item) {
                        html += '<li class="joblist-item" data-jobid="'+item.id+'">';
                        html +=     '<a href="'+url+'/career/'+item.slug+'.html" target="_blank">';
                        html +=         '<div class="panelbox-expandable">';
                        html +=             '<div class="panelbox-expandable-tile">';
                        html +=                 '<div class="panelbox-expandable-wrapper">';
                        html +=                     '<div class="panelbox-expandable-content">';
                        html +=                         '<h4 class="jobtitle">'+item.name+'</h4>';
                        html +=                         '<p class="joblocation">'+item.title+'</p>';
                        html +=                     '</div>';
                        html +=                 '</div>';
                        html +=             '</div>';
                        html +=         '</div>';
                        html +=     '</a>';
                        html += '</li>';
                    });
                    if(parseInt(data.last_page) > 1) {
                        html += '<li class="text-center">';
                        html += '<button class="btn btn-secundary btn-show-all exec" onclick="getAllFilterData()">Show All </button>';
                        html += '</li>';
                    }
                    $('.joblist').html(html);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    </script>
    <style>
        .h1-light h1 {
            font-weight: 100;
        }
        .text-background-color-white h3 {
            color: white;
        }
        .content-image img {
            width: 100%;
        }
        iframe {
            width: 100%;
        }
        @media (min-width: 1024px) {
            iframe {
                min-height: 660px;
            }
        }
        @media (min-width: 769px) and (max-width: 1024px) {
            iframe {
                min-height: 550px;
            }
        }
        @media (min-width: 769px) and (max-width: 768px) {
            iframe {
                min-height: 425px;
            }
        }
        @media (max-width: 425px) {
            iframe {
                min-height: 330px;
            }
        }
    </style>
@endsection