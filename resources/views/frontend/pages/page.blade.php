@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_decription)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)

@section('content')
    <!-- 404 Page Area Start Here -->
    <div class="page-content sec-spacer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 error-page-message">
                    {!! $pageContent->content !!}
                </div>
            </div>
        </div>
    </div>
    <!-- 404 Page Area End Here -->

    <!-- Partner Start -->
    @include('frontend.layouts.partner')
    <!-- Partner End -->

    <style>
        .page-content img {
            margin: 0 auto !important;
            display: block;
        }

        .page-content table {
            width: 100% !important;
        }

        @media screen and (min-width: 600px) {

        }

        @media screen and (min-width: 767px) {

        }
    </style>
@endsection