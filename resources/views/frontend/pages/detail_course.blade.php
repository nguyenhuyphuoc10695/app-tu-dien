@extends('frontend.layouts.master')
@section('title', $course['meta_title'])
@section('description', $course['meta_description'])
@section('keywords', $course['meta_keywords'])
@section('image', $course['img'])

@section('content')

    <!-- Courses Details Start -->
    <div class="rs-courses-details pt-100 pb-70">
        <div class="container">
            <div class="row mb-30">
                <div class="col-lg-8 col-md-12">
                    <div class="detail-img">
                        <img src="{!! url('upload/images', $course->image) !!}" alt="{!! $course->name !!}" />
                        <div class="course-seats">
                            {!! $course->slot !!} <span>SEATS</span>
                        </div>
                    </div>
                    <div class="course-content">
                        <!--<h3 class="course-title">Computer Science And Engineering</h3>-->
                        <div class="course-instructor">
                            <div class="row">
                                <div class="col-md-6 mobile-mb-20">
                                    <h3 class="instructor-title"><span class="primary-color">GIẢNG VIÊN</span></h3>
                                    @if($staff)
                                        <div class="instructor-inner">
                                            <div class="instructor-img">
                                                <img src="{!! url('upload/images', $staff->img) !!}" alt="{!! $staff->name !!}" />
                                            </div>
                                            <div class="instructor-body">
                                                <h3 class="name">{!! $staff->name !!}</h3>
                                                <span class="designation">{!! $staff->position !!}</span>
                                                <div class="social-icon">
                                                    <a href="{!! $staff->facebook !!}"><i class="fa fa-facebook"></i></a>
                                                    <a href="{!! $staff->twitter !!}"><i class="fa fa-twitter"></i></a>
                                                    <a href="{!! $staff->google !!}"><i class="fa fa-google-plus"></i></a>
                                                    <a href="{!! $staff->pinterest !!}"><i class="fa fa-pinterest"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="short-desc">
                                            {!! $staff->content !!}
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <h3 class="instructor-title"><span class="primary-color">THÔNG TIN</span></h3>
                                    <div class="row info-list">
                                        <div class="col-md-6">
                                            <ul>
                                                <li>
                                                    <span>Học phí :</span> {!! number_format($course->price) !!}
                                                </li>
                                                <li>
                                                    <span>Số tiết học :</span> {!! $course->lesson !!}
                                                </li>
                                                <li>
                                                    <span>Thời gian :</span> {!! $course->time !!} Năm
                                                </li>
                                                <li>
                                                    <?php $level = $course->level - 1 ?>
                                                    <span>Cấp độ :</span> {!! config('level')[$level] !!}
                                                </li>
                                                <li>
                                                    <span>Danh mục :</span> {!! \App\Helpers\Common::getCategoryByID($course->category_id)['name'] !!}
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <ul>
                                                <li>
                                                    <span>Ngày bắt đầu :</span> {!! date('d-m-Y', strtotime($course['created_at'])) !!}
                                                </li>
                                                <li>
                                                    <span>Ca học :</span> {!! $course->shift !!}
                                                </li>
                                                <li>
                                                    <span>Số lớp mở :</span> {!! $course->class !!}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="apply-btn">
                                        <a href="{!! $course->link !!}">THAM GIA NGAY</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="course-desc">
                        <h3 class="desc-title">Mô tả khóa học</h3>
                        <div class="desc-text">
                            {!! $course->content !!}
                        </div>
                        <div class="course-syllabus">
                            <h3 class="desc-title">Giáo trình khóa học</h3>
                            <div id="accordion" class="rs-accordion-style1">
                                @foreach(json_decode($course->lesson_detail) as $key => $item)
                                <div class="card">
                                    <div class="card-header" id="heading{!! $key !!}">
                                        <h3 class="acdn-title" data-toggle="collapse" data-target="#collapse{!! $key !!}" aria-expanded="true" aria-controls="collapse{!! $key !!}">
                                            <strong>Lessons  {!! $key !!}: </strong>
                                            <span>{!! $item-> name !!}</span>
                                        </h3>
                                    </div>
                                    <div id="collapse{!! $key !!}" class="collapse @if($key == 1) show @endif" aria-labelledby="heading{!! $key !!}" data-parent="#accordion">
                                        <div class="card-body">
                                            {!! $item-> content !!}
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
                @include('frontend.layouts.sidebar')
            </div>
        </div>
        <div id="rs-testimonial" class="rs-testimonial pt-100 pb-45 sec-color" style="display: none;">
            <div class="container">
                <div class="sec-title-2 mb-50">
                    <h2>Student Reviews</h2>
                    {{--<p>Considering primary motivation for the generation of narratives is a useful concept</p>--}}
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div  class="rs-carousel owl-carousel" data-loop="true" data-items="2" data-margin="30" data-autoplay="true" data-autoplay-timeout="8000" data-smart-speed="1200" data-dots="false" data-nav="true" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="false" data-ipad-device="1" data-ipad-device-nav="true" data-ipad-device-dots="false" data-md-device="2" data-md-device-nav="true" data-md-device-dots="false">
                            @foreach($reviews as $item)
                                <div class="testimonial-item">
                                    <div class="testi-img">
                                        <img src="{!! url('upload/images',$item->img) !!}" alt="{!! $item->name !!}">
                                    </div>
                                    <div class="testi-desc">
                                        <h4 class="testi-name">{!! $item->name !!}</h4>
                                        <div class="cl-client-rating">
                                            @for ($i = 1; $i <= 5; $i++)
                                                @if($i < $item->star)
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                @else
                                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                                @endif
                                            @endfor
                                        </div>
                                        {!! $item->content !!}
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pt-100">
            <!-- Testimonial Start -->
            <div class="related-courses rs-courses-3">
                <div class="sec-title-2 mb-50">
                    <h2>KHÓA HỌC LIÊN QUAN</h2>
                    {{--<p>Considering primary motivation for the generation of narratives is a useful concept</p>--}}
                </div>
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-autoplay-timeout="5000" data-smart-speed="1500" data-nav="true" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="true" data-ipad-device="2" data-ipad-device-nav="true" data-md-device="3" data-md-device-nav="true">
                    @foreach($courseRelated as $item)
                        <div class="course-item">
                            <div class="course-img">
                                <img src="{!! url('upload/images',$item->img) !!}" alt="{!! $item->name !!}" />
                                <span class="course-value">{!! number_format($item->price) !!}</span>
                                <div class="course-toolbar">
                                    <h4 class="course-category">
                                        <a href="{!! url('category',\App\Helpers\Common::getCategoryByID($item->category_id)['slug']) !!}.html">
                                            {!! \App\Helpers\Common::getCategoryByID($item->category_id)['name'] !!}
                                        </a>
                                    </h4>
                                    <div class="course-date">
                                        <i class="fa fa-calendar"></i> {!! date('d-m-Y', strtotime($item->created_at)) !!}
                                    </div>
                                    <div class="course-duration">
                                        <i class="fa fa-clock-o"></i> {!! $item->time !!} năm
                                    </div>
                                </div>
                            </div>
                            <div class="course-body">
                                <div class="course-desc">
                                    <h4 class="course-title"><a href="{!! url('course',$item->slug) !!}.html">{!! $item->name !!}</a></h4>
                                    {!! $item->description !!}
                                </div>
                            </div>
                            <div class="course-footer">
                                <div class="course-seats">
                                    <i class="fa fa-users"></i> {!! $item->slot !!} HỌC VIÊN
                                </div>
                                <div class="course-button">
                                    <a href="{!! $item->link !!}">XEM NGAY</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- Courses Details End -->

    <!-- Partner Start -->
    @include('frontend.layouts.partner')
    <!-- Partner End -->

    <style>
        .entry-content img {
            margin: 0 auto !important;
            width: 100% !important;
            display: block;
            filter: grayscale(0%);
            transition: all 0.8s ease-in-out 0s;
        }

        .entry-content img:hover {
            filter: grayscale(100%);
            transform: scale(1.1);
        }

        @media screen and (min-width: 600px) {

        }

        @media screen and (min-width: 767px) {

        }
    </style>
@endsection