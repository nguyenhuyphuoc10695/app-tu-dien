@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_description)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)

@section('content')
    <!-- Events Start -->
    <div class="rs-events-2 sec-spacer">
        <div class="container">
            @foreach($events->chunk(2) as $event)
            <div class="row space-bt30">
                @foreach($event as $key => $item)
                    <div class="col-lg-6 col-md-12">
                        <div class="event-item">
                            <div class="row rs-vertical-middle">
                                <div class="col-md-6">
                                    <div class="event-img">
                                        <img src="{!! url('upload/images',$item->img) !!}" alt="{!! $item->name !!}" />
                                        <a class="image-link" href="{!! url('event',$item->slug) !!}.html"
                                           title="{!! $item->name !!}">
                                            <i class="fa fa-link"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="event-content">
                                        <div class="event-meta">
                                            <div class="event-date">
                                                <i class="fa fa-calendar"></i>
                                                <span>{!! date('F d, Y', strtotime($item['created_at'])) !!}</span>
                                            </div>
                                            <div class="event-time">
                                                <i class="fa fa-clock-o"></i>
                                                <span>{!! $item->duration !!}</span>
                                            </div>
                                        </div>
                                        <h3 class="event-title">
                                            <a href="{!! url('event',$item->slug) !!}.html">{!! $item->name !!}</a>
                                        </h3>
                                        <div class="event-location">
                                            <i class="fa fa-map-marker"></i>
                                            <span>{!! $item->address !!}</span>
                                        </div>
                                        <div class="event-desc">
                                            {!! $item->description !!}
                                        </div>
                                        <div class="event-btn">
                                            <a href="{!! url('event',$item->slug) !!}.html">Xem ngay</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @endforeach
            <nav aria-label="Page navigation example" class="mt-50">
                @php $paginator = $events @endphp
                {!! $paginator->render('frontend.layouts.pagination') !!}
            </nav>
        </div>
    </div>
    <!-- Events End -->

    <!-- Branches Start -->
    @include('frontend.layouts.branch')
    <!-- Branches End -->

    <!-- Partner Start -->
    @include('frontend.layouts.partner')
    <!-- Partner End -->

@endsection