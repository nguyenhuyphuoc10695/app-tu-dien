@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_description)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)
@section('pageName', 'contact')
@section('pageID', $pageContent->id)

@section('content')

    <div id="title-banner" style="background: url({!! url('upload/images',$pageContent->image) !!}) center center no-repeat;background-size: cover;">
        <div class="contained middle">
            <h1>{!! $pageContent->title !!}</h1>
        </div>
    </div>
    <section id="contact-section" class="contained">
        @if (Session::has('flash_message'))
            <div class="alert alert-{{ Session::get('flash_level') }} icons-alert">
                <strong>{!! Session::get('flash_message') !!}</strong>
            </div>
        @endif
        <div class="col-60">
            {!! $pageContent->content !!}
        </div>
        <div class="col-40">
            <div role="form" class="" id="wpcf7-f751-o1" lang="en-US" dir="ltr">
                <div class="screen-reader-response"></div>
                <form action="{!! route('save_contact') !!}" method="post" class="wpcf7-form" novalidate="novalidate">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <div class="form-group has-error">
                            <span class="help-block">{!! $errors->first('name') !!}</span>
                        </div>
                        <span class="wpcf7-form-control-wrap your-name">
                            <input type="text" name="name" value=""size="40"
                                   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-lg"
                                   id="first-name-field" aria-required="true" aria-invalid="false" placeholder="Họ tên"/>
                        </span>
                    </div>
                    <div class="form-group">
                        <div class="form-group has-error">
                            <span class="help-block">{!! $errors->first('company') !!}</span>
                        </div>
                        <span class="wpcf7-form-control-wrap your-name">
                            <input type="text" name="company" value=""size="40"
                                   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-lg"
                                   id="first-name-field" aria-required="true" aria-invalid="false" placeholder="Tên công ty"/>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="form-group has-error">
                            <span class="help-block">{!! $errors->first('address') !!}</span>
                        </div>
                        <span class="wpcf7-form-control-wrap your-name">
                            <input type="text" name="address" value=""size="40"
                                   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-lg"
                                   id="first-name-field" aria-required="true" aria-invalid="false" placeholder="Địa chỉ"/>
                        </span>
                    </div>
                    <div class="form-group">
                        <div class="form-group has-error">
                            <span class="help-block">{!! $errors->first('email') !!}</span>
                        </div>
                        <span class="wpcf7-form-control-wrap your-name">
                            <input type="email" name="email" value=""size="40"
                                   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-lg"
                                   id="first-name-field" aria-required="true" aria-invalid="false" placeholder="Email"/>
                        </span>
                    </div>

                    <div class="row">
                        <div class="col-50 email-address-field-wrapper">
                            <div class="form-group">
                                <div class="form-group has-error">
                                    <span class="help-block">{!! $errors->first('space') !!}</span>
                                </div>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <input type="text" name="space" value="" size="40" aria-required="true"
                                           class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-lg"
                                           id="email-address-field" aria-invalid="false" placeholder="Số chỗ đậu xe"/>
                                </span>
                            </div>
                        </div>
                        <div class="col-50 phone-number-field-wrapper">
                            <div class="form-group">
                                <div class="form-group has-error">
                                    <span class="help-block">{!! $errors->first('phone') !!}</span>
                                </div>
                                <span class="wpcf7-form-control-wrap your-phone">
                                    <input type="text" name="phone" value="" size="40" id="phone-number-field"
                                           class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-lg"
                                           aria-required="true" aria-invalid="false" placeholder="Số điện thoại"/>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea rows="7" class="form-control" name="message" placeholder="Ghi chú"></textarea>
                    </div>
                    <p><input type="submit" value="Submit"
                              class="wpcf7-form-control wpcf7-submit btn btn-primary blue-btn"/></p>
                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                </form>
            </div>
        </div>
    </section>

    <!-- Service Footer -->
    @include('frontend.layouts.request')

@endsection