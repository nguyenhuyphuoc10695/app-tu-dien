@extends('frontend.layouts.master')
{{--@section('title', $post['meta_title'])--}}
{{--@section('description', $post['meta_decription'])--}}
{{--@section('keywords', $post['meta_keywords'])--}}
{{--@section('image', $post['img'])--}}

@section('content')
    <div class="page-content">
        <div class="container">
            <div class="col-sm-10 col-sm-offset-1">
                <h1 class="section-title"><span class="title-underline">Careers</span></h1>
                <section class="">
                    <h2 class="job-title">{!! $career['name'] !!}</h2>
                    <h4 class="job-location">{!! $career['title'] !!}</h4>
                </section>
                @if (Session::has('flash_message'))
                    <div class="alert alert-{{ Session::get('flash_level') }} fa fa-info-circle">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="fa fa-times-circle"></i>
                        </button>
                        <p><strong>{!! Session::get('flash_message') !!}</strong></p>
                    </div>
                @endif
                <section class="post-body">

                    <div class="row">
                        <div class="post-news">
                            <div class="col-md-12">
                                {!! $career['content'] !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="heading">{!! config('language')[\App\Helpers\Common::getLanguage()]['APPLY'] !!}</h2>
                            <form method="POST" action="{!! route('contact_career') !!}" enctype="multipart/form-data" id="formSubmit" novalidate>
                                {!! csrf_field() !!}
                                <input type="hidden" value="uhsuy" name="slug"/>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="firstName">{!! config('language')[\App\Helpers\Common::getLanguage()]['FIRST_NAME'] !!} <span class="asterisk">*</span></label>
                                        <input type="text" value="{!! old('firstName') !!}"
                                               class="form-control @if($errors->first('firstName')) form-control-danger @endif"
                                               id="firstName" name="firstName" />
                                        <div class="form-group has-error">
                                            <span class="help-block" id="error-name">{!! $errors->first('firstName') !!}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="lastName">{!! config('language')[\App\Helpers\Common::getLanguage()]['LAST_NAME'] !!} <span class="asterisk">*</span></label>
                                        <input type="text" value="{!! old('lastName') !!}"
                                               class="form-control @if($errors->first('lastName')) form-control-danger @endif"
                                               id="lastName" name="lastName" />
                                        <div class="form-group has-error">
                                            <span class="help-block" id="error-name">{!! $errors->first('lastName') !!}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="email">{!! config('language')[\App\Helpers\Common::getLanguage()]['EMAIL'] !!} <span class="asterisk">*</span></label>
                                        <input type="email" value="{!! old('email') !!}"
                                               class="form-control @if($errors->first('email')) form-control-danger @endif"
                                               id="email" name="email" />
                                        <div class="form-group has-error">
                                            <span class="help-block" id="error-name">{!! $errors->first('email') !!}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="phone">{!! config('language')[\App\Helpers\Common::getLanguage()]['PHONE'] !!} <span class="asterisk">*</span></label>
                                        <input type="text" value="{!! old('phone') !!}"
                                               class="form-control @if($errors->first('phone')) form-control-danger @endif"
                                               id="phone" name="phone" />
                                        <div class="form-group has-error">
                                            <span class="help-block" id="error-name">{!! $errors->first('phone') !!}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="address">{!! config('language')[\App\Helpers\Common::getLanguage()]['ADDRESS'] !!} <span class="asterisk">*</span></label>
                                        <input type="text" value="{!! old('address') !!}"
                                               class="form-control @if($errors->first('address')) form-control-danger @endif"
                                               id="address" name="address" />
                                        <div class="form-group has-error">
                                            <span class="help-block" id="error-name">{!! $errors->first('address') !!}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group col-md-offset-1">
                                        <label class="inputFile"> {!! config('language')[\App\Helpers\Common::getLanguage()]['CV'] !!}*
                                            <input type="file" name="cv_file" id="cv_file"/>
                                        </label>
                                        <div class="form-group has-error">
                                            <span class="help-block" id="error-name">{!! $errors->first('cv_file') !!}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group col-md-offset-2">
                                        <label class="inputFile"> {!! config('language')[\App\Helpers\Common::getLanguage()]['COVER_LETTER'] !!}
                                            <input type="file" name="cover_letter" id="cover_letter"/>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group col-md-offset-4">
                                        <button type="submit" style="width: 100%;" class="btn btn-warning" id="btnPrevent">{!! config('language')[\App\Helpers\Common::getLanguage()]['SUBMIT'] !!}</button>
                                        <div class="waitting-execute" id="loadingIcon" style="display: none;">
                                            <img src="{!! asset('images/Loading_icon.gif') !!}" style="width:100px;display:block;margin:0 auto;"/>
                                            <p style="text-align:center;color:red;">{!! config('language')[\App\Helpers\Common::getLanguage()]['LOADING'] !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <script>
        $("#formSubmit").on("submit", function(){
            $("#btnPrevent").prop('disabled',true);
            $('#loadingIcon').show();
        });
    </script>
    <style>
        h2.heading {
            color: #0a3370;
            font-size: 23px;
            font-weight: 400;
        }
        .alert p {
            font-size: 20px;
            margin-left: 50px;
        }
        .alert.icons-alert {
            border-left-width: 48px;
        }
        .alert-success {
            background-color: #fff;
            border-color: #2ecc71 !important;
            color: #2ecc71;
        }
        .alert-danger {
            background-color: #fff;
            border-color: #e74c3c;
            color: #e74c3c;
        }
        .fa-info-circle:before {
            content: "\f05a";
            font-size: 30px;
            position: absolute;
        }
        .alert {
            padding: 15px;
            margin-bottom: 22px;
            border: 1px solid transparent;
            border-radius: 4px;
            display: block !important;
        }
        .icons-alert {
            position: relative;
        }
        .form-group {
            margin-bottom: 15px !important;
        }
        .form-control-danger {
            border-color: #e74c3c;
        }
        .has-error .help-block {
            color: #e74c3c;
        }
        input[type="file"] {
            display: none;
        }
        .inputFile {
            border: 2px solid #f7ac15;
            line-height: 45px;
            padding: 0 40px;
            width: auto;
            text-align: center;
            color: #333;
            text-transform: uppercase;
            font-weight: 700;
            border-radius: 25px;
        }
        .inputFile:hover {
            color: white;
            background: #f7ac15;
        }
        form .form-group label {
            text-align: left;
            font-size: 16px;
            display: block !important;
        }
        .asterisk {
            color: #DB2A3A;
        }
        .post-body .post-news img {
            margin: 0 auto !important;
            width: 100% !important;
            display: block;
        }
        .job-title {
            text-align: center;
            font-size: 2.250em;
            font-weight: 100;
            margin: 1.2em 0 0.8em;
        }
        .job-location {
            color: #999;
            text-align: center;
            font-size: 1.125em;
            font-weight: 100;
            margin-bottom: 1em;
        }
    </style>
@endsection