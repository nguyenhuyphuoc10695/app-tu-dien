@extends('frontend.layouts.master')
@section('title', \App\Helpers\Common::getSettings()->meta_title)
@section('description', \App\Helpers\Common::getSettings()->meta_description)
@section('keywords', \App\Helpers\Common::getSettings()->meta_keywords)
@section('image', \App\Helpers\Common::getSettings()->img)

@section('content')
    <!-- Courses Start -->
    <div id="rs-courses-3" class="rs-courses-3 sec-spacer">
        <div class="container">
            <div class="abt-title">
                <h2>KẾT QUẢ TÌM KIẾM KHÓA HỌC</h2>
            </div>

            @if(count($courses) > 0)
                @foreach($courses->chunk(3) as $course)
                    <div class="row grid">
                        @foreach($course as $key => $item)
                            <div class="col-lg-4 col-md-6 grid-item filter1">
                                <div class="course-item">
                                    <div class="course-img">
                                        <img src="{!! url('upload/images',$item->img) !!}" alt="{!! $item->name !!}" />
                                        <span class="course-value">{!! number_format($item->price) !!}</span>
                                        <div class="course-toolbar">
                                            <h4 class="course-category">
                                                <a href="{!! url('category',\App\Helpers\Common::getCategoryByID($item->category_id)['slug']) !!}.html">
                                                    {!! \App\Helpers\Common::getCategoryByID($item->category_id)['name'] !!}
                                                </a>
                                            </h4>
                                            <div class="course-date">
                                                <i class="fa fa-calendar"></i> {!! date('d-m-Y', strtotime($item->created_at)) !!}
                                            </div>
                                            <div class="course-duration">
                                                <i class="fa fa-clock-o"></i> {!! $item->time !!} năm
                                            </div>
                                        </div>
                                    </div>
                                    <div class="course-body">
                                        <div class="course-desc">
                                            <h4 class="course-title">
                                                <a href="{!! url('course',$item->slug) !!}.html">{!! $item->name !!}</a>
                                            </h4>
                                            {!! $item->description !!}
                                        </div>
                                    </div>
                                    <div class="course-footer">
                                        <div class="course-seats">
                                            <i class="fa fa-users"></i> {!! $item->slot !!} HỌC VIÊN
                                        </div>
                                        <div class="course-button">
                                            <a href="{!! $item->link !!}">XEM CHI TIẾT</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                <nav aria-label="Page navigation example">
                    @php $paginator = $courses @endphp
                    {!! $paginator->render('frontend.layouts.pagination') !!}
                </nav>
            @else
            <!-- 404 Page Area Start Here -->
                <div class="error-page-area sec-spacer">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 error-page-message">
                                <div class="error-page">
                                    <h1>404</h1>
                                    <p>Không tìm thấy kết khóa học phù hợp với <span style="color: #ff3115;">{!! $keyword !!}</span></p>
                                    <div class="home-page">
                                        <a href="{!! url('/') !!}">Trở về trang chủ</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 404 Page Area End Here -->
            @endif

        </div>
    </div>
    <!-- Courses End -->

    <!-- Partner Start -->
    @include('frontend.layouts.partner')
    <!-- Partner End -->

@endsection