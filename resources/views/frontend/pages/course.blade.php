@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_description)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)

@section('content')

    <!-- Courses Start -->
    <div id="rs-courses-3" class="rs-courses-3 sec-spacer">
        <div class="container">
            <div class="abt-title">
                <h2>{!! $pageContent->name !!}</h2>
            </div>
            <div class="gridFilter">
                <button class="category-item active" onclick="getCourseData(null, 1)">ALL</button>
                @foreach(\App\Helpers\Common::getMenuCategories() as $cate)
                    <button class="category-item " id="button-{!! $cate['id'] !!}" onclick="getCourseData({!! $cate['id'] !!}, 1)">{!! $cate['name'] !!}</button>
                @endforeach
            </div>
            <div id="courseList">
                @foreach($courses->chunk(3) as $course)
                    <div class="row grid">
                        @foreach($course as $key => $item)
                            <div class="col-lg-4 col-md-6 grid-item filter1">
                                <div class="course-item">
                                    <div class="course-img">
                                        <img src="{!! url('upload/images',$item->img) !!}" alt="{!! $item->name !!}" />
                                        <span class="course-value">{!! number_format($item->price) !!}</span>
                                        <div class="course-toolbar">
                                            <h4 class="course-category">
                                                <a href="{!! url('category',\App\Helpers\Common::getCategoryByID($item->category_id)['slug']) !!}.html">
                                                    {!! \App\Helpers\Common::getCategoryByID($item->category_id)['name'] !!}
                                                </a>
                                            </h4>
                                            <div class="course-date">
                                                <i class="fa fa-calendar"></i> {!! date('d-m-Y', strtotime($item->created_at)) !!}
                                            </div>
                                            <div class="course-duration">
                                                <i class="fa fa-clock-o"></i> {!! $item->time !!} năm
                                            </div>
                                        </div>
                                    </div>
                                    <div class="course-body">
                                        <div class="course-desc">
                                            <h4 class="course-title">
                                                <a href="{!! url('course',$item->slug) !!}.html">{!! $item->name !!}</a>
                                            </h4>
                                            {!! $item->description !!}
                                        </div>
                                    </div>
                                    <div class="course-footer">
                                        <div class="course-seats">
                                            <i class="fa fa-users"></i> {!! $item->slot !!} HỌC VIÊN
                                        </div>
                                        <div class="course-button">
                                            <a href="{!! $item->link !!}">XEM CHI TIẾT</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                <nav aria-label="Page navigation example">
                    @php $paginator = $courses @endphp
                    {!! $paginator->render('frontend.layouts.pagination') !!}
                </nav>
            </div>
        </div>
    </div>
    <!-- Courses End -->

    <!-- Partner Start -->
    @include('frontend.layouts.partner')
    <!-- Partner End -->

    <script>
        let perPage = 1;
        function getCourseData(cateID, page){
            $('.category-item').removeClass('active');
            $('#button-'+cateID).addClass('active');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/ajax/list-course?page='+page,
                type: 'GET',
                cache: false,
                data: {
                    cate: cateID,
                },
                success: function (data) {
                    $("#courseList").html(data.html);
                },
                error: function (data) {

                }
            });
        }

        function getDataPrev(cate){
            perPage--;
            getCourseData(cate, perPage);
        }
        function getDataNext(cate){
            perPage++;
            getCourseData(cate, perPage);
        }
    </script>

@endsection