@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_description)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)
@section('pageName', 'page-parent solutions')
@section('pageID', $pageContent->id)

@section('content')

    <div id="title-banner" style="background: url({!! url('upload/images',$pageContent->image) !!}) center center no-repeat;background-size: cover;">
        <div class="contained middle">
            <h1>{!! $pageContent->title !!}</h1>
        </div>
    </div>
    <section>
        <!-- Intro -->
        <div id="intro" class="contained">
            {!! $pageContent->content !!}
        </div>

        @foreach($solutions as $key => $item)
            @if($key == 0)
                <div class="contained col-padding solution-item">
                    <div id="info-puzzle" class="solutions-view-specs"
                         data-end-retrieval="{!! $item->time !!}"  data-end-levels="{!! $item->level !!}" data-end-installed="{!! $item->space !!}">View the specs...</div>
                    <div class="solutions-specs">
                        <div class="solutions-time">
                            <h3><span id="info-puzzle-retrieval">0</span></h3>
                            <p>Average Retrieval Time<span>(in seconds)</span></p>
                        </div>
                        <div class="solutions-levels">
                            <h3><span id="info-puzzle-levels">0</span></h3>
                            <p>Maximum Levels</p>
                        </div>
                        <div class="solutions-installed">
                            <h3><span id="info-puzzle-installed">0</span></h3>
                            <p>Spaces Installed</span></p>
                        </div>
                    </div>
                    <div class="col-l">
                        <div id="svg-puzzle" class="svg-wrapper">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;" xml:space="preserve">
                                <path id="puzzle-tl" d="M-345,257v47h47v-47H-345z M-304,298h-35v-35h35V298z"/>
                                <path id="puzzle-tr" d="M-292,257v47h47v-47H-292z M-251,298h-35v-35h35V298z"/>
                                <path id="puzzle-br" d="M-292,310v47h47v-47H-292z M-251,351h-35v-35h35V351z"/>
                                <path id="puzzle-bl" d="M-345,310v47h47v-47H-345z M-304,351h-35v-35h35V351z"/>
                            </svg>
                        </div>
                    </div>
                    <div class="col-r">
                        <h3>{!! $item->name !!}</h3>
                        <h4>{!! $item->tile !!}</h4>
                        {!! $item->content !!}
                    </div>
                    <a href="{!! url('giai-phap',$item['slug']) !!}.html" class="solutions-learn-more">Xem thêm</a>
                </div>
            @elseif($key == 1)
                <div class="contained col-padding solution-item">
                    <div id="info-tower" class="solutions-view-specs"
                         data-end-retrieval="{!! $item->time !!}"  data-end-levels="{!! $item->level !!}" data-end-installed="{!! $item->space !!}">View the specs...</div>
                    <div class="solutions-specs">
                        <div class="solutions-time">
                            <h3><span id="info-tower-retrieval">0</span></h3>
                            <p>Average Retrieval Time<span>(in seconds)</span></p>
                        </div>
                        <div class="solutions-levels">
                            <h3><span id="info-tower-levels">0</span></h3>
                            <p>Maximum Levels</p>
                        </div>
                        <div class="solutions-installed">
                            <h3><span id="info-tower-installed">0</span></h3>
                            <p>Spaces Installed</span></p>
                        </div>
                    </div>
                    <div class="col-l">
                        <div id="svg-tower" class="svg-wrapper">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;" xml:space="preserve">
                                <style type="text/css">
                                    .st0{fill:#11A4DE;}
                                </style>
                                <g id="object_1tw">
                                    <path id="tower-1" class="st0" d="M-345,304v53h100v-53H-345z M-251,351h-88v-41h88V351z"/>
                                </g>
                                <g id="object_2tw">
                                    <polygon id="tower-2" class="st0" points="-345,350.4 -345,357 -245,310.4 -245,310.2 -245,303.8 	"/>
                                </g>


                                <g id="object_3tw">
                                    <path id="tower-3" class="st0" d="M-345,257v53h100v-53H-345z M-251,304h-88v-41h88V304z"/>
                                </g>


                                <g id="object_4tw">
                                    <polygon id="tower-4" class="st0" points="-345,257 -345,263.6 -245,309.8 -245,303.8 -245,303.6 	"/>
                                </g>

                                <g id="object_5tw">
                                    <polygon id="tower-5" class="st0" points="-245,257 -245,263.6 -345,309.8 -345,303.8 -345,303.6 	"/>
                                </g>


                                <g id="object_7tw">
                                    <polygon id="tower-7" class="st0" points="-345,304.2 -345,310.9 -245,357 -245,351 -245,350.9 	"/>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class="col-r">
                        <h3>{!! $item->name !!}</h3>
                        <h4>{!! $item->tile !!}</h4>
                        {!! $item->content !!}
                    </div>
                    <a href="{!! url('giai-phap',$item['slug']) !!}.html" class="solutions-learn-more">Xem thêm</a>
                </div>
            @elseif($key == 2)
                <div class="contained col-padding solution-item">
                    <div id="info-aisle" class="solutions-view-specs"
                         data-end-retrieval="{!! $item->time !!}"  data-end-levels="{!! $item->level !!}" data-end-installed="{!! $item->space !!}">View the specs...</div>
                    <div class="solutions-specs">
                        <div class="solutions-time">
                            <h3><span id="info-aisle-retrieval">0</span></h3>
                            <p>Average Retrieval Time<span>(in seconds)</span></p>
                        </div>
                        <div class="solutions-levels">
                            <h3><span id="info-aisle-levels">0</span></h3>
                            <p>Maximum Levels</p>
                        </div>
                        <div class="solutions-installed">
                            <h3><span id="info-aisle-installed">0</span></h3>
                            <p>Spaces Installed</span></p>
                        </div>
                    </div>
                    <div class="col-l">
                        <div id="svg-aisle" class="svg-wrapper">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;" xml:space="preserve">
                                <path id="aisle-t" d="M-279,311h-59.3h-6.7v6v34v6h6.7h59.3h6v-6v-34v-6H-279z M-279,351h-59.3v-34h59.3V351z"/>
                                <path id="aisle-m" d="M-251,284.9h-59.3h-6.7v6v34v6h6.7h59.3h6v-6v-34v-6H-251z M-251,324.9h-59.3v-34h59.3V324.9z"/>
                                <path id="aisle-b" d="M-279,257h-59.3h-6.7v6v34v6h6.7h59.3h6v-6v-34v-6H-279z M-279,297h-59.3v-34h59.3V297z"/>
                            </svg>
                        </div>
                    </div>
                    <div class="col-r">
                        <h3>{!! $item->name !!}</h3>
                        <h4>{!! $item->tile !!}</h4>
                        {!! $item->content !!}
                    </div>
                    <a href="{!! url('giai-phap',$item['slug']) !!}.html" class="solutions-learn-more">Xem thêm</a>
                </div>
            @elseif($key == 3)
                <div class="contained col-padding solution-item">
                    <div id="info-stacker" class="solutions-view-specs"
                         data-end-retrieval="{!! $item->time !!}"  data-end-levels="{!! $item->level !!}" data-end-installed="{!! $item->space !!}">View the specs...</div>
                    <div class="solutions-specs">
                        <div class="solutions-time">
                            <h3><span id="info-stacker-retrieval">0</span></h3>
                            <p>Average Retrieval Time<span>(in seconds)</span></p>
                        </div>
                        <div class="solutions-levels">
                            <h3><span id="info-stacker-levels">0</span></h3>
                            <p>Maximum Levels</p>
                        </div>
                        <div class="solutions-installed">
                            <h3><span id="info-stacker-installed">0</span></h3>
                            <p>Spaces Installed</span></p>
                        </div>
                    </div>
                    <div class="col-l">
                        <div id="svg-stacker" class="svg-wrapper">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;" xml:space="preserve">
                                <style type="text/css">
                                    .st0{fill:#11A4DE;}
                                </style>
                                <g id="object_3s">
                                    <path class="st0" d="M-345,329v28h100v-28H-345z M-251,351h-88v-16h88V351z"/>
                                </g>
                                <g id="object_2s">
                                    <path class="st0" d="M-345,293v28h100v-28H-345z M-251,315h-88v-16h88V315z"/>
                                </g>
                                <g id="object_1s">
                                    <path class="st0" d="M-345,257v28h100v-28H-345z M-251,279h-88v-16h88V279z"/>
                                </g>

                            </svg>
                        </div>
                    </div>
                    <div class="col-r">
                        <h3>{!! $item->name !!}</h3>
                        <h4>{!! $item->tile !!}</h4>
                        {!! $item->content !!}
                    </div>
                    <a href="{!! url('giai-phap',$item['slug']) !!}.html" class="solutions-learn-more">Xem thêm</a>
                </div>
            @endif
        @endforeach
    </section>

    <!-- Service Footer -->
    @include('frontend.layouts.service')

@endsection