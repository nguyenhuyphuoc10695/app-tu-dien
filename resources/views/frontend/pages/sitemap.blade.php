@extends('frontend.layouts.master')
@section('title', $pageContent->meta_title)
@section('description', $pageContent->meta_decription)
@section('keywords', $pageContent->meta_keywords)
@section('image', $pageContent->img)

@section('content')
    <div class="page-content">


        <div class="sitemap-page">
            <div>
                <section class="page-hero">
                    <div class="container">
                        <div class="row">
                            <picture>
                                <img class="img-responsive" src="{!! url('upload/images',$pageContent->img) !!}"
                                     alt="{!! $pageContent->meta_title !!}"
                                     title="{!! $pageContent->title !!}" />
                            </picture>
                        </div>
                    </div>
                </section>
            </div>
            <div class="container">
                <div class="row">
                    <h1 class="title underline-full text-center"><span>{!! $pageContent->title !!}</span></h1>
                </div>
            </div>
            <div>
                <section class="sitemap-section">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div data-jc="sitemapUI" class="sitemap-nav" style="display: none;">
                                    <div class="row"></div>
                                    <ul class="sitemap-list row list-unstyled">
                                        <li><a href="{!! url('/') !!}">HOME</a></li>
                                        @foreach(\App\Helpers\Common::getMenuPosition(1, \App\Helpers\Common::getLang()) as $key => $item)
                                        <li><a href="{!! url('page', $item['slug']) !!}.html">{!! $item['name'] !!}</a></li>
                                        @endforeach
                                        @foreach(\App\Helpers\Common::getMenuPosition(2, \App\Helpers\Common::getLang()) as $key => $item)
                                            <li><a href="{!! url('page', $item['slug']) !!}.html">{!! $item['name'] !!}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>


    </div>
@endsection