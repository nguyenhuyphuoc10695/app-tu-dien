@if(count($comments) > 0)
    @foreach($comments as $key => $item)
        <li>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="image-comments"><img src="{!! asset('frontend') !!}/images/blog-details/comment.png"
                                                     alt="{!! $item['name'] !!}"></div>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                            <span class="reply"> <span class="date">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    {!! date('F d, Y', strtotime($item['created_at'])) !!}
                                                </span>
                                            </span>
                    <div class="dsc-comments">
                        <h4>{!! $item['name'] !!}</h4>
                        {!! $item['content'] !!}
                        <a href="#"> Reply</a>
                    </div>
                </div>
            </div>
        </li>
        @if(isset($item['children']))
            @foreach($item['children'] as $k => $val)
                <li class="children">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="image-comments"><img src="{!! asset('frontend') !!}/images/blog-details/comment.png"
                                                             alt="{!! $val['name'] !!}"></div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                        <span class="reply"> <span class="date">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                {!! date('F d, Y', strtotime($val['created_at'])) !!}
                                            </span>
                                        </span>
                            <div class="dsc-comments">
                                <h4>{!! $val['name'] !!}</h4>
                                {!! $val['content'] !!}
                                <a href="#"> Reply</a>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach
        @endif

    @endforeach
@endif
<style>
    .children {
        margin-left: 100px !important;
    }
</style>