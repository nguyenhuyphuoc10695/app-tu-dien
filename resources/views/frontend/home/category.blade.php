@if(count($posts) > 0)
    <div class="td-block-row">
    @foreach($posts as $item)
        <div class="td-block-span4">

            <div class="td_module_mx4 td_module_wrap td-animation-stack td-meta-info-hide">
                <div class="td-module-image">
                    <div class="td-module-thumb">
                        <a href="{!! url('tin-tuc',$item['slug']) !!}.html"
                           rel="bookmark" class="td-image-wrap"
                           title="{!! $item['name'] !!}">
                            <img width="218" height="150" class="entry-thumb category-0-img"
                                 src="{!! url('upload/images',$item['img']) !!}"
                                 sizes="(max-width: 218px) 100vw, 218px" alt="{!! $item['name'] !!}"
                                 title="{!! $item['name'] !!}"/>
                        </a>
                    </div>
                    <a href="{!! url('category',\App\Helpers\Common::getPostCategory($item['category_id'])['slug']) !!}.html"
                       class="td-post-category">{!! \App\Helpers\Common::getPostCategory($item['category_id'])['name'] !!}
                    </a>
                </div>

                <h3 class="entry-title td-module-title">
                    <a href="{!! url('tin-tuc',$item['slug']) !!}.html"
                       rel="bookmark"
                       title="{!! $item['name'] !!}">
                        {!! $item['name'] !!}
                    </a>
                </h3>
            </div>


        </div>
    @endforeach
    </div>
    <ul class="pagination" role="navigation">
        <li class="page-item" aria-disabled="true" aria-label="« Previous">
            <a class="page-link" href="javascript:void(0)" @if (!$posts->onFirstPage()) onclick="getDataCategoryPrev()" @endif rel="prev" aria-label="« Previous">‹</a>
        </li>

        <li class="page-item">
            <a class="page-link" href="javascript:void(0)" @if ($posts->hasMorePages()) onclick="getDataCategoryNext()" @endif rel="next" aria-label="Next »">›</a>
        </li>
    </ul>
@endif
<style>
    ul.pagination li a:hover {
        background-color: #009688;
        color: white;
    }
    body.td-animation-stack-type0 .post img, body.td-animation-stack-type0 .td-animation-stack .entry-thumb {
        opacity: 1 !important;
    }
</style>