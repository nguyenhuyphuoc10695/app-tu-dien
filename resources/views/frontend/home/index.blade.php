@extends('frontend.layouts.master')
@section('title', \App\Helpers\Common::getSettings()->meta_title)
@section('description', \App\Helpers\Common::getSettings()->meta_description)
@section('keywords', \App\Helpers\Common::getSettings()->meta_keywords)
@section('image', \App\Helpers\Common::getSettings()->img)
@section('pageName', 'home')
@section('pageID', 0)

@section('content')

    <div id="home">
        <div class="section active" id="section-0">
            @foreach(\App\Helpers\Common::getSlides() as $key => $item)
                <div class="home-container content">
                    <h1>{!! $item->name !!}</h1>
                    <h3>{!! strip_tags($item->content, '') !!}</h3>
                </div>
                <div id="video-bg">
                    <div id="video_overlay"></div>
                    <video id="subvideo" autoplay="" loop="">
                        <source type="video/mp4"
                                src="{!! $item->link !!}">
                        {{--<source type="video/webm"--}}
                                {{--src="https://cityliftparking.com/wp-content/themes/citylift-v2/video/city-lift-feature.webm">--}}
                    </video>
                </div>
            @endforeach

        </div>
        <div class="section" id="section-1">
            <div class="home-container content">
                @if($approach)
                    {!! $approach->description !!}
                    <a class="blue-btn" href="{!! url('page',$approach->slug) !!}.html" role="button">{!! $approach->name !!}</a>
                @endif
            </div>
        </div>
        <div class="section" id="section-2">
            <div class="home-container content">
                <div class="home-container-left">
                    @if($solution)
                        <div>
                            {!! $solution->description !!}
                            <a class="blue-btn" href="{!! url('page',$solution->slug) !!}.html" role="button">{!! $solution->name !!}</a>
                        </div>
                    @endif
                </div>
                <div class="home-container-right">
                    <div id="svg-home-box" class="svg-wrapper-home svg home-start">
                        <svg version="1.1" id="object_5" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 432 432"
                             enable-background="new 0 0 432 432" xml:space="preserve">
                            <polyline fill="none" stroke="#23B3E6" stroke-width="8" stroke-miterlimit="10"
                                      points="329.9,138.6 289.5,138.6 289.5,28.6 399.5,28.6 399.5,138.6 356.5,138.6"/>
                            <polyline fill="none" stroke="#93daf3" stroke-width="8" stroke-miterlimit="10"
                                      points="202,138.6 159.5,138.6 159.5,28.6 269.5,28.6 269.5,138.6 215.5,138.6 215.5,334.8"/>
                            <rect x="29.5" y="28.6" fill="none" stroke="#93daf3" stroke-width="8" stroke-miterlimit="10"
                                  width="110" height="110"/>
                            <rect x="289.5" y="158.6" fill="none" stroke="#c9ebf7" stroke-width="8"
                                  stroke-miterlimit="10"
                                  width="110" height="110"/>
                            <polyline fill="none" stroke="#23B3E6" stroke-width="8" stroke-miterlimit="10"
                                      points="139.5,226.6 139.5,268.6 29.5,268.6 29.5,158.6 139.5,158.6 139.5,212.9 342.5,212.9 342.5,69.8 "/>
                            <rect x="29.5" y="288.6" fill="none" stroke="#c9ebf7" stroke-width="8"
                                  stroke-miterlimit="10"
                                  width="110" height="110"/>
                            <circle fill="none" stroke="#93daf3" stroke-width="8" stroke-miterlimit="10" cx="215.5"
                                    cy="342.2" r="11.5" transform="rotate(-90 215.5 342.2)"/>
                            <polyline fill="none" stroke="#93daf3" stroke-width="8" stroke-miterlimit="10"
                                      points="229,288.6 269.5,288.6 269.5,398.6 159.5,398.6 159.5,288.6 202.3,288.6 "/>
                            <rect x="289.5" y="288.6" fill="none" stroke="#93daf3" stroke-width="8"
                                  stroke-miterlimit="10"
                                  width="110" height="110"/>
                            <line fill="none" stroke="#23B3E6" stroke-width="8" stroke-linecap="square"
                                  stroke-linejoin="bevel" stroke-miterlimit="10" x1="342.5" y1="68" x2="325.5" y2="85"/>
                            <line fill="none" stroke="#23B3E6" stroke-width="8" stroke-linecap="square"
                                  stroke-linejoin="bevel" stroke-miterlimit="10" x1="342.5" y1="68" x2="359.5" y2="85"/>
                        </svg>
                    </div>
                </div>
            </div>

        </div>
        <div class="section" id="section-3">
            <div class="home-container content">
                @if($service)
                    {!! $service->description !!}
                    <a class="blue-btn" href="{!! url('page',$service->slug) !!}.html" role="button">{!! $service->name !!}</a>
                @endif
            </div>
        </div>
        <div class="section" id="section-4">
            <div class="home-container content">
                @if (Session::has('flash_message'))
                    <div class="alert alert-{{ Session::get('flash_level') }} icons-alert">
                        <strong>{!! Session::get('flash_message') !!}</strong>
                    </div>
                @endif

                <div id="contact-swapper" class="col-60">
                    @if($contact)
                        {!! $contact->description !!}
                    @endif
                </div>
                <div class="col-40">
                    <div role="form" class="" id="wpcf7-f751-o1" lang="en-US" dir="ltr">
                        <div class="screen-reader-response"></div>
                        <form action="{!! route('save_contact') !!}" method="post" class="wpcf7-form" novalidate="novalidate">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <div class="form-group has-error">
                                    <span class="help-block">{!! $errors->first('name') !!}</span>
                                </div>
                                <span class="wpcf7-form-control-wrap your-name">
                            <input type="text" name="name" value=""size="40"
                                   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-lg"
                                   id="first-name-field" aria-required="true" aria-invalid="false" placeholder="Họ tên"/>
                        </span>
                            </div>
                            <div class="form-group">
                                <div class="form-group has-error">
                                    <span class="help-block">{!! $errors->first('company') !!}</span>
                                </div>
                                <span class="wpcf7-form-control-wrap your-name">
                            <input type="text" name="company" value=""size="40"
                                   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-lg"
                                   id="first-name-field" aria-required="true" aria-invalid="false" placeholder="Tên công ty"/>
                        </span>
                            </div>

                            <div class="form-group">
                                <div class="form-group has-error">
                                    <span class="help-block">{!! $errors->first('address') !!}</span>
                                </div>
                                <span class="wpcf7-form-control-wrap your-name">
                            <input type="text" name="address" value=""size="40"
                                   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-lg"
                                   id="first-name-field" aria-required="true" aria-invalid="false" placeholder="Địa chỉ"/>
                        </span>
                            </div>
                            <div class="form-group">
                                <div class="form-group has-error">
                                    <span class="help-block">{!! $errors->first('email') !!}</span>
                                </div>
                                <span class="wpcf7-form-control-wrap your-name">
                            <input type="email" name="email" value=""size="40"
                                   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-lg"
                                   id="first-name-field" aria-required="true" aria-invalid="false" placeholder="Email"/>
                        </span>
                            </div>

                            <div class="row">
                                <div class="col-50 email-address-field-wrapper">
                                    <div class="form-group">
                                        <div class="form-group has-error">
                                            <span class="help-block">{!! $errors->first('space') !!}</span>
                                        </div>
                                        <span class="wpcf7-form-control-wrap your-email">
                                    <input type="text" name="space" value="" size="40" aria-required="true"
                                           class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-lg"
                                           id="email-address-field" aria-invalid="false" placeholder="Số chỗ đậu xe"/>
                                </span>
                                    </div>
                                </div>
                                <div class="col-50 phone-number-field-wrapper">
                                    <div class="form-group">
                                        <div class="form-group has-error">
                                            <span class="help-block">{!! $errors->first('phone') !!}</span>
                                        </div>
                                        <span class="wpcf7-form-control-wrap your-phone">
                                    <input type="text" name="phone" value="" size="40" id="phone-number-field"
                                           class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control input-lg"
                                           aria-required="true" aria-invalid="false" placeholder="Số điện thoại"/>
                                </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea rows="3" class="form-control" name="message" placeholder="Ghi chú"></textarea>
                            </div>
                            <p><input type="submit" value="Submit"
                                      class="wpcf7-form-control wpcf7-submit btn btn-primary blue-btn"/></p>
                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="footer-home"></div>

@endsection