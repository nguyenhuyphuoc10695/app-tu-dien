@if(count($posts) > 0)
    @foreach($posts as $item)
        <div class="td_module_16 td_module_wrap td-animation-stack td-meta-info-hide">
            <div class="td-module-thumb">
                <a href="{!! url('tin-tuc',$item['slug']) !!}.html"
                   rel="bookmark" class="td-image-wrap"
                   title="{!! $item['name'] !!}">
                    <img width="150" height="" class="entry-thumb"
                         src="{!! url('upload/images',$item['img']) !!}"
                         alt="{!! $item['name'] !!}"
                         title="{!! $item['name'] !!}"/>
                </a>
            </div>
            <div class="item-details">
                <h3 class="entry-title td-module-title">
                    <a href="{!! url('tin-tuc',$item['slug']) !!}.html"
                       rel="bookmark"
                       title="{!! $item['name'] !!}">
                        {!! $item['name'] !!}
                    </a>
                </h3>
            </div>

        </div>
    @endforeach
    <ul class="pagination" role="navigation">
        <li class="page-item" aria-disabled="true" aria-label="« Previous">
            <a class="page-link" href="javascript:void(0)" @if (!$posts->onFirstPage()) onclick="getCarePostPrev({!! $category !!})" @endif rel="prev" aria-label="« Previous">‹</a>
        </li>

        <li class="page-item">
            <a class="page-link" href="javascript:void(0)" @if ($posts->hasMorePages()) onclick="getCarePostNext({!! $category !!})" @endif rel="next" aria-label="Next »">›</a>
        </li>
    </ul>
@endif
<style>
    ul.pagination li a:hover {
        background-color: #009688;
        color: white;
    }
    body.td-animation-stack-type0 .post img, body.td-animation-stack-type0 .td-animation-stack .entry-thumb {
        opacity: 1 !important;
    }
</style>