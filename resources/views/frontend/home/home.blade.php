@extends('frontend.layouts.master_home')
@section('title', \App\Helpers\Common::getSettings(\App\Helpers\Common::getLang())->meta_title)
@section('description', \App\Helpers\Common::getSettings(\App\Helpers\Common::getLang())->meta_decription)
@section('keywords', \App\Helpers\Common::getSettings(\App\Helpers\Common::getLang())->meta_keywords)
@section('image', \App\Helpers\Common::getSettings(\App\Helpers\Common::getLang())->img)

@section('content')
    <div class="td-main-content-wrap td-main-page-wrap td-container-wrap">
        <div class="td-container td-pb-article-list ">
            <div class="td-pb-row">
                <div class="td-pb-span8 td-main-content" role="main">
                    <div class="td-ss-main-content td_block_template_1">
                        <div class="td-block-title-wrap">
                            <h4 class="block-title td-block-title">
                                <span class="td-pulldown-size">TIN MỚI NHẤT</span>
                            </h4>
                        </div>
                        @foreach($posts->chunk(2) as $post)
                            <div class="td-block-row">
                                @foreach($post as $item)
                                    <div class="td-block-span6">
                                        <div class="td_module_1 td_module_wrap td-animation-stack td-meta-info-hide">
                                            <div class="td-module-image">
                                                <div class="td-module-thumb">
                                                    <a href="{!! url('tin-tuc',$item['slug']) !!}.html"
                                                       rel="bookmark" class="td-image-wrap"
                                                       title="{!! $item['name'] !!}">
                                                        <img width="324" height="160" class="entry-thumb"
                                                             data-src="{!! url('upload/images',$item['img']) !!}"
                                                             src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                                                             sizes="(max-width: 324px) 100vw, 324px" alt="{!! $item['name'] !!}"
                                                             title="{!! $item['name'] !!}"/>
                                                    </a>
                                                </div>
                                                <a href="{!! url('category',\App\Helpers\Common::getPostCategory($item['category_id'])['slug']) !!}.html"
                                                   class="td-post-category">{!! \App\Helpers\Common::getPostCategory($item['category_id'])['name'] !!}
                                                </a>
                                                <h3 class="entry-title td-module-title">
                                                    <a href="{!! url('tin-tuc',$item['slug']) !!}.html"
                                                       rel="bookmark"
                                                       title="{!! $item['name'] !!}">
                                                        {!! $item['name'] !!}
                                                    </a>
                                                </h3>
                                                <div class="td-module-meta-info">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                        {!! $posts->links() !!}
                    </div>
                </div>
                <div class="td-pb-span4 td-main-sidebar" role="complementary">
                    <div class="td-ss-main-sidebar">
                    </div>
                </div>
            </div> <!-- /.td-pb-row -->
        </div>
    </div>
@endsection