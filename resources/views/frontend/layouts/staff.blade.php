<div id="rs-team" class="rs-team sec-spacer">
    <div class="container">
        <div class="abt-title mb-70 text-center">
            <h2>OUR EXPERIENCED STAFFS</h2>
            <p>Considering desire as primary motivation for the generation of narratives is a useful concept.</p>
        </div>
        <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="false"
             data-autoplay-timeout="5000" data-smart-speed="1200" data-dots="true" data-nav="true" data-nav-speed="false"
             data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="true" data-ipad-device="2"
             data-ipad-device-nav="true" data-ipad-device-dots="true" data-md-device="4" data-md-device-nav="true"
             data-md-device-dots="true">
            @foreach(\App\Helpers\Common::getStaffList() as $key => $item)
                <div class="team-item">
                    <div class="team-img">
                        <img src="{!! url('upload/images',$item->img) !!}" alt="{!! $item->name !!}" />
                        {{--<div class="normal-text">--}}
                        {{--<h3 class="team-name">{!! $item->name !!}</h3>--}}
                        {{--<span class="subtitle">{!! $item->position !!}</span>--}}
                        {{--</div>--}}
                    </div>
                    <div class="staff-content @if($key%2 > 0) staff-content-2 @endif">
                        <h3 class="name">{!! $item->name !!}</h3>
                        <div class="description">
                            {!! $item->content !!}
                        </div>
                    </div>
                    {{--<div class="team-content">--}}
                    {{--<div class="overly-border"></div>--}}
                    {{--<div class="display-table">--}}
                    {{--<div class="display-table-cell">--}}
                    {{--<h3 class="team-name"><a href="#">{!! $item->name !!}</a></h3>--}}
                    {{--<span class="team-title">{!! $item->position !!}</span>--}}
                    {{--<p class="team-desc">{!! strip_tags($item->content, '') !!}</p>--}}
                    {{--<div class="team-social">--}}
                    {{--<a href="{!! $item->facebook !!}" class="social-icon"><i class="fa fa-facebook"></i></a>--}}
                    {{--<a href="{!! $item->google !!}" class="social-icon"><i class="fa fa-google-plus"></i></a>--}}
                    {{--<a href="{!! $item->twitter !!}" class="social-icon"><i class="fa fa-twitter"></i></a>--}}
                    {{--<a href="{!! $item->pinterest !!}" class="social-icon"><i class="fa fa-pinterest-p"></i></a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            @endforeach
        </div>
    </div>
</div>
<style>
    .staff-content {
        background: #ec1f27;
        padding: 25px;
        text-align: center;
    }
    .staff-content-2 {
        background: #fcaf17 !important;
    }
    .staff-content h3.name {
        font-size: 14px;
        margin: 0;
        color: #fff;
        text-transform: uppercase;
        padding-bottom: 7px;
    }
    .staff-content .description p, .staff-content .description  {
        color: #fff;
        font-size: 12px;
        margin: 0;
    }
</style>