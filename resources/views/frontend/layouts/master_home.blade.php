<!doctype html>
<!--[if lt IE 7]>
<html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>
<html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>
<html lang="en" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">


    <!-- Meta -->
    <link rel="shortcut icon" href="{!! asset('frontend/wp-content/themes/citylift-v2/img/CityLift_favicon.ico') !!}" type="image/icon">
    <link rel="icon" href="{!! asset('frontend/wp-content/themes/citylift-v2/img/CityLift_favicon.ico') !!}" type="image/icon">


    <!-- Meta -->
    <meta property="og:site_name" value="CityLift -v2"/>
    <meta property="og:type" value="Website"/>
    <meta property="og:title" content="CityLift / Take the Space out of Parking"/>
    <meta property="og:url" content="http://cityliftparking.com/"/>
    <meta property="og:image" content="https://cityliftparking.com/wp-content/uploads/2015/09/meta-image.png"/>
    <meta property="og:description"
          content="CityLift makes every square inch count by reducing your parking footprint by 40-80%."/>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{!! asset('frontend/wp-content/themes/citylift-v2/style.css') !!}"/>

    <!-- Scripts -->

    <!-- This site is optimized with the Yoast SEO plugin v7.0.3 - https://yoast.com/wordpress/plugins/seo/ -->
    <title>Turning Parking Spaces into People Spaces | CityLift</title>
    <meta name="description"
          content="We see each project as a partnership, acting as expert consultants to help our clients create customized parking solutions in space-challenged areas."/>
    <link rel="canonical" href="approach.html"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Turning Parking Spaces into People Spaces | CityLift"/>
    <meta property="og:description"
          content="We see each project as a partnership, acting as expert consultants to help our clients create customized parking solutions in space-challenged areas."/>
    <meta property="og:url" content="https://cityliftparking.com/approach"/>
    <meta property="og:site_name" content="CityLift"/>
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description"
          content="We see each project as a partnership, acting as expert consultants to help our clients create customized parking solutions in space-challenged areas."/>
    <meta name="twitter:title" content="Turning Parking Spaces into People Spaces | CityLift"/>


    <link rel='dns-prefetch' href='https://cdnjs.cloudflare.com'/>
    <link rel='dns-prefetch' href='https://s.w.org'/>
    <link rel='stylesheet' id='fullpage-css-css' href='{!! asset('frontend/wp-content/themes/citylift-v2/css/jquery.fullPage.css') !!}'
          type='text/css' media='all'/>
    <script type='text/javascript' src='{!! asset('frontend/wp-includes/js/jquery/jquery.js') !!}'></script>
    <script type='text/javascript' src='{!! asset('frontend/wp-includes/js/jquery/jquery-migrate.min.js') !!}'></script>
    <script type='text/javascript'
            src='https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>
    <script type='text/javascript'
            src='{!! asset('frontend/wp-content/themes/citylift-v2/js/vendor/waypoints/jquery.waypoints.min.js') !!}'></script>
    <script type='text/javascript'
            src='{!! asset('frontend/wp-content/themes/citylift-v2/js/vendor/greensock/DrawSVGPlugin.min.js') !!}'></script>
    <script type='text/javascript' src='{!! asset('frontend/wp-content/themes/citylift-v2/js/vendor/greensock/TweenMax.min.js') !!}'></script>
    <script type='text/javascript' src='{!! asset('frontend/wp-content/themes/citylift-v2/js/svg.js') !!}'></script>
    <link rel='https://api.w.org/' href='{!! asset('frontend/wp-json/index.html') !!}'/>
    <link rel='shortlink' href='approach.html'/>

    <!-- HFCM by 99 Robots - Snippet # 1: Facebook Pixel -->


    <!-- /end HFCM by 99 Robots -->
    <!-- HFCM by 99 Robots - Snippet # 2: CF7 Submit -->

    {{--<script type="text/javascript" src="https://www.33infra-strat.com/js/79545.js"></script>--}}
    <noscript><img src="https://www.33infra-strat.com/79545.png" style="display:none;"/></noscript>

    <script type="text/javascript"
            src="https://cdn.callrail.com/companies/590986980/413b9e04465f6eb06469/12/swap.js"></script>
</head>

<body class="page-template-default page page-id-24 approach preload">

<!--Full width header Start-->
@include('frontend.layouts.header')
<!--Full width header End-->
@include('frontend.layouts.nav')

@yield('content')

<!-- Footer Start -->
@include('frontend.layouts.footer')


<link rel='stylesheet' id='contact-form-7-css' href='{!! asset('frontend/wp-content/plugins/contact-form-7/includes/css/styles.css') !!}' type='text/css' media='all'/>
<script type='text/javascript' src='{!! asset('frontend/wp-content/themes/citylift-v2/js/vendor/fullpage/jquery.fullPage.min.js') !!}'></script>
<script type='text/javascript' src='{!! asset('frontend/wp-content/themes/citylift-v2/js/jquery.main.js') !!}'></script>
<script type='text/javascript' src='{!! asset('frontend/wp-includes/js/wp-embed.min.js') !!}'></script>
</body>
</html>