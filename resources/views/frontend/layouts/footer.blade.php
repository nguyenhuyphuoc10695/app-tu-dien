<footer>
    <div class="contained">
        <div class="col-l">
            {!! \App\Helpers\Common::getSettings()->content !!}
            <p class="footer">{!! strip_tags(\App\Helpers\Common::getSettings()->map, '') !!}<br>
                {!! \App\Helpers\Common::getSettings()->hotline !!}
                <a href="mailto:{!! \App\Helpers\Common::getSettings()->email !!}">{!! \App\Helpers\Common::getSettings()->email !!}</a>
            </p>
        </div>
        <div class="col-r">
            <ul id="menu-footer-col-1" class="menu">
                @foreach(\App\Helpers\Common::getMenuList(0) as $key => $item)
                    @if($key < 2)
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{!! $item['id'] !!}">
                            <a href="{!! url('page',$item['slug']) !!}.html">{!! $item['name'] !!}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
            <ul id="menu-footer-col-2" class="menu">
                @foreach(\App\Helpers\Common::getMenuList(0) as $key => $item)
                    @if($key > 1 && $key < 4)
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{!! $item['id'] !!}">
                            <a href="{!! url('page',$item['slug']) !!}.html">{!! $item['name'] !!}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
            <ul id="menu-footer-col-3" class="menu">
                @foreach(\App\Helpers\Common::getMenuList(0) as $key => $item)
                    @if($key > 3 && $key < 6)
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{!! $item['id'] !!}">
                            <a href="{!! url('page',$item['slug']) !!}.html">{!! $item['name'] !!}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
            <ul id="menu-footer-col-4" class="menu">
                @foreach(\App\Helpers\Common::getMenuList(0) as $key => $item)
                    @if($key > 5)
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{!! $item['id'] !!}">
                            <a href="{!! url('page',$item['slug']) !!}.html">{!! $item['name'] !!}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
            <p class="footer">&copy;2019 GreenP. All rights reserved.</p>
        </div>
    </div>
</footer>
