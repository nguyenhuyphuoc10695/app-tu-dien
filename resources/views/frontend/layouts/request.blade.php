<!-- Service Requests -->
<div id="service-requests">
    <div class="contained">
        <h3>Gửi yêu cầu dịch vụ</h3>
        <p>Hãy liên hệ với chúng tôi khi bạn cần sự hỗ trợ</p>
        <h3 class="blue">{!! \App\Helpers\Common::getSettings()->hotline !!}</h3>
    </div>
</div><!-- Service Footer -->