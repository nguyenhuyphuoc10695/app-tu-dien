<div class="td-menu-background"></div>
<div id="td-mobile-nav">
    <div class="td-mobile-container">
        <!-- mobile menu top section -->
        <div class="td-menu-socials-wrap">
            <!-- socials -->
            <div class="td-menu-socials">
                @foreach(\App\Helpers\Common::getSocials() as $item)
                    <span class="td-social-icon-wrap">
                            <a target="_blank" href="{!! $item->link !!}" title="{!! $item->name !!}">
                                <i class="td-icon-font td-icon-{!! $item->slug !!}"></i>
                            </a>
                        </span>
                @endforeach
            </div>
            <!-- close button -->
            <div class="td-mobile-close">
                <a href="#"><i class="td-icon-close-mobile"></i></a>
            </div>
        </div>

        <!-- login section -->

        <!-- menu section -->
        <div class="td-mobile-content">
            <div class="menu-trang-chu-container">
                <ul id="menu-trang-chu" class="td-mobile-main-menu">
                    @foreach(\App\Helpers\Common::getMenuCategories() as $key => $item)
                        <li class="menu-item menu-item-type-taxonomy menu-item-object-category @if(\Request::segment(2) == $item['slug'].'.html' && \Request::segment(1) == 'category') current-menu-item @endif @if($key === 0) menu-item-first @endif td-menu-item td-normal-menu menu-item-48">
                            <a href="{!! url('category',$item['slug']) !!}.html">{!! $item['name'] !!}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="td-search-background"></div>
<div class="td-search-wrap-mob">
    <div class="td-drop-down-search" aria-labelledby="td-header-search-button">
        <form method="get" class="td-search-form" action="{!! route('search') !!}">
            <!-- close button -->
            <div class="td-search-close">
                <a href="#"><i class="td-icon-close-mobile"></i></a>
            </div>
            <div role="search" class="td-search-input">
                <span>Search</span>
                <input id="td-header-search-mob" type="text" value="" name="keywprd" autocomplete="off"/>
            </div>
        </form>
        <div id="td-aj-search-mob"></div>
    </div>
</div>