<nav id="nav-mobile">
    <ul>
        <div class="menu-main-menu-container">
            <li class="@if(\Request::segment(1) == '') current-menu-item  current_page_item @endif menu-item menu-item-type-post_type menu-item-object-page menu-item-0"><a
                        href="{!! url('/') !!}">Trang chủ</a></li>
            @foreach(\App\Helpers\Common::getMenuList(0) as $key => $item)
                @if($key < 3)
                    <li class="@if(\Request::segment(2) == $item['slug'].'.html' || \Request::segment(1) == $item['slug'] || ($item['slug'] == 'tin-tuc' && \Request::segment(1) == 'chi-tiet-tin')) current-menu-item  current_page_item @endif menu-item menu-item-type-post_type menu-item-object-page menu-item-{!! $item['id'] !!}">
                        <a href="{!! url('page',$item['slug']) !!}.html">{!! $item['name'] !!}</a>
                    </li>
                @endif
            @endforeach
        </div>
        <div class="menu-secondary-menu-container">
            @foreach(\App\Helpers\Common::getMenuList(0) as $key => $item)
                @if($key > 2)
                    <li class="@if(\Request::segment(2) == $item['slug'].'.html' || \Request::segment(1) == $item['slug'] || ($item['slug'] == 'tin-tuc' && \Request::segment(1) == 'chi-tiet-tin')) current-menu-item  current_page_item @endif menu-item menu-item-type-post_type menu-item-object-page menu-item-{!! $item['id'] !!}">
                        <a href="{!! url('page',$item['slug']) !!}.html">{!! $item['name'] !!}</a>
                    </li>
                @endif
            @endforeach
        </div>
    </ul>
</nav>