<section class="search-nav">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="flex-wrapper">
                    <div class="sort-by">
                        @foreach(\App\Helpers\Common::getLangCategories(\App\Helpers\Common::getLang()) as $item)
                        <a href="{!! url('category',$item['slug']) !!}.html" class="@if(\Request::segment(2) == $item['slug'].'.html' && \Request::segment(1) == 'category') selected @endif" rel="nofollow">
                            <span>{!! $item['name'] !!}</span>
                        </a>
                        @endforeach
                    </div>
                    <button class="btn-mobile-search" data-jc="searchUX"></button>
                    <form id="search" class="searchform" action="{!! url()->current() !!}" method="GET">
                        <div class="form-group">
                            <input class="form-control" type="text" name="keyword"
                                   placeholder="Search" aria-label="Search" value=""/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <hr class="border-bottom">
</section>