<header>
    <div class="header-contained">
        <div class="header-logo"><a style="background: url({!! url('upload/images',\App\Helpers\Common::getSettings()->img) !!}) center center no-repeat;background-size: contain;" href="{!! url('/') !!}"></a></div>
        <nav class="header-main">
            <ul id="menu-main-menu" class="menu">
                <li class="@if(\Request::segment(1) == '') current-menu-item  current_page_item @endif menu-item menu-item-type-post_type menu-item-object-page menu-item-0"><a
                            href="{!! url('/') !!}">Trang chủ</a></li>
                @foreach(\App\Helpers\Common::getMenuList(0) as $key => $item)
                    @if($key < 3)
                        <li class="@if(\Request::segment(2) == $item['slug'].'.html' || \Request::segment(1) == $item['slug'] || ($item['slug'] == 'tin-tuc' && \Request::segment(1) == 'chi-tiet-tin')) current-menu-item  current_page_item @endif menu-item menu-item-type-post_type menu-item-object-page menu-item-{!! $item['id'] !!}">
                            <a href="{!! url('page',$item['slug']) !!}.html">{!! $item['name'] !!}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </nav>
        <nav class="header-secondary">
            <ul id="menu-secondary-menu" class="menu">
                @foreach(\App\Helpers\Common::getMenuList(0) as $key => $item)
                    @if($key > 2)
                        <li class="@if(\Request::segment(2) == $item['slug'].'.html' || \Request::segment(1) == $item['slug'] || ($item['slug'] == 'tin-tuc' && \Request::segment(1) == 'chi-tiet-tin')) current-menu-item  current_page_item @endif menu-item menu-item-type-post_type menu-item-object-page menu-item-{!! $item['id'] !!}">
                            <a href="{!! url('page',$item['slug']) !!}.html">{!! $item['name'] !!}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </nav>
        <div id="header-button" class="js-menu">
            <div class="toggle">
                <span class="t1"></span>
                <span class="t2"></span>
                <span class="t3"></span>
            </div>
        </div>
    </div>
</header>
