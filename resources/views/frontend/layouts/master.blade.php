<!DOCTYPE html>

<!--[if lt IE 7]>
<html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>
<html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>
<html lang="en" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">


    <!-- Meta -->
    <link rel="shortcut icon" href="{!! asset('images/favicon.png') !!}" type="image/icon">
    <link rel="icon" href="{!! asset('images/favicon.png') !!}" type="image/icon">


    <!-- Meta -->
    <meta property="og:site_name" value="GreenP"/>
    <meta property="og:type" value="Website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{!! url('/') !!}"/>
    <meta property="og:image" content="{!! url('upload/images/') !!}@yield('image')"/>
    <meta property="og:description"
          content="@yield('description')"/>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{!! asset('frontend/wp-content/themes/citylift-v2/style.css') !!}"/>

    <!-- Scripts -->

    <!-- This site is optimized with the Yoast SEO plugin v7.0.3 - https://yoast.com/wordpress/plugins/seo/ -->
    <title>@yield('title')</title>
    <meta name="description"
          content="@yield('description')"/>
    <link rel="canonical" href="{!! url()->current() !!}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:description"
          content="@yield('description')"/>
    <meta property="og:url" content="{!! url('/') !!}"/>
    <meta property="og:site_name" content="GreenP"/>
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description"
          content="@yield('description')"/>
    <meta name="twitter:title" content="@yield('title')"/>
    {{--<script type='application/ld+json'>{--}}
        {{--"@context": "https:\/\/schema.org",--}}
        {{--"@type": "WebSite",--}}
        {{--"@id": "#website",--}}
        {{--"url": "https:\/\/cityliftparking.com\/",--}}
        {{--"name": "CityLift",--}}
        {{--"potentialAction": {--}}
            {{--"@type": "SearchAction",--}}
            {{--"target": "https:\/\/cityliftparking.com\/?s={search_term_string}",--}}
            {{--"query-input": "required name=search_term_string"--}}
        {{--}--}}
    {{--}</script>--}}
    {{--<script type='application/ld+json'>{--}}
        {{--"@context": "https:\/\/schema.org",--}}
        {{--"@type": "Organization",--}}
        {{--"url": "https:\/\/cityliftparking.com\/",--}}
        {{--"sameAs": [],--}}
        {{--"@id": "#organization",--}}
        {{--"name": "CityLift Parking",--}}
        {{--"logo": ""--}}
    {{--}</script>--}}
    <meta name="google-site-verification" content=""/>
    <!-- / Yoast SEO plugin. -->

    <link rel='dns-prefetch' href='https://cdnjs.cloudflare.com'/>
    <link rel='dns-prefetch' href='https://s.w.org'/>
    <link rel='stylesheet' id='owl-carousel-css-css'
          href='{!! asset('frontend/wp-content/themes/citylift-v2/owl-carousel/css/owl.carousel.css') !!}' type='text/css' media='all'/>
    <link rel='stylesheet' id='owl-carousel-theme-css'
          href='{!! asset('frontend/wp-content/themes/citylift-v2/owl-carousel/css/owl.theme.css') !!}' type='text/css' media='all'/>
    <link rel='stylesheet' id='fullpage-css-css' href='{!! asset('frontend/wp-content/themes/citylift-v2/css/jquery.fullPage.css') !!}'
          type='text/css' media='all'/>
    <script type='text/javascript' src='{!! asset('frontend/wp-includes/js/jquery/jquery.js') !!}'></script>
    <script type='text/javascript' src='{!! asset('frontend/wp-includes/js/jquery/jquery-migrate.min.js') !!}'></script>
    <script type='text/javascript'
            src='https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>
    <script type='text/javascript'
            src='{!! asset('frontend/wp-content/themes/citylift-v2/js/vendor/waypoints/jquery.waypoints.min.js') !!}'></script>
    <script type='text/javascript'
            src='{!! asset('frontend/wp-content/themes/citylift-v2/js/vendor/greensock/DrawSVGPlugin.min.js') !!}'></script>
    <script type='text/javascript' src='{!! asset('frontend/wp-content/themes/citylift-v2/js/vendor/greensock/TweenMax.min.js') !!}'></script>
    <script type='text/javascript' src='{!! asset('frontend/wp-content/themes/citylift-v2/js/svg.js') !!}'></script>
    <link rel='https://api.w.org/' href='{!! asset('frontend/wp-json/index.html') !!}'/>
    <link rel='shortlink' href='{!! url()->current() !!}'/>
    {{--<link rel="alternate" type="application/json+oembed"--}}
          {{--href="wp-json/oembed/1.0/embed-url=https_%7C%7Ccityliftparking.com%7C.json"/>--}}
    {{--<link rel="alternate" type="text/xml+oembed"--}}
          {{--href="wp-json/oembed/1.0/embed-url=https_%7C%7Ccityliftparking.com%7C&format=xml.xml"/>--}}
    {{--<script type="text/javascript" src="https://www.33infra-strat.com/js/79545.js"></script>--}}
    <noscript><img src="https://www.33infra-strat.com/79545.png" style="display:none;"/></noscript>

    <script type="text/javascript"
            src="https://cdn.callrail.com/companies/590986980/413b9e04465f6eb06469/12/swap.js"></script>
    <style>
        textarea {
            box-sizing: border-box;
            border: 3px solid #a1a8ad;
            padding: 10px 12px 8px;
            margin-bottom: 12px;
            background-color: transparent;
            width: 90%;
            color: #999;
            text-transform: uppercase;
            font-family: "AvenirNextLTPro-Cn",sans-serif;
            font-style: normal;
            font-size: 16px;
            outline: none;
            outline: 0;
            -webkit-appearance: none;
            -webkit-border-radius: 0;
            border-radius: 0;
            -ms-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            -webkit-box-sizing: content-box;
        }
        .help-block {
            color: red;
            padding-left: 5px;
        }
        .alert {
            padding: 10px;
        }
        .alert-success {
            border: 2px solid #4cae4c;
        }
        .alert-success strong {
            color: #4cae4c;
        }
        .alert-danger {
            border: 2px solid #d43f3a;
        }
        .alert-danger strong {
            color: #d43f3a;
        }
    </style>
</head>

<body class="page-template-default page page-id-@yield('pageID') @yield('pageName') preload">


<!--Full width header Start-->
@include('frontend.layouts.header')
<!--Full width header End-->
@include('frontend.layouts.nav')

@yield('content')

<!-- Footer Start -->
@include('frontend.layouts.footer')
<!-- Footer End -->

<link rel='stylesheet' id='contact-form-7-css' href='{!! asset('frontend/wp-content/plugins/contact-form-7/includes/css/styles.css') !!}'
      type='text/css' media='all'/>
<script type='text/javascript' src='{!! asset('frontend/wp-content/themes/citylift-v2/owl-carousel/js/owl.carousel.js') !!}'></script>
<script type='text/javascript'
        src='{!! asset('frontend/wp-content/themes/citylift-v2/js/vendor/numbers/jquery.animateNumber.min.js') !!}'></script>
<script type='text/javascript' src='{!! asset('frontend/wp-content/themes/citylift-v2/js/vendor/fullpage/jquery.fullPage.min.js') !!}'></script>
<script type='text/javascript' src='{!! asset('frontend/wp-content/themes/citylift-v2/js/jquery.main.js') !!}'></script>
<script type='text/javascript' src='{!! asset('frontend/wp-includes/js/wp-embed.min.js') !!}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/cityliftparking.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }, "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{!! asset('frontend/wp-content/plugins/contact-form-7/includes/js/scripts.js') !!}'></script>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>