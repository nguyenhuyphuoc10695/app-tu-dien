<div id="rs-branches" class="rs-branches sec-color pt-100 pb-70">
    <div class="container">
        <div class="abt-title mb-70 text-center">
            <h2>OUR BRANCHES</h2>
            <p>Considering desire as primary motivation for the generatio.</p>
        </div>
        <div class="row">
            @foreach(\App\Helpers\Common::getBranches() as $key => $item)
            <div class="col-lg-3 col-md-6">
                <div class="branches-item">
                    <img src="{!! url('upload/images',$item->img) !!}" alt="{!! $item->name !!}">
                    <h3>
                        <span>0{!! $key + 1 !!}</span>
                        {!! $item->name !!}
                    </h3>
                    {!! $item->content !!}
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>