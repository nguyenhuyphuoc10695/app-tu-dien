<div id="footer-services" class="contained">
    <?php $service = \App\Helpers\Common::getMenuContent(92) ?>
    <h3>Dịch vụ của chúng tôi:</h3>
        @if($service)
            <?php $service_item = \App\Helpers\Common::getMenuList($service->id) ?>
                <ul>
                    @foreach($service_item as $key => $item)
                        <li>
                            @if($key == 0)
                                <a href="{!! url('page', $service->slug) !!}.html#engineering">
                            @elseif($key == 1)
                                <a href="{!! url('page', $service->slug) !!}.html#project-management">
                            @elseif($key == 2)
                                <a href="{!! url('page', $service->slug) !!}.html#maintenance">
                            @endif

                                <h5>{!! $item->name !!}</h5>
                                @if($key == 0)
                                    <div id="svg-me" class="svg-wrapper">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;" xml:space="preserve">
                                        <path id="me1" class="st0" d="M-285.8,339.6v-4.8l-5.6-1.1c-0.5-1.5-1.1-2.9-1.9-4.1l3.4-4.6l-3.4-3.5l-4.8,3.3c-1.3-0.8-2.8-1.5-4.2-1.8 l-1-5.7h-4.8l-1.1,5.7c-1.6,0.4-2.9,1-4.2,1.7l-4.7-3.4l-3.4,3.4l3.3,4.7c-0.8,1.3-1.5,2.8-1.8,4.4l-5.6,0.8v4.8l5.6,1.1 c0.4,1.6,1,3.1,1.8,4.5l-3.3,4.5l3.4,3.4l4.6-3.1c1.3,0.8,2.9,1.6,4.5,1.9l0.8,5.4h4.8l1.1-5.4c1.6-0.4,3.1-1,4.5-1.9l4.5,3.3 l3.4-3.4l-3.1-4.6c0.8-1.3,1.5-2.9,1.8-4.5L-285.8,339.6z M-305.8,346.1c-4.8,0-8.7-4-8.7-8.7c0-4.8,4-8.7,8.7-8.7 c4.7,0,8.7,4,8.7,8.7C-297,342.1-301,346.1-305.8,346.1z"/>
                                            <path id="me2" class="st0" d="M-245.1,315.5l0.1-5.5l-7.2-1.5c-0.3-1.4-0.8-2.8-1.6-4.1l4.3-5.9l-3.8-3.9l-6,4c-1.3-0.8-2.6-1.4-4.1-1.8 l-1.1-7.1l-5.5-0.1l-1.4,7c-1.5,0.3-3,1-4.3,1.7l-5.7-4.2l-3.9,3.8l3.9,5.8c-0.7,1.4-1.3,2.9-1.8,4.5l-6.9,1l-0.1,5.5l6.9,1.4 c0.4,1.5,1,3.1,1.8,4.4l-4.2,5.6l3.8,3.9l5.9-3.9c1.3,0.8,2.7,1.4,4.2,1.8l1,7l5.5,0.1l1.5-7.1c1.4-0.4,2.9-1,4.2-1.7l5.9,4.3 l3.9-3.8l-4.1-6.2c0.8-1.3,1.3-2.6,1.7-4L-245.1,315.5z M-267.8,323.3c-5.9-0.1-10.7-4.9-10.6-10.8c0.1-5.9,4.9-10.7,10.8-10.6 c5.9,0.1,10.7,4.9,10.6,10.8S-261.9,323.3-267.8,323.3z"/>
                                            <path id="me3" class="st0" d="M-284.8,290.8v-7.2l-9.1-1.7c-0.7-1.9-1.5-4-2.6-5.8l5.4-7.5l-5.1-5.1l-7.7,5.2c-1.8-1-3.8-1.9-5.8-2.4 l-1.4-9.2l-7.2,0l-1.8,9.2c-1.9,0.5-3.9,1.2-5.6,2.4l-7.6-5.5l-5.1,5.1l5.3,8c-0.9,1.7-1.7,3.6-2.3,5.5l-9.5,1.5v7.2l9.6,1.8 c0.5,1.9,1.2,3.7,2.3,5.3l-5.6,7.9l5.1,5.1l8.1-5.4c1.6,1,3.5,1.8,5.3,2.3l1.5,9.5l7.2,0l1.8-9.4c1.9-0.5,3.8-1.1,5.5-2.3l7.8,5.5 l5.1-5.1l-5.3-7.8c1.1-1.7,1.8-3.7,2.4-5.6L-284.8,290.8z M-304.9,296.9c-5.5,5.5-14.5,5.5-20.1,0s-5.5-14.5,0-20.1 s14.5-5.5,20.1,0S-299.3,291.4-304.9,296.9z"/>
                                        </svg>
                                    </div>
                                @elseif($key == 1)
                                    <div id="svg-pm" class="svg-wrapper">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;" xml:space="preserve">
                                            <g id="object_1pm">
                                                <rect class="box-pm1 all-box-pm" x="-311" y="268" class="st0" width="66" height="6"/>
                                                <rect class="box-pm-in"  x="-345" y="257" class="st0" width="28" height="28"/>
                                            </g>
                                            <g class="box-pm3 all-box-pm" id="object_2pm">
                                                <path class="st0" d="M-345,329v28h28v-28H-345z M-323,351h-16v-16h16V351z"/>
                                                <rect x="-311" y="340" class="st0" width="66" height="6"/>
                                            </g>
                                            <g class="box-pm2 all-box-pm" id="object_3pm">
                                                <path class="st0" d="M-345,293v28h28v-28H-345z M-323,315h-16v-16h16V315z"/>
                                                <rect x="-311" y="304" class="st0" width="66" height="6"/>
                                            </g>
                                            <g id="object_4pm" class="box-pm1 all-box-pm">
                                                <path class="box-pm1 all-box-pm" class="st0" d="M-345,285h28v-28h-28V285z M-339,263h16v16h-16V263z"/>
                                                <rect x="-311" y="268" class="st0" width="66" height="6"/>
                                            </g>
                                        </svg>
                                    </div>
                                @elseif($key == 2)
                                    <div id="svg-maintenance" class="svg-wrapper">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;" xml:space="preserve">
                                            <rect id="hour" x="-298" y="277" width="6" height="30"/>
                                            <rect id="minute" x="-298" y="271" width="6" height="36"/>
                                            <g id="clock-face">
                                                <path d="M-295,257c-27.6,0-50,22.4-50,50s22.4,50,50,50s50-22.4,50-50S-267.4,257-295,257z M-262.5,336.7l-3.5-3.5 l-2.8,2.8l3.5,3.5c-7.4,6.7-17,11-27.7,11.5V346h-4v4.9c-10.6-0.5-20.3-4.7-27.7-11.5l3.5-3.5l-2.8-2.8l-3.5,3.5 c-6.7-7.4-11-17-11.5-27.7h4.9v-4h-4.9c0.5-10.6,4.7-20.3,11.5-27.7l3.5,3.5l2.8-2.8l-3.5-3.5c7.4-6.7,17-11,27.7-11.5v4.9h4v-4.9 c10.6,0.5,20.3,4.7,27.7,11.5l-3.5,3.5l2.8,2.8l3.5-3.5c6.7,7.4,11,17,11.5,27.7h-4.9v4h4.9C-251.5,319.6-255.8,329.3-262.5,336.7 z"/>
                                                <circle class="st0" cx="-295" cy="307" r="7.1"/>
                                            </g>
                                        </svg>
                                    </div>
                                @endif
                            </a>
                        </li>
                    @endforeach
                </ul>
            <a href="{!! url('page', $service->slug) !!}.html" class="blue-btn mobile-show">Xem tất cả</a>
        @endif
</div>