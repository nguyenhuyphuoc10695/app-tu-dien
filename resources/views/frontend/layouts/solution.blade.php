<div id="footer-services" class="contained">
    <h3>Giải pháp của chúng tôi:</h3>
    <ul>
        @foreach(\App\Helpers\Common::getAllSolutions() as $key => $item)
            @if($key == 0)
                <li class="col-sm-2"><a href="{!! url('giai-phap', $item->slug) !!}.html">
                        <h5>{!! $item->name !!}</h5>
                        <div id="svg-puzzle" class="svg-wrapper">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                 y="0px" viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;"
                                 xml:space="preserve">
                                <path id="puzzle-tl" d="M-345,257v47h47v-47H-345z M-304,298h-35v-35h35V298z"/>
                                <path id="puzzle-tr" d="M-292,257v47h47v-47H-292z M-251,298h-35v-35h35V298z"/>
                                <path id="puzzle-br" d="M-292,310v47h47v-47H-292z M-251,351h-35v-35h35V351z"/>
                                <path id="puzzle-bl" d="M-345,310v47h47v-47H-345z M-304,351h-35v-35h35V351z"/>
                            </svg>
                        </div>
                    </a>
                </li>
            @elseif($key == 1)
                <li class="col-sm-2"><a href="{!! url('giai-phap', $item->slug) !!}.html">
                        <h5>{!! $item->name !!}</h5>
                        <div id="svg-tower" class="svg-wrapper">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                 y="0px" viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;"
                                 xml:space="preserve">
                        <path id="tower-1" class="st0" d="M-345,304v53h100v-53H-345z M-251,351h-88v-41h88V351z"/>
                                <polygon id="tower-2" class="st0" points="-345,350.4 -345,357 -245,310.4 -245,310.2 -245,303.8"/>
                                <path id="tower-3" class="st0" d="M-345,257v53h100v-53H-345z M-251,304h-88v-41h88V304z"/>
                                <polygon id="tower-4" class="st0" points="-345,257 -345,263.6 -245,309.8 -245,303.8 -245,303.6"/>
                                <polygon id="tower-5" class="st0" points="-245,257 -245,263.6 -345,309.8 -345,303.8 -345,303.6"/>
                                <polygon id="tower-7" class="st0" points="-345,304.2 -345,310.9 -245,357 -245,351 -245,350.9"/>
                    </svg>
                        </div>
                    </a>
                </li>
            @elseif($key == 2)
                <li class="col-sm-2"><a href="{!! url('giai-phap', $item->slug) !!}.html">
                        <h5>{!! $item->name !!}</h5>
                        <div id="svg-aisle" class="svg-wrapper">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                 y="0px" viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;"
                                 xml:space="preserve">
                                <path id="aisle-t"
                                    d="M-279,311h-59.3h-6.7v6v34v6h6.7h59.3h6v-6v-34v-6H-279z M-279,351h-59.3v-34h59.3V351z"/>
                                <path id="aisle-m"
                                      d="M-251,284.9h-59.3h-6.7v6v34v6h6.7h59.3h6v-6v-34v-6H-251z M-251,324.9h-59.3v-34h59.3V324.9z"/>
                                <path id="aisle-b"
                                      d="M-279,257h-59.3h-6.7v6v34v6h6.7h59.3h6v-6v-34v-6H-279z M-279,297h-59.3v-34h59.3V297z"/>
                            </svg>
                        </div>
                    </a>
                </li>
            @else
                <li class="col-sm-2"><a href="{!! url('giai-phap', $item->slug) !!}.html">
                        <h5>{!! $item->name !!}</h5>
                        <div id="svg-stacker" class="svg-wrapper">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                 y="0px" viewBox="-345 257 100 100" style="enable-background:new -345 257 100 100;"
                                 xml:space="preserve">
                        <path id="object_3s" class="st0" d="M-345,329v28h100v-28H-345z M-251,351h-88v-16h88V351z"/>
                                <path id="object_2s" class="st0" d="M-345,293v28h100v-28H-345z M-251,315h-88v-16h88V315z"/>
                                <path id="object_1s" class="st0" d="M-345,257v28h100v-28H-345z M-251,279h-88v-16h88V279z"/>
                    </svg>
                        </div>
                    </a>
                </li>
            @endif
        @endforeach
    </ul>
    <a href="{!! url('giai-phap') !!}.html" class="blue-btn mobile-show">Giải pháp</a>
</div>