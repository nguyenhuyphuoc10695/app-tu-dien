<nav id="news-r">
    <div id="categories-2" class="widget widget_categories"><h3>Danh mục</h3>
        <ul>
            @foreach(\App\Helpers\Common::getCategories() as $item)
                <li class="cat-item cat-item-3"><a href="{!! url('tin-tuc', $item['slug']) !!}.html">{!! $item['name'] !!}</a></li>
            @endforeach
        </ul>
    </div>
    <div id="archives-2" class="widget widget_archive"><h3>Archives</h3>
        <ul>
            @foreach(\App\Helpers\Common::getArchiveList() as $item)
                <li><a href='{!! url()->current() !!}?month={!! $item->month !!}&year={!! $item->year !!}'>{!! $item->name !!}</a></li>
            @endforeach
        </ul>
    </div>
</nav>