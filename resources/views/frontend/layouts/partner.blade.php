<div id="rs-partner" class="rs-partner sec-color pt-70 pb-170">
    <div class="container">
        <div class="rs-carousel owl-carousel" data-loop="true" data-items="5" data-margin="80" data-autoplay="true" data-autoplay-timeout="5000" data-smart-speed="2000" data-dots="false" data-nav="false" data-nav-speed="false" data-mobile-device="2" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="4" data-ipad-device-nav="false" data-ipad-device-dots="false" data-md-device="5" data-md-device-nav="false" data-md-device-dots="false">
            @foreach(\App\Helpers\Common::getPartners() as $item)
                <div class="partner-item">
                    <a href="{!! $item->link !!}"><img src="{!! url('upload/images',$item->img) !!}" alt="{!! $item->name !!}"></a>
                </div>
            @endforeach
        </div>
        <h2 class="text-center" style="padding-top: 50px;">ĐĐỐI TÁC LIÊN KẾT VỚI BIG LANGUAGE</h2>
    </div>
</div>