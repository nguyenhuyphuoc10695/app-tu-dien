<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 6/4/2019
 * Time: 11:48 AM
 */
return [
    0 => [
        "id" => 1,
        "name" => "Câu nói, câu kể",
        "ge_name" => "Sprüche",
    ],
    1 => [
        "id" => 2,
        "name" => "Câu hỏi có từ để hỏi",
        "level" => "Fragen mit Worten zum Fragen",
    ],
    2 => [
        "id" => 3,
        "name" => "Câu hỏi có không",
        "level" => "ja, keine Fragen",
    ],
];
