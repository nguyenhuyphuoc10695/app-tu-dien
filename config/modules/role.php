<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 6/4/2019
 * Time: 11:48 AM
 */
return [
    0 => [
        "id" => 1,
        "name" => "Quản trị viên cấp cao",
        "level" => 1,
    ],
    1 => [
        "id" => 2,
        "name" => "Quản trị viên (Admin)",
        "level" => 2,
    ],
    2 => [
        "id" => 3,
        "name" => "Học viên",
        "level" => 3,
    ],
    3 => [
        "id" => 4,
        "name" => "Khách",
        "level" => 4,
    ],
];
