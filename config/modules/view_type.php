<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 6/4/2019
 * Time: 11:48 AM
 */
return [
    0 => [
        "id" => 1,
        "name" => "Tiếp cận",
    ],
    1 => [
        "id" => 2,
        "name" => "Giải pháp",
    ],
    2 => [
        "id" => 3,
        "name" => "Dịch vụ",
    ],
    3 => [
        "id" => 4,
        "name" => "Giới thiệu",
    ],
    4 => [
        "id" => 5,
        "name" => "Tin tức",
    ],
    5 => [
        "id" => 6,
        "name" => "Dự án",
    ],
    6 => [
        "id" => 7,
        "name" => "Testimonials",
    ],
    7 => [
        "id" => 8,
        "name" => "Liên hệ",
    ],
];