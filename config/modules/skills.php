<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 6/4/2019
 * Time: 11:48 AM
 */
return [
    0 => [
        "id" => 1,
        "name" => "Đọc",
        "ge_name" => "Lesen",
    ],
    1 => [
        "id" => 2,
        "name" => "Viết",
        "level" => "Schreiben",
    ],
    2 => [
        "id" => 3,
        "name" => "Nghe",
        "level" => "Hören",
    ],
    3 => [
        "id" => 4,
        "name" => "Nói",
        "level" => "Sprechen",
    ],
];
