<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 7/7/2019
 * Time: 4:25 PM
 */

return [
    0 => '#e8357c',
    1 => '#d8a0c2',
    2 => '#81d742',
    3 => '#dd5a5a',
    4 => '#a38a6d',
    5 => '#e5e220',
    6 => '#489e31',
    7 => '#6ac4c2',
    8 => '#b17fe2',
    9 => '#a4c4be',
];