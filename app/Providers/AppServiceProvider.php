<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        foreach (glob(app_path() . '/Helpers/*.php') as $file) {
//            require_once($file);
//        }
        $this->mergeConfigFrom(__DIR__ . '/../../config/modules/role.php', 'role');
        $this->mergeConfigFrom(__DIR__ . '/../../config/modules/view_type.php', 'typeView');
//        $this->mergeConfigFrom(__DIR__ . '/../../config/modules/language.php', 'language');
        $this->mergeConfigFrom(__DIR__ . '/../../config/modules/lang.php', 'lang');
        $this->mergeConfigFrom(__DIR__ . '/../../config/modules/color.php', 'color');
        $this->mergeConfigFrom(__DIR__ . '/../../config/modules/level.php', 'level');
        $this->mergeConfigFrom(__DIR__ . '/../../config/modules/skills.php', 'skills');
        $this->mergeConfigFrom(__DIR__ . '/../../config/modules/sentence.php', 'sentence');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
