<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected  $table = 'categories';

    protected  $fillable = ['name', 'slug', 'meta_title', 'meta_description', 'meta_keywords', 'img', 'description', 'content', 'ord', 'status', 'menu'];
}
