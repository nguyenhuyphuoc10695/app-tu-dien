<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected  $table = 'posts';

    protected  $fillable = ['name', 'slug', 'meta_title', 'meta_description', 'meta_keywords', 'category_id', 'img', 'link', 'description', 'content', 'ord', 'status', 'hotnews', 'care', 'price', 'specification', 'image'];
}
