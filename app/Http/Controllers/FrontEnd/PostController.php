<?php

namespace App\Http\Controllers\FrontEnd;

use App\Helpers\Common;
use App\Http\Services\PostService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    public $perPage = 30;
    protected $postService;
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function getDetailNews($slug) {
        $alias = str_replace('.html','',$slug);
        $post = $this->postService->getPostBySlug($alias);
        $postNext = $this->postService->getPostNext($post['id']);
        return view('frontend.posts.detail_news', compact('post', 'postNext'));
    }

    public function getRelatedPost(Request $request) {
        $posts = $this->postService->getPostRealated($request->all());
//        dd($posts);
        $view = view('frontend.posts.related',compact('posts'))->render();
        return response()->json(['html'=>$view]);
    }

    public function getCategoryNews($slug) {
        $alias = str_replace('.html','',$slug);
        $category = $this->postService->getCategory($alias);
        if (isset($_GET['month'])) {
            $month = $_GET['month'];
        } else {
            $month = null;
        }

        if (isset($_GET['year'])) {
            $year = $_GET['year'];
        } else {
            $year = null;
        }
        $news = $this->postService->getNewsByCategory($category['id'],$month, $year);
        return view('frontend.pages.category', compact('news', 'category'));
    }

    public function getSearchNews() {
        $keyword = $_GET['keyword'];
        $postSearch = $this->postService->getPostByKeyword($this->perPage, $keyword);
        return view('frontend.posts.search', compact('postSearch', 'keyword'));
    }
}
