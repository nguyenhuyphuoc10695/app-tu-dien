<?php

namespace App\Http\Controllers\FrontEnd;

use App\Helpers\Common;
use App\Http\Requests\ContactRequest;
use App\Http\Services\MenuService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public $perPage = 5;
    protected $menuService;
    public function __construct(MenuService $menuService)
    {
        $this->menuService = $menuService;
        //view()->share('menuService',$this->menuService);
    }

    public function getPage($slug) {
//        dd(Common::getMenuByID([18,20]));
        $alias = str_replace('.html','',$slug);
        $pageContent = $this->menuService->getMenuByAlias($alias);
        switch($pageContent->type) {
            case 1:
                $approaches = Common::getMenuList($pageContent->id);
                return view('frontend.pages.approach', compact('pageContent', 'approaches'));
                break;
            case 2:
                $solutions = $this->menuService->getSolutionList();
                return view('frontend.pages.solution', compact('pageContent', 'solutions'));
                break;
            case 3:
                $services = Common::getMenuList($pageContent->id);
                return view('frontend.pages.service', compact('pageContent', 'services'));
                break;
            case 4:
                $abouts = Common::getMenuList($pageContent->id);
                $about_list = $this->menuService->getAboutContent();
                return view('frontend.pages.about', compact('pageContent', 'abouts', 'about_list'));
                break;
            case 5:
                if (isset($_GET['month'])) {
                    $month = $_GET['month'];
                } else {
                    $month = null;
                }

                if (isset($_GET['year'])) {
                    $year = $_GET['year'];
                } else {
                    $year = null;
                }
                $news = $this->menuService->getNews($month, $year);
                return view('frontend.pages.blog', compact('pageContent', 'news'));
                break;
            case 6:
                $images = [];
                $image_with_link = [];
                $projects = $this->menuService->getProjects();
                foreach ($projects as $key => $item) {
                    foreach (json_decode($item->img) as $img) {
                        $image_with_link[] = url('upload/images', $img);
                    }
                    $images[$key] = $image_with_link;
                }
                return view('frontend.pages.project', compact('pageContent','projects', 'images'));
                break;
            case 7:
                $testimonials = $this->menuService->getAlbumList();
                return view('frontend.pages.testimonial', compact('pageContent', 'testimonials'));
                break;
            case 8:
                return view('frontend.pages.contact', compact('pageContent'));
                break;
            default:
                return view('frontend.pages.page', compact('pageContent'));
        }
    }

    public function getCareerDetail($slug) {
        $alias = str_replace('.html','',$slug);
        $career = $this->menuService->getCareerByAlias($alias);
        return view('frontend.pages.detail_career', compact('career'));
    }

    public function saveContact(ContactRequest $request) {
        $saveContact = $this->menuService->saveContact($request->all());
        if ($saveContact) {
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Gửi liên hệ thành công']);
        } else {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Gửi liên hệ thất bại']);
        }
    }

    public function saveNewsLetter(Request $request) {
        $this->menuService->saveNewsLetter($request->all());
        return redirect()->route('home');
    }

    public function getDetailEvent($slug) {
        $alias = str_replace('.html','',$slug);
        $event = $this->menuService->getEventBySlug($alias);
        return view('frontend.pages.detail_event', compact('event'));
    }

    public function getCommentByPost(Request $request) {
        $comments = [];
        $parent_comments = $this->menuService->getCommentInPost($request->id, 0);
        foreach($parent_comments as $key => $item) {
            $comments[$key] = $item;
            $children_comments = $this->menuService->getCommentInPost($request->id,$item['id']);
            foreach($children_comments as $k => $value) {
                $comments[$key]['children'][$k] = $value;
            }
        }
        $view = view('frontend.pages.comment',compact('comments'))->render();
        return response()->json(['html'=>$view]);
    }

    public function saveCommentByPost(Request $request) {
        $saveComment = $this->menuService->saveCommentInPost($request->all());
        if ($saveComment) {
            return response()->json(['success'=>'Thêm comment thành công'], 200);
        } else {
            return response()->json(['error'=>'Thêm comment thất bại'], 500);
        }
    }

    public function getCategorySolution($slug) {
        $alias = str_replace('.html','',$slug);
        $solution = $this->menuService->getSolutionBySlug($alias);
//        dd($solution);
        $projects = $this->menuService->getProjectBySolution($solution->id);
        $images = [];
        $image_with_link = [];
        foreach ($projects as $key => $item) {
            foreach (json_decode($item->img) as $img) {
                $image_with_link[] = url('upload/images', $img);
            }
            $images[$key] = $image_with_link;
        }
        return view('frontend.pages.detail_solution', compact('solution', 'projects', 'images'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
