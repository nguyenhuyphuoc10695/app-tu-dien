<?php

namespace App\Http\Controllers\FrontEnd;

use App\Helpers\Common;
use App\Http\Services\MainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public $perPage = 5;
    protected $mainService;
    public function __construct(MainService $mainService)
    {
        $this->mainService = $mainService;
        //view()->share('menuService',$this->menuService);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $approach = $this->mainService->getOneMenu(90);
        $solution = $this->mainService->getOneMenu(91);
        $service = $this->mainService->getOneMenu(92);
        $contact = $this->mainService->getOneMenu(97);
        return view('frontend.home.index', compact('approach', 'solution', 'service','contact'));
    }

    public function getCarePosts(Request $request) {
        $posts = $this->mainService->getPostCare($request->all());
        $category = $request->category_id;
        $view = view('frontend.home.filter',compact('posts', 'category'))->render();
        return response()->json(['html'=>$view]);
    }

    public function getCategoryPosts(Request $request) {
        if (Common::getCategories()[0])
            $category_id = Common::getCategories()[0]['id'];
        $posts = $this->mainService->getPostOfOneCategory($category_id);
        $view = view('frontend.home.category',compact('posts'))->render();
        return response()->json(['html'=>$view]);
    }


    public function changeLanguage($slug) {
        if (Session::has('lang')) {
            if ($slug === 'en') {
                Session::put('lang', 2);
            } else {
                Session::put('lang', 1);
            }
        } else {
            Session::put('lang', 1);
        }
//        dd(Session::get('lang'));
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
