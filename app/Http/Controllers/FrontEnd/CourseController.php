<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Services\CourseService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    public $perPage = 9;
    protected $courseService;
    public function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    public function getDetailCourse($slug) {
        $alias = str_replace('.html','',$slug);
        $course = $this->courseService->getCourseBySlug($alias);
        $courseRelated = $this->courseService->getCourseRealated($course['id'], $course['category_id']);
        $reviews = $this->courseService->getReviewOfCourse($course['id']);
        $staff = $this->courseService->getStaffOfCourse($course['teacher_id']);
        return view('frontend.pages.detail_course', compact('course', 'courseRelated', 'reviews', 'staff'));
    }

    public function getCategoryCourse($slug) {
        $alias = str_replace('.html','',$slug);
        $category = $this->courseService->getCourseCategory($alias);
//        dd($category);
        $courses = $this->courseService->getCourseByCategory($category['id'], $this->perPage);
        return view('frontend.pages.category', compact('courses', 'category'));
    }

    public function getSearchCourse() {
        $keyword = $_GET['keyword'];
        $courses = $this->courseService->getCourseByKeyword($this->perPage, $keyword);
        return view('frontend.pages.search', compact('courses', 'keyword'));
    }

    public function getCourseAjax(Request $request) {
        $courses = $this->courseService->getListOfCourse($request->all());
        if ($request->cate)
            $category = $request->cate;
        else
            $category = "null";
//        dd($category);
        $view = view('frontend.pages.course_ajax',compact('courses', 'category'))->render();
        return response()->json(['html'=>$view]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
