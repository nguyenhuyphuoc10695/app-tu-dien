<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostRequest;
use App\Http\Services\PostService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
//    public $perPage = 20;
    public $perPage = 20;
    protected $postService;
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
        //view()->share('menuService',$this->menuService);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isset($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $posts = $this->postService->getPosts($this->perPage, $keyword);
        } else {
            $posts = $this->postService->getPosts($this->perPage, null);
        }

        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $menus = Common::getMenuData();
        //dd($menus);
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
//        dd($request->image);
        $time = time();
        $savePost= $this->postService->savePost($time, $request->all());
        if ($savePost) {
            foreach ($request->image as $item) {
                $this->postService->uploadImage($time, $item, 150, 200, 'post_detail');
            }
            return redirect()->route('admin.post.index')->with(['flash_level' => 'success', 'flash_message' => 'Thêm bài viết thành công']);
        } else {
            return redirect()->route('admin.post.index')->with(['flash_level' => 'danger', 'flash_message' => 'Thêm bài viết thất bại']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->postService->getPost($id);
//        dd(json_decode($post['image']));
        return view('admin.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
//        dd($request->image_del);
        $time = time();
        $post = $this->postService->getPost($id);
//        dd($request->image_del, array_diff(json_decode($post['image']), $request->image_del));
        $updatePost = $this->postService->updatePost($time,$request->all(),$id);
        if ($updatePost) {
            if ($request->img_name != $post['img']) {
                $this->postService->deleteImage($post['img']);
            }
            if (isset($request->image)) {
                foreach ($request->image as $item) {
                    $this->postService->uploadImage($time, $item, 150, 200, 'post_detail');
                }
            }
            if (isset($request->image_del)) {
                foreach($request->image_del as $item) {
                    $this->postService->deleteImage($item);
                }
            }
            return redirect()->route('admin.post.index')->with(['flash_level' => 'success', 'flash_message' => 'Chỉnh sửa bài viết thành công']);
        } else {
            return redirect()->route('admin.post.index')->with(['flash_level' => 'danger', 'flash_message' => 'Chỉnh sửa bài viết thất bại']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->postService->getPost($id);
        $deletePost = $this->postService->deletePost($id);
        if ($deletePost) {
            $this->postService->deleteImage($post['img']);
            foreach(json_decode($post['image']) as $item) {
                $this->postService->deleteImage($item);
            }
            return redirect()->route('admin.post.index')->with(['flash_level' => 'success', 'flash_message' => 'Xóa bài viết thành công']);
        } else {
            return redirect()->route('admin.post.index')->with(['flash_level' => 'danger', 'flash_message' => 'Xóa bài viết thất bại']);
        }
    }

    public function delete(Request $request)
    {
        //dd($request->all());
        $data_id = explode(',', $request->data_checkboxes);
        $posts = $this->postService->getMultiplePost($data_id);
        $deletePosts = $this->postService->deleteMultiplePost($data_id);
        if ($deletePosts) {
            foreach($posts as $item) {
                $this->postService->deleteImage($item['img']);
                foreach(json_decode($item['image']) as $img) {
                    $this->postService->deleteImage($img);
                }
            }
            return redirect()->route('admin.post.index')->with(['flash_level' => 'success', 'flash_message' => 'Xóa bài viết thành công']);
        } else {
            return redirect()->route('admin.post.index')->with(['flash_level' => 'danger', 'flash_message' => 'Xóa bài viết thất bại']);
        }
    }

    public function uploadImage(Request $request) {
//        dd($request->all());
        $time = time();
        return $this->postService->uploadImage($time, $request->img, 150, 200, 'post');
    }
}
