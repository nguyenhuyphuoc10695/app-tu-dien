<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UserRequest;
use App\Http\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public $perPage = 10;
    protected $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
        //view()->share('menuService',$this->menuService);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isset($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $users = $this->userService->getUsers($this->perPage, $keyword);
        } else {
            $users = $this->userService->getUsers($this->perPage, null);
        }
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $saveUser = $this->userService->saveUser($request->all());
        if ($saveUser) {
            return redirect()->route('admin.user.index')->with(['flash_level' => 'success', 'flash_message' => 'Thêm mới tài khoản thành công']);
        } else {
            return redirect()->route('admin.user.index')->with(['flash_level' => 'danger', 'flash_message' => 'Thêm mới tài khoản thất bại']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->userService->getUser($id);
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        if (!is_null($request->name)) {
            $updateUser = $this->userService->updateUser($request->all(),$id);
            if ($updateUser) {
                return redirect()->route('admin.user.index')->with(['flash_level' => 'success', 'flash_message' => 'Chỉnh sửa tài khoản thành công']);
            } else {
                return redirect()->route('admin.user.index')->with(['flash_level' => 'danger', 'flash_message' => 'Chỉnh sửa tài khoản thất bại']);
            }
        } else {
            return redirect()->route('admin.user.create')->with(['flash_level' => 'danger', 'flash_message' => 'Vui lòng nhập tên']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteUser = $this->userService->deleteUser($id);
        if ($deleteUser) {
            return redirect()->route('admin.user.index')->with(['flash_level' => 'success', 'flash_message' => 'Xóa tài khoản thành công']);
        } else {
            return redirect()->route('admin.user.index')->with(['flash_level' => 'danger', 'flash_message' => 'Xóa tài khoản thất bại']);
        }
    }

    public function getChangePassword() {
        $user = Auth::user();
        return view('admin.users.change_password', compact('user'));
    }

    public function updatePassword(ChangePasswordRequest $request)
    {
        $id = Auth::user()->id;
        if (Hash::check($request->newPassword, Auth::user()->getAuthPassword())) {
            $updatePassword = $this->userService->updatePassword($request->all(),$id);
            if ($updatePassword) {
                Auth::logout();
                return redirect()->route('login')->with(['flash_level' => 'success', 'flash_message' => 'Chỉnh sửa tài khoản thành công vui lòng đăng nhập lại']);
            } else {
                return redirect()->route('admin.user.change_password')->with(['flash_level' => 'danger', 'flash_message' => 'Chỉnh sửa tài khoản thất bại']);
            }
        } else {
            return redirect()->route('admin.user.change_password')->with(['flash_level' => 'danger', 'flash_message' => 'Mật khẩu cũ không chính xác']);
        }
    }
}
