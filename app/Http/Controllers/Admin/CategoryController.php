<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use App\Http\Services\CategoryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    protected $categoryService;
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
        //view()->share('menuService',$this->menuService);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->categoryService->getCategories();
        return view('admin.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        //dd($this->categoryService->getLastOrder('categories'));
        $time = time();
        $saveCategory = $this->categoryService->saveCategory($time,$request->all());
        if ($saveCategory) {
            $this->categoryService->uploadImage($time, $request->img, 150, 100, 'category');
            return redirect()->route('admin.category.index')->with(['flash_level' => 'success', 'flash_message' => 'Thêm danh mục thành công']);
        } else {
            return redirect()->route('admin.category.index')->with(['flash_level' => 'danger', 'flash_message' => 'Thêm danh mục thất bại']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->categoryService->getCategory($id);
        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        //dd($request->all());
        $category = $this->categoryService->getCategory($id);
        $time = time();
        $updateCategory = $this->categoryService->updateCategory($time,$request->all(),$id);
        if ($updateCategory) {
            if (isset($request->img)) {
                $this->categoryService->uploadImage($time, $request->img, 150, 100, 'category');
                $this->categoryService->deleteImage($category['img']);
            }

            return redirect()->route('admin.category.index')->with(['flash_level' => 'success', 'flash_message' => 'Chỉnh sửa danh mục bài viết thành công']);
        } else {
            return redirect()->route('admin.category.index')->with(['flash_level' => 'danger', 'flash_message' => 'Chỉnh sửa danh mục bài viết thất bại']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->categoryService->getCategory($id);
        $deleteCategory = $this->categoryService->deleteCategory($id);
        if ($deleteCategory) {
            $this->categoryService->deleteImage($category['img']);
            $this->categoryService->deleteCourseHasCategory($category['id']);
            return redirect()->route('admin.category.index')->with(['flash_level' => 'success', 'flash_message' => 'Xóa danh mục bài viết thành công']);
        } else {
            return redirect()->route('admin.category.index')->with(['flash_level' => 'danger', 'flash_message' => 'Xóa danh mục bài viết thất bại']);
        }
    }
}
