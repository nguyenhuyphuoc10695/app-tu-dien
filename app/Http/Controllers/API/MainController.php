<?php

namespace App\Http\Controllers\API;

use App\Career;
use App\Helpers\Common;
use App\Http\Services\MainService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public $perPage = 5;
    protected $mainService;
    public function __construct(MainService $mainService)
    {
        $this->mainService = $mainService;
        //view()->share('menuService',$this->menuService);
    }

    public function updateStatus(Request $request) {
        //dd($request->all());
        $updateStatus = $this->mainService->updateStatus($request->all());
        if ($updateStatus)
            return response()->json(['success' => 'Cập nhât trạng thái thành công'], 200);
        else
            return response()->json(['error' => 'Cập nhât trạng thái thất bại'], 500);
    }

    public function updateOrder(Request $request) {
        //dd($request->all());
        $updateOrder = $this->mainService->updateOrder($request->all());
        if ($updateOrder)
            return response()->json(['success' => 'Cập nhât thứ tự thành công'], 200);
        else
            return response()->json(['error' => 'Cập nhât thứ tự thất bại'], 500);
    }

    public function saveOwner(Request $request) {
//        dd($request->all());
        $saveOwner = $this->mainService->saveOwner($request->all());
        if ($saveOwner)
            return response()->json(['success' => 'Gửi liên hệ dành cho chủ sỏ hữu đất thành công'], 200);
        else
            return response()->json(['error' => 'Gửi liên hệ dành cho chủ sỏ hữu đất thất bại'], 500);
    }

    public function saveInvestor(Request $request) {
//        dd($request->all());
        $saveOwner = $this->mainService->saveInvestor($request->all());
        if ($saveOwner)
            return response()->json(['success' => 'Gửi liên hệ dành cho nhà đầu tư thành công'], 200);
        else
            return response()->json(['error' => 'Gửi liên hệ dành cho nhà đầu tư thất bại'], 500);
    }

    public function getListCareers(Request $request) {
//        dd($request->all());
        $builder = Career::query();
        $builder->where('status','=',1);
        $builder->where('lang','=', Common::getLang());

        if (isset($request->locations)) {
            $locations = explode(',',$request->locations);
            foreach($locations as $item) {
                $builder->where('location', 'LIKE', "%$item%");
            }
        }

        if (isset($request->departments)) {
            $departments = explode(',',$request->departments);
            foreach($departments as $item) {
                $builder->where('department', 'LIKE', "%$item%");
            }
        }
        $builder->orderBy('id','DESC');
        $careers = $builder->paginate((int)$request->perPage);
//        dd($careers);
//        $careers = Career::paginate(4);
        return response()->json($careers);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
