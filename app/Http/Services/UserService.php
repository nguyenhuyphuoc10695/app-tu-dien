<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 6/1/2019
 * Time: 3:39 PM
 */

namespace App\Http\Services;


use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserService extends MainService
{
    public function getUsers($perPage, $keyword) {
        if ($keyword)
            return User::where('level','>',Auth::user()->level)->where('name','like','%'.$keyword.'%')->orderBy('id','DESC')->paginate($perPage);
        else
            return User::where('level','>',Auth::user()->level)->orderBy('id','DESC')->paginate($perPage);
    }

    public function getUser($id) {
        return User::find($id);
    }

    public function saveUser($data) {

        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->level = $data['level'];
        $user->course = json_encode($data['courses']);
        if (isset($data['status']))
            $user->status = $data['status'];
        else
            $user->status = 0;
        $user->remember_token = $data['_token'];
        $user->save();
        return $user->id;
    }

    public function updateUser($data,$id) {

        $user = User::find($id);
        $user->name = $data['name'];
        $user->level = $data['level'];
        $user->course = json_encode($data['courses']);
        if (isset($data['status']))
            $user->status = $data['status'];
        else
            $user->status = 0;
        $user->save();
        return $user->id;
    }

    public function deleteUser($id) {
        $user = $this->getUser($id);
        return $user->delete();
    }

    public function updatePassword($data, $id) {
        $user = $this->getUser($id);
        $user->name = $data['name'];
        $user->password = Hash::make($data['newPassword']);
        return $user->save();
    }
}
