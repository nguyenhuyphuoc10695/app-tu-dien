<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 6/1/2019
 * Time: 3:38 PM
 */

namespace App\Http\Services;


use App\About;
use App\Achievement;
use App\Career;
use App\Category;
use App\Course;
use App\Event;
use App\InvestorContact;
use App\Menu;
use App\OwnerContact;
use App\Post;
use App\Review;
use App\Staff;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class MainService
{
    public function getCareerList($page, $lang) {
        return Career::select('name','id','slug','location','department')->where('lang','=',$lang)->paginate($page);
    }

    public function getLatestPosts($page) {
        return Post::where('status','=',1)->paginate($page);
    }

    public function getOneMenu($id) {
        return Menu::find($id);
    }

    public function getMenuHomeContent($alias1, $alias2) {
        return Menu::whereSlug($alias1)->orWhere('slug', '=', $alias2)->first();
    }

    public function getAchievement() {
        return Achievement::where('status','=',1)->first();
    }

    public function getListOfStaff() {
        return Staff::where('status','=',1)->limit(8)->get();
    }

    public function getUpcomingEvent() {
        return Event::where('status','=',1)->where('hotnews','=',1)->limit(6)->get();
    }

    public function getAboutContent() {
        return About::where('status','=',1)->get();
    }

    public function getBlogs() {
        return Post::where('status','=',1)->where('hotnews','=',1)->limit(4)->get();
    }

    public function getCourseList() {
        return Course::where('status','=',1)->where('hot','=',1)->limit(6)->get();
    }

    public function getReviews() {
        return Review::where('status','=',1)->limit(6)->get();
    }

    public function getPostCare($data) {
        if (isset($data['category_id'])) {
            return Post::whereStatus(1)
                ->where('care','=',1)
                ->where('category_id','=',$data['category_id'])
                ->orderBy('id','DESC')
                ->paginate(5);
        } else {
            return Post::whereStatus(1)
                ->where('care','=',1)
                ->orderBy('id','DESC')
                ->paginate(5);
        }
    }

    public function getPostOfOneCategory($category,$page) {
        return Post::whereStatus(1)
            ->where('hotnews','=',1)
            ->where('category_id','=',$category)
            ->orderBy('id','DESC')
            ->paginate($page);
    }

    public function getNineHotnewsPosts() {
        return Post::where('status','=',1)
            ->where('hotnews','=',1)
            ->orderBy('id', 'DESC')
            ->paginate(9);
    }

    public function uploadImage($time,$image, $width = 90, $height = 100, $str)
    {
        if (!is_null($image)) {
            $thumbnailPath = public_path('upload/images/_thumbs/thumb');
            $originalPath = public_path('upload/images');

            $image_name = $str.'_'.$time.'_'.$image->getClientOriginalName();
            $thumbnailImage = \Intervention\Image\Facades\Image::make($image);
            $image->move($originalPath, $image_name);
            $thumbnailImage->fit($width, $height);
            $thumbnailImage->save($thumbnailPath.'_'.$image_name);
            return $image_name;
        } else {
            return null;
        }
    }


    public function uploadFile($time,$file)
    {
        if (!is_null($file)) {
            $originalPath = public_path('upload/files');

            $file_name = $time.'_'.$file->getClientOriginalName();
            $file->move($originalPath, $file_name);
            return $file_name;
        } else {
            return null;
        }
    }

    public function deleteImage($image)
    {
        File::delete('upload/images/_thumbs/thumb_' . $image);
        File::delete('upload/images/' . $image);
    }

    public function deleteFile($file)
    {
        File::delete('upload/files/' . $file);
    }

    public function getLastOrder($table) {
        $ord =  DB::table($table)->max('ord');
        if ($ord != null || $ord == 0) {
            return $ord + 1;
        } else {
            return 0;
        }
    }

    public function updateStatus($data) {
        if ($data['status'] == 1)
            $status = 0;
        else
            $status = 1;
        return DB::table($data['table'])->where('id','=', $data['id'])->update(['status' => $status]);
    }

    public function updateOrder($data) {
        return DB::table($data['table'])->where('id','=', $data['id'])->update(['ord' => $data['order']]);
    }

    public function saveOwner($data) {
        $owner = New OwnerContact();
        $owner->name = $data['lastName'].' '.$data['firstName'];
        $owner->phone = $data['phone'];
        $owner->email = $data['email'];
        $owner->address = $data['address'];
        $owner->address_1 = $data['address2'];
        $owner->owner_name = $data['parcelOwnerName'];
        $owner->country = $data['parcelCounty'];
        $owner->tax_code = $data['parcelID_taxID'];
        $owner->acreage = $data['parcelAcreage'];
        $owner->intersection = $data['nearestIntersection'];
        $owner->utility_company = $data['utilityCompanyServing'];
        $owner->status = 0;
        $owner->save();
        return $owner->id;
    }

    public function saveInvestor($data) {
        $investor = New InvestorContact();
        $investor->name = $data['lastName'].' '.$data['firstName'];
        $investor->phone = $data['phone'];
        $investor->email = $data['email'];
        $investor->company_name = $data['companyName'];
        $investor->invest_size = $data['targetInvestmentSize'];
        $investor->investor_type = $data['investorType'];
        $investor->info = $data['additionaInfo'];
        $investor->status = 0;
        $investor->save();
        return $investor->id;
    }

    public function stripUnicode($str)
    {
        if (!$str) return false;
        $unicode = array(
            'a' => 'á|à|ã|ạ|ả|ắ|ằ|ẳ|ặ|ấ|ầ|ậ|ẩ|ẫ|ẵ|ă|â',
            'A' => 'Á|À|Ã|Ạ|Ả|Ắ|Ằ|Ẳ|Ặ|Ấ|Ầ|Ậ|Ẩ|Ẫ|Ẵ|Ă|Â',
            'd' => 'đ',
            'D' => 'Đ',
            'e' => 'è|é|ẹ|ẻ|ẽ|ê|ề|ế|ể|ễ|ệ',
            'E' => 'È|É|Ẻ|Ẽ|Ẹ|Ê|Ề|Ế|Ể|Ễ|Ệ',
            'i' => 'ì|í|ỉ|ĩ|ị',
            'I' => 'Ì|Í|Ỉ|Ĩ|Ị',
            'u' => 'ù|ú|ủ|ũ|ụ|ư|ừ|ứ|ử|ữ|ự',
            'U' => 'Ù|Ú|Ủ|Ũ|Ụ|Ư|Ừ|Ứ|Ử|Ữ|Ự',
            'o' => 'ò|ó|ỏ|õ|ọ|ô|ồ|ố|ổ|ỗ|ộ|ơ|ờ|ớ|ở|ỡ|ợ',
            'O' => 'Ò|Ó|Ỏ|Õ|Ọ|Ô|Ồ|Ố|Ổ|Ỗ|Ộ|Ơ|Ờ|Ớ|Ỡ|Ở|Ợ',
            'y' => 'ỳ|ý|ỷ|ỹ|ỵ',
            'Y' => 'Ỳ|Ý|Ỷ|Ỹ|Ỵ'
        );

        foreach ($unicode as $khongdau => $codau) {
            $arr = explode("|", $codau);
            $str = str_replace($arr, $khongdau, $str);
        }
        return $str;
    }

    public function slugGenerator($str)
    {
        // replace non letter or digits by -
        $str = preg_replace('~[^\pL\d]+~u', '-', $str);

        // transliterate
        $str = $this->stripUnicode($str);

        // remove unwanted characters
        $str = preg_replace('~[^-\w]+~', '', $str);

        // trim
        $str = trim($str, '-');

        // remove duplicate -
        $str = preg_replace('~-+~', '-', $str);

        // lowercase
        $str = strtolower($str);

        if (empty($str)) {
            return 'n-a';
        }

        return $str;
    }

    public function getPostData($perPage, $keyword, $lang) {
        if ($keyword)
            return Post::where('status','=',1)
                ->where('lang','=',$lang)
                ->where('name','like','%'.$keyword.'%')
                ->orderBy('ord','DESC')->paginate($perPage);
        else
            return Post::where('status','=',1)
                ->where('lang','=',$lang)
                ->orderBy('ord','DESC')->paginate($perPage);
    }

}