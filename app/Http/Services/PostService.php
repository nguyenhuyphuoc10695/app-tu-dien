<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 6/1/2019
 * Time: 3:39 PM
 */

namespace App\Http\Services;


use App\Category;
use App\Comment;
use App\Post;
use Illuminate\Support\Facades\DB;

class PostService extends MainService
{
    public function getPosts($perPage, $keyword) {
        if ($keyword)
            return Post::where('name','like','%'.$keyword.'%')->orderBy('id','DESC')->paginate($perPage);
        else
            return Post::orderBy('id','DESC')->paginate($perPage);
    }

    public function getPost($id) {
        return Post::find($id);
    }

    public function getMultiplePost($data) {
        return Post::select('id', 'name', 'category_id', 'img')->whereIn('id',$data)->get()->toArray();
    }

    public function savePost($time, $data) {
        $post = New Post();
        $post->img = $data['img_name'];
        if (isset($data['image'])) {
            $arr_img = [];
            foreach ($data['image'] as $item) {
                $arr_img[] = 'post_detail_'.$time.'_'.$item->getClientOriginalName();
            }
            $post->image = json_encode($arr_img);
        } else {
            $post->image = json_encode([]);
        }
        if (isset($data['status']))
            $status = $data['status'];
        else
            $status = 0;

        if (isset($data['hotnews']))
            $post->hotnews = $data['hotnews'];
        else
            $post->hotnews = 0;

        if (isset($data['care']))
            $post->care = $data['care'];
        else
            $post->care = 0;

        if ($data['tags']) {
            $post->tags = json_encode(explode(", ",$data['tags']));
        } else {
            $post->tags = $data['tags'];
        }

        $post->name = $data['name'];
        $post->slug = $data['slug'];
        $post->meta_title = $data['meta_title'];
        $post->meta_description = $data['meta_description'];
        $post->meta_keywords = $data['meta_keywords'];
        $post->link = $data['link'];
        $post->price = $data['price'] ? str_replace(',', '', $data['price']) : '0.00';
        $post->specification = $data['specification'];
        $post->description = $data['description'];
        $post->content = $data['content'];
        $post->ord = $this->getLastOrder('posts');
        $post->status = $status;
        $post->category_id = $data['category_id'];
        $post->save();
        return $post->id;
    }

    public function updatePost($time,$data, $id) {
        $post = $this->getPost($id);
        $post->img = $data['img_name'];
        if (isset($data['image_del'])) {
            $old_image = array_diff(json_decode($post['image']), $data['image_del']);
        } else {
            $old_image = json_decode($post['image']);
        }
        if (isset($data['image'])) {
            $arr_img = [];
            foreach ($data['image'] as $item) {
                $arr_img[] = 'post_detail_'.$time.'_'.$item->getClientOriginalName();
            }
            $post->image = json_encode(array_merge($old_image,$arr_img));
        } else {
            $post->image = json_encode($old_image);
        }
        if (isset($data['status']))
            $post->status = $data['status'];
        else
            $post->status = 0;

        if (isset($data['hotnews']))
            $post->hotnews = $data['hotnews'];
        else
            $post->hotnews = 0;

        if (isset($data['care']))
            $post->care = $data['care'];
        else
            $post->care = 0;

        if ($data['tags']) {
            $post->tags = json_encode(explode(", ",$data['tags']));
        } else {
            $post->tags = $data['tags'];
        }

        $post->name = $data['name'];
        $post->slug = $data['slug'];
        $post->meta_title = $data['meta_title'];
        $post->meta_description = $data['meta_description'];
        $post->meta_keywords = $data['meta_keywords'];
        $post->link = $data['link'];
        $post->price = $data['price'] ? str_replace(',', '', $data['price']) : '0.00';
        $post->specification = $data['specification'];
        $post->description = $data['description'];
        $post->content = $data['content'];
        $post->ord = $post['ord'];
        $post->category_id = $data['category_id'];
        $post->save();
        return $post->id;
    }

    public function deletePost($id) {
        $post = Post::find($id);
        return $post->delete();
    }

    public function deleteMultiplePost($did) {
        $posts = $this->getMultiplePost($did);
        DB::beginTransaction();
        try {
            foreach ($posts as $item) {
                Post::where('id','=',$item['id'])->delete();
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function getCategory($slug) {
        return Category::where('slug','=',$slug)->first();
    }

    public function getPostByCategory($cate, $filter) {
        if ($filter) {
            $builder = Post::query();
            $builder->where('status','=',1);
            $builder->where('category_id','=',$cate);

            if ($filter == 1) {
                $builder->orderBy('id', 'DESC');
            }
            if ($filter == 2) {
                $builder->orderBy('hotnews', 'DESC');
            }
            if ($filter == 3) {
                $builder->orderBy(DB::raw('RAND()'));
            }
            $categories = $builder->get();
            return $categories;
        } else {
            return Post::where('status','=',1)
                ->where('category_id','=',$cate)
                ->orderBy('ord','DESC')->get();
        }
    }

    public function getPostBySlug($slug) {
        return Post::whereSlug($slug)->first();
    }

    public function getPostPrev($id) {
        return Post::where('id', '<', $id)->first();
    }

    public function getPostNext($pid,$id) {
        return Post::where('id', '!=', $id)->where('category_id', '=', $pid)->limit(3)->orderBy('id','DESC')->get();
    }

    public function getPostRealated($id) {
        return Post::whereStatus(1)
            ->where('id','!=',$id)
            ->orderBy('id','DESC')
            ->limit(3)->get();
    }

    public function getPostComment($post_id, $pid) {
        $comments = Comment::whereStatus(1)
            ->where('post_id','=',$post_id)
            ->where('parent_id','=',$pid)
            ->orderBy('id','DESC')
            ->get()->toArray();
        return $comments;
//        return Common::getCommentOption($comments, 0);
    }

    public function getPostByKeyword($page, $keyword) {
        return Post::select('name','id','slug','img','description')
            ->where('name','like','%'.$keyword.'%')
            ->orWhere('description','like','%'.$keyword.'%')
            ->orderBy('id', 'DESC')
            ->paginate($page);
    }

    public function getNewsByCategory($category, $order_by) {
        if ($order_by) {
            $builder = DB::table('posts');
            $builder->where('category_id', '=', $category);
            if ($order_by == 'date') {
                $builder->orderBy('id', 'DESC');
            }
            if ($order_by == 'price-asc') {
                $builder->orderBy('price', 'ASC');
            }
            if ($order_by == 'price-desc') {
                $builder->orderBy('price', 'DESC');
            }
            return $builder->paginate(9);
        } else {
            return Post::where('status','=',1)
                ->where('category_id', '=', $category)
                ->orderBy('ord','DESC')->paginate(9);
        }
    }

    public function getNewsByKeyword($keyword, $order_by) {
        if ($order_by) {
            $builder = DB::table('posts');
            $builder->where('name', 'LIKE', "%$keyword%");
            if ($order_by == 'date') {
                $builder->orderBy('id', 'DESC');
            }
            if ($order_by == 'price-asc') {
                $builder->orderBy('price', 'ASC');
            }
            if ($order_by == 'price-desc') {
                $builder->orderBy('price', 'DESC');
            }
            return $builder->paginate(9);
        } else {
            return Post::where('status','=',1)
                ->where('name', 'LIKE', "%$keyword%")
                ->orderBy('ord','DESC')->paginate(9);
        }
    }
}