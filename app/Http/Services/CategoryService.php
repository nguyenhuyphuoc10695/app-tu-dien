<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 6/1/2019
 * Time: 3:39 PM
 */

namespace App\Http\Services;


use App\Category;
use App\Course;
use App\Post;
use Illuminate\Support\Facades\DB;

class CategoryService extends MainService
{
//    const DEFAULT_PARENT_ID = 0;
    public function getCategories() {
        return Category::orderBy('id','DESC')->get();
    }

    public static function getCategoryByParent($parentId) {
        return Category::where('parent_id', '=', $parentId)->orderBy('id','DESC')->get();
    }

    public function getCategoryStructureId($categoryId) {
        return Category::select('id', 'structure_id')->where('id', '=', $categoryId)->orderBy('id','DESC')->first();
    }

    public function getCategory($id) {
        return Category::find($id);
    }

    public function saveCategory($time, $data) {
        $category = New Category();
        if (isset($data['img']))
            $category->img = 'category_'.$time.'_'.$data['img']->getClientOriginalName();
        else
            $category->img = null;
        if (isset($data['status']))
            $status = $data['status'];
        else
            $status = 0;
        if (isset($data['has_children']))
            $hasChildren = $data['has_children'];
        else
            $hasChildren = 0;
        $structureId = $this->getCategoryStructureId($data['category_id']);
        $category->name = $data['name'];
        $category->ge_name = $data['ge_name'];
        $category->ge_content = $data['ge_content'];
        $category->content = $data['content'];
        $category->parent_id = $data['category_id'];
        $category->structure_id = $structureId ? $structureId->structure_id : '';
        $category->ord = $this->getLastOrder('categories');
        $category->status = $status;
        $category->has_children = $hasChildren;
        if ($category->save()) {
            $categoryId = $category->id;
            $category->structure_id = $category->structure_id.$categoryId;
            $category->save();
        }
        return $category->id;
    }

    public function updateCategory($time, $data, $id) {
        $category = $this->getCategory($id);
        if (isset($data['img']))
            $category->img = 'category_'.$time.'_'.$data['img']->getClientOriginalName();
        else
            $category->img = $category['img'];

        if (isset($data['status']))
            $category->status = $data['status'];
        else
            $category->status = 0;

        if (isset($data['has_children']))
            $category->has_children = $data['has_children'];
        else
            $category->has_children = 0;
        if ($data['category_id'] != $category->structure_id) {
            $structureId = $this->getCategoryStructureId($data['category_id']);
            $category->structure_id = $structureId ? $structureId->structure_id : '';
        }

        $category->name = $data['name'];
        $category->ge_name = $data['ge_name'];
        $category->ge_content = $data['ge_content'];
        $category->content = $data['content'];
        $category->parent_id = $data['category_id'];
        $category->ord = $category['ord'];
        $category->save();
        return $category->id;
    }

    public function deleteCategory($id) {
        $category = Category::find($id);
        return $category->delete();
    }

    public function deleteCourseHasCategory($cate) {
        $courses = Course::where('category_id','=',$cate)->get()->toArray();
        DB::beginTransaction();
        try {
            foreach ($courses as $item) {
                Course::where('id','=',$item['id'])->delete();
                $this->deleteImage($item['img']);
                $this->deleteImage($item['image']);
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }
}
