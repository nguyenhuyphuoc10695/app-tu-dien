<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'ge_name' => 'required',
            'img' => 'bail|image|mimes:jpeg,png,jpg,gif|max:2048',
        ];
    }

    public function messages () {
        return [
            'name.required' => 'Vui lòng nhập tên danh mục tiếng Việt',
            'ge_name.required' => 'Vui lòng nhập tên danh mục bằng tiếng Đức',
            'img.image' => 'Không đúng định dạng ảnh',
            'img.mimes' => 'Định dạng ảnh không hợp lệ (png,jpg,jpeg,gif)',
            'img.max' => 'Ảnh upload vượt quá 2MB',
        ];
    }
}
