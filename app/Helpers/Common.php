<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 6/3/2019
 * Time: 9:50 AM
 */

namespace App\Helpers;


use App\Archive;
use App\Branch;
use App\CareerContact;
use App\Category;
use App\Comment;
use App\Course;
use App\Contact;
use App\Department;
use App\InvestorContact;
use App\Location;
use App\Menu;
use App\Newsletter;
use App\OwnerContact;
use App\Partner;
use App\Post;
use App\Region;
use App\Service;
use App\Setting;
use App\Slide;
use App\Social;
use App\Solution;
use App\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Common
{
    public static function getSettings() {
        return Setting::first();
    }

    public static function getMenuPosition($pos,$lang) {
        return Menu::where('status','=',1)
            ->where('parent_id','=',0)
            ->where('lang','=',$lang)
            ->where('position','=',$pos)
            ->orderBy('ord', 'ASC')
            ->get()->toArray();
    }

    public static function convertTime($time) {
        $day = date('d', strtotime($time));
        $month = date('F', strtotime($time));
        $year = date('Y', strtotime($time));
        $date = $month.' '.$day.', '.$year;
        return $date;
    }

    public static function getMenuData($id) {
        return Menu::where('status','=',1)->where('id','!=',$id)->get()->toArray();
    }

    public  static function getMenuContent($id) {
        return Menu::find($id);
    }

    public static function getSocials() {
        return Social::all();
    }

    public static function getMenuOptions($data, $parent = 0, $str = "", $select = 0) {
        foreach ($data as $key => $val) {
            $id = $val["id"];
            $name = $val["name"];
            if ($val["parent_id"] == $parent) {
                if ($select != 0 && $id == $select) {
                    echo "<option value='$id' selected = 'selected'>$str $name</option>";
                } else {
                    echo "<option value='$id'>$str $name</option>";
                }
                // Xóa chuyên mục đã lặp
                unset($data[$key]);
                self::getMenuOptions($data, $id, $str . "--- ", $select);
            }
        }
    }

    public static function getRegionData($id) {
        return Region::where('status','=',1)->where('id','!=',$id)->get()->toArray();
    }

    public static function getRegionOptions($data, $parent = 0, $str = "", $select = 0) {
        foreach ($data as $key => $val) {
            $id = $val["id"];
            $name = $val["name"];
            if ($val["parent_id"] == $parent) {
                if ($select != 0 && $id == $select) {
                    echo "<option value='$id' selected = 'selected'>$str $name</option>";
                } else {
                    echo "<option value='$id'>$str $name</option>";
                }
                // Xóa chuyên mục đã lặp
                unset($data[$key]);
                self::getRegionOptions($data, $id, $str . "--- ", $select);
            }
        }
    }

    public static function getCategoryOptions($parent = 0) {
        $categories = Category::where('status','=',1)->get()->toArray();
        foreach ($categories as $key => $item) {
            $id = $item['id'];
            $name = $item['name'];
            if ($id == $parent) {
                echo "<option value='$id' selected = 'selected'>$name</option>";
            } else {
                echo "<option value='$id'>$name</option>";
            }
        }
    }

    public static function getSolutionOptions($parent = 0) {
        $solutions = Solution::where('status','=',1)->get()->toArray();
        foreach ($solutions as $key => $item) {
            $id = $item['id'];
            $name = $item['name'];
            if ($id == $parent) {
                echo "<option value='$id' selected = 'selected'>$name</option>";
            } else {
                echo "<option value='$id'>$name</option>";
            }
        }
    }

    public static function getStaffOptions($parent = 0) {
        $staff = Staff::where('status','=',1)->get()->toArray();
        foreach ($staff as $key => $item) {
            $id = $item['id'];
            $name = $item['name'];
            if ($id == $parent) {
                echo "<option value='$id' selected = 'selected'>$name</option>";
            } else {
                echo "<option value='$id'>$name</option>";
            }
        }
    }

    public static function getStatus($stt,$id,$table) {
        if ($stt == 1)
            return '<span class="span-color" style="background: #2ab27b;color:white;padding: 5px;border-radius: 5px;cursor: pointer;" onclick="changeStatus('.$stt.','.$id.','.$table.')">Hiển thị</span>';
        return '<span class="span-color" style="background: grey;color:white;padding: 5px;border-radius: 5px;cursor: pointer;" onclick="changeStatus('.$stt.', '.$id.','.$table.')">Khóa</span>';
    }

    public static function getCategoryLevel($str) {
        $count = count(explode('-',$str)) - 1;
        $level = '';
        for($i = 0; $i < $count; $i++) {
            if ($i != 0)
                $level = $level.'--- ';
        }
        return $level;
    }

    public static function getRoleOptions($level, $user_level) {
        foreach (config('role') as $key => $val) {
            $name = $val["name"];
            $role= $val["level"];

            if ($role > $level) {
                if ($role == $user_level) {
                    echo "<option value='$role' selected = 'selected'>$name</option>";
                } else {
                    echo "<option value='$role'>$name</option>";
                }
            }
        }
    }

    public static function getLanguageOption($lang) {
        foreach (config('lang') as $key => $val) {
            $id = $val["id"];
            $name = $val["name"];
            if ($id === $lang) {
                echo "<option value='$id' selected = 'selected'>$name</option>";
            } else {
                echo "<option value='$id'>$name</option>";
            }
        }
    }

    public static function getCategories() {
        return Category::where('status','=',1)->get()->toArray();
    }

    public static function getArchiveList() {
        return Archive::where('status','=',1)->get();
    }

    public static function getAllSolutions() {
        return Solution::where('status','=',1)->get();
    }

    public static function getSolutionByID($id) {
        return Solution::find($id);
    }

    public static function getHomeCategories() {
        return Category::where('status','=',1)->orderBy('ord','ASC')->get()->toArray();
    }

    public static function getMenuCategories() {
        return Category::where('status','=',1)
           ->orderBy('ord','ASC')
            ->get()->toArray();
    }

    public static function getMenuList($pid) {
        return Menu::where('status','=',1)
            ->where('parent_id','=',$pid)
            ->orderBy('ord','ASC')
            ->get();
    }

    public static function getPartners() {
        return Partner::where('status','=',1)->get();
    }

    public static function getBranches() {
        return Branch::where('status','=',1)->get();
    }

    public static function getStaffList() {
        return Staff::where('status','=',1)->limit(9)->get();
    }

    public static function getNumberCommentInNews($post_id) {
        return Comment::where('status','=',1)->where('post_id','=',$post_id)->get()->count();
    }


    public static function getLangCategories($lang) {
        return Category::where('status','=',1)->where('lang','=',$lang)->get()->toArray();
    }

    public static function getCategoryByID($id) {
        return Category::find($id);
    }

    public static function getCourseByID($id) {
        return Course::find($id);
    }

    public static function getNumberLatestCourse($limit) {
        return Course::where('status','=',1)->orderBy('id', 'DESC')->limit($limit)->get();
    }

    public static function getFivePostsForEachCategory($category_id) {
        return Post::where('status','=',1)->where('category_id','=',$category_id)->orderBy('ord', 'DESC')->paginate(5);
    }

    public static function getFiveHotNewsPost() {
        return Post::where('status','=',1)->where('hotnews','=',1)->orderBy('ord', 'DESC')->paginate(5);
    }
    public static function getNumberLatestPost($limit) {
        return Post::where('status','=',1)->orderBy('id', 'DESC')->limit($limit)->get();
    }
    public static function getPostByID($id) {
        return Post::find($id);
    }


    public static function getMenuChildren($pid, $lang) {
        return Menu::where('status','=',1)
            ->where('lang','=',$lang)
            ->where('parent_id','=',$pid)
            ->orderBy('ord','ASC')->get()->toArray();
    }

    public static function getMenuByID($did, $lang) {
        return Menu::whereIn('id',$did)->where('lang','=',$lang)->get()->toArray();
    }

    public static function getRegionChildren($pid,$lang) {
        return Region::where('status','=',1)->where('lang','=',$lang)->where('parent_id','=',$pid)->get()->toArray();
    }

    public static function get_contacts() {
        return Contact::where('status','=',0)->get()->count();
    }

    public static function get_comments() {
        return Comment::where('status','=',0)->get()->count();
    }

    public static function getCourseByCategoryID($category_id) {
        return Course::where('status','=',1)->where('category_id','=',$category_id)->get()->count();
    }

    public static function getNewsletter() {
        return Newsletter::where('status','=',0)->get()->count();
    }

    public static function getCareerContact() {
        return CareerContact::where('status','=',0)->get()->count();
    }

    public static function getDepartments() {
        return Department::where('status','=',1)->get();
    }

    public static function getSlides() {
        return Slide::where('status','=',1)->get();
    }

    public static function getServices() {
        return Service::where('status','=',1)->get();
    }

    public static function getLocations() {
        return Location::where('status','=',1)->get();
    }

    public static function getLanguage() {
        if (Session::has('lang')) {
            return Session::get('lang') - 1;
        } else {
            return 0;
        }
    }

    public static function getLang() {
        if (Session::has('lang')) {
            return Session::get('lang');
        } else {
            return 1;
        }
    }

    public static function getCommentOption(array $data, $parent = 0) {
        $comments = [];
        foreach ($data as $item) {
            if ($item['parent_id'] == $parent) {
                $children = self::getCommentOption($item, $item['id']);
                if ($children) {
                    $item['children'] = $children;
                }
                $comments[] = $item;
            }
        }
        return $comments;
    }

    public static function getAlbumType($type) {
        if ($type == 1)
            return 'Thư viện ảnh';
        else
            return 'Thư viện video';
    }
}